package com.isos.tt.page;

import org.openqa.selenium.By;

public class TravelTrackerHomePage {

	/**
	 * Below Locators are related to Login information in the Travel Tracker
	 * Login page
	 */
	public static By userName;
	public static By password;
	public static By loginButton;

	/**
	 * Below Locators are related to Send Message Screen in Map Home Page
	 */
	public static By travellerCount;
	public static By loadingImage;
	public static By messageIcon;
	public static By subject;
	public static By messageBody;
	public static By twoSMSResponse;
	public static By sendMessageButton;
	public static By travellerName;
	public static By emailText;
	public static By emailCheckbox;
	public static By phoneText;
	public static By phoneCheckbox;
	public static By messageLength;
	public static By closePopup;

	/**
	 * Below Locators are related to Communication History Page
	 */
	public static By communicationHistory;
	public static By messageType;
	public static By searchBtn;
	public static By searchResults;
	public static By searchResultsSubject;

	public static By logOff;
	public static By toolsLink;
	public static By intlSOSPortal;
	public static By searchButtonIsosPortal;
	public static By printButton;
	public static By exportToExcelBtn;
	public static By exportToZipBtn;
	public static By messageManagerLink;
	public static By helpLink;
	public static By userGuide;
	public static By feedbackLink;
	public static By userSettingsLink;
	public static By changePwdChallengeQstns;
	public static By updateEmailAddress;
	public static By updateButton;
	public static By welcomePage;
	public static By noOfPages;
	public static By intlSOS;
	public static By intlSOSURL;
	public static By resetBtn;
	public static By filtersBtn;
	public static By location;
	public static By nowCheckBox;
	public static By last31Days;
	public static By next24Hours;
	public static By next1to7Days;
	public static By next8to31Days;
	public static By alertCount;
	public static By internationalCheckbox;
	public static By domesticCheckbox;
	public static By expatriateCheckbox;
	public static By asiaAndPacific;
	public static By homeCountry;
	public static By extremeTravelChkbox;
	public static By highTravelChkbox;
	public static By mediumTravelChkbox;
	public static By lowTravelChkbox;
	public static By insignificantTravelChkbox;
	public static By extremeMedicalChkbox;
	public static By highMedicalChkbox;
	public static By mediumMedicalChkbox;
	public static By lowMedicalChkbox;
	public static By searchDropdown;
	public static By searchBox;
	public static By goButton;
	public static By tileContent;
	public static By locationName;
	public static By countryName;
	public static By travellerPanel;
	public static By locationsTravellerCount;
	public static By exportPeoplePane;
	public static By messageBtn;
	public static By travellerNameSearch;
	public static By travellerDetailsHeader;
	public static By mapHomeTab;
	public static By yearDateRange;
	public static By yearValue;
	public static By monthDateRange;
	public static By monthValue;
	public static By dayDateRange;
	public static By fromCalendarIcon;
	public static By dateRange;
	public static By flightCount;
	public static By flightName;
	public static By filterCountry;
	public static By travellersList;
	public static By closeTravellersList;
	public static By travellerCheckbox;
	public static By sortByFilter;
	public static By sortByCountry;
	public static By sortByTravellerCount;
	public static By alertsTab;
	public static By mapIFrame;
	public static By messageIFrame;
	public static By alertTile;
	public static By readMoreLink;
	public static By alertPopupHeader;
	public static By alertPopupLocation;
	public static By alertPopupEventDate;
	public static By closeAlertPopup;

	/**
	 * Regression test cases
	 */
	public static By findTrains;
	public static By findHotels;
	public static By findResults;
	public static By closeBtnSearch;
	public static By buildingsTab;
	public static By addBuilding;
	public static By buildingName;
	public static By buildingAddress;
	public static By buildingCity;
	public static By buildingCountry;
	public static By buildingSave;
	public static By flyBtn;
	public static By aerialBtn;
	public static By editBuilding;
	public static By findBuilding;

	public static By searchIcon;
	public static By travelerName;
	public static By sendMessageAtTravellerDetails;
	public static By sendMessageConfirmationlabel;
	public static By itineraryResultExpandIcon;
	public static By mapPoints;
	public static By buildingsCheckbox;
	public static By buildingsIconOnMap;
	public static By buildingsResultPane;
	public static By buildingDetailsForm;
	public static By buildingOfficeImage;

	public static By checkInsCheckbox;
	public static By trainName;
	public static By countryGuide;
	public static By countryGuideHeader;
	public static By travellerFromSearch;
	public static By flightFromSearch;
	public static By hotelName;
	public static By buildingFromResult;

	public static By medicalRiskIcon;
	public static By travelRiskIcon;
	public static By countryGuideIcon;
	public static By countryCount;
	public static By presetSelect;

	public static By arrowDownFilter;
	public static By vipTraveller;
	public static By nonVipTraveller;
	public static By ticketed;
	public static By nonTicketed;
	public static By travellerNameInTravellerList;
	public static By vipStatusIndicator;
	public static By nonVipStatusIndicator;
	public static By datePresetList;

	public static By travellerNameDetails;
	public static By travellerEmailDetails;
	public static By travellerCountryDetails;

	public static By sortByTitle;
	public static By sortByDate;

	public static By vismoTab;
	public static By vismoNormalLabel;
	public static By alertItemsCount;
	public static By countryNameInAlertsList;

	public static By intlSOSResourcesTab;
	public static By findIntlSOSResources;
	public static By intlSOSResourceAddress;
	public static By intlSOSResourcePhone;
	public static By intlSOSResourceImage;
	public static By intlSOSCheckBox;

	public void travelTrackerHomePage()

	{

		travellerCount = By.xpath("//div[@id='tileContent']//following-sibling::ul//li[1]//h2");
		loadingImage = By.xpath("//div[@id='tt-loading-overlay']");
		messageIcon = By.xpath("//div[@id='messageLink1']/a/img");
		subject = By.xpath("//input[@id='ctl00_MainContent_txtSubject']");
		messageBody = By.xpath("//textarea[@id='ctl00_MainContent_txtMessage']");
		twoSMSResponse = By.xpath("//input[@id='ctl00_MainContent_chkResponseOptions']");
		sendMessageButton = By.xpath("//input[@id='ctl00_MainContent_btnSendMessage']");
		travellerName = By.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblName']");
		emailCheckbox = By.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblEmail']");
		emailCheckbox = By.xpath("//input[@id='ctl00_MainContent_gvRecipients_ctl02_chkEmail']");
		phoneCheckbox = By.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblPhone']");
		phoneCheckbox = By.xpath("//input[@id='ctl00_MainContent_gvRecipients_ctl02_chkPhone']");
		messageLength = By.xpath("//span[@id='ctl00_MainContent_lblCharacterCount']");
		closePopup = By.cssSelector("#close-popup");
		communicationHistory = By.xpath("//a[text()='Communication History']");
		messageType = By.xpath("//select[@id='ctl00_MainContent_drpMessageTypeId']");
		searchBtn = By.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		searchResults = By.xpath("//table[@id='ctl00_MainContent_gvResults']");
		searchResultsSubject = By.xpath(
				"//table[@id='ctl00_MainContent_gvResults']//a[@id='ctl00_MainContent_gvResults_ctl02_lnkMessageID']");
		logOff = By.xpath("//li[@id='LogOff']");
		userName = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
		password = By.xpath("//input[@id='ctl00_MainContent_LoginUser_txtPassword']");
		loginButton = By.xpath("//input[@id='ctl00_MainContent_LoginUser_btnLogIn']");
		toolsLink = By.xpath("//a[@id='ctl00_lnkTools']");
		intlSOSPortal = By.xpath("//li[@id='ctl00_lnkIntlSOSPortal']");
		searchButtonIsosPortal = By.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		printButton = By.xpath("//input[@id='ctl00_MainContent_btnPrint']");
		exportToExcelBtn = By.xpath("//input[@id='ctl00_MainContent_btnExport']");
		exportToZipBtn = By.xpath("//*[@id='ctl00_MainContent_btnZipExport']");
		messageManagerLink = By.xpath("//li[@id='ctl00_lnkMessageManager']");
		helpLink = By.xpath("//li[@id='Help']");
		userGuide = By.xpath("//div[contains(text(),'Users Guide')]");
		feedbackLink = By.xpath("//li[@id='Feedback']");
		userSettingsLink = By.xpath("//li[@id='ctl00_lnkUserSettings']");
		changePwdChallengeQstns = By.xpath("//a[@id='ctl00_MainContent_hlUpdatePassword']");
		updateEmailAddress = By.xpath("//a[@id='ctl00_MainContent_hlUpdateEmail']");
		updateButton = By.xpath("//input[@id='ctl00_MainContent_ProactiveEmails1_btnUpdate']");
		welcomePage = By.xpath("//span[contains(text(),'Welcome')]");
		noOfPages = By.xpath("//*[@id='numPages']");

		intlSOS = By.xpath(
				"//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers']/option[text()='Test Customer 1']");
		intlSOSURL = By.xpath(
				"//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lblEncryptedValue']");
		resetBtn = By.xpath("//label[text()='Reset Map']");
		// filtersBtn = By.xpath("//div[@id='FilterButton']");
		filtersBtn = By.id("FilterButton");
		location = By.xpath("//select[@id='presetSelect']");
		last31Days = By.xpath("//input[@id='last31']");
		nowCheckBox = By.xpath("//input[@id='Now']");
		next24Hours = By.xpath("//input[@id='Next24h']");
		next1to7Days = By.xpath("//input[@id='Next1to7d']");
		next8to31Days = By.xpath("//input[@id='Next31d']");
		alertCount = By.xpath("//h2[@title='Click to view details']");
		internationalCheckbox = By.xpath("//input[@id='international1']");
		domesticCheckbox = By.xpath("//input[@id='domestic1']");
		expatriateCheckbox = By.xpath("//input[@id='expatriate1']");
		homeCountry = By.xpath("//input[@id='country']");
		extremeTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='extreme']");
		highTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='high']");
		mediumTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='medium']");
		lowTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='low']");
		insignificantTravelChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='insignificant']");
		extremeMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='extreme1']");
		highMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='high1']");
		mediumMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='medium1']");
		lowMedicalChkbox = By.xpath(
				"//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='low1']");
		searchDropdown = By.xpath("//select[@id='searchoptions']");
		searchBox = By.xpath("//input[@name='deskSearch']");
		goButton = By.xpath("//button[@id='btnSearch']");
		tileContent = By.xpath("//div[@id='tileContent']");
		countryName = By.xpath("//div[@id='tileContent']//span[contains(text(),'Asia & the Pacific')]");
		locationsTravellerCount = By.xpath("//div[contains(text(),'People')]");
		exportPeoplePane = By.xpath("//div[@id='exportLink']/a/img");
		messageBtn = By.xpath("//div[@id='messageLink1']/a/img");
		travellerDetailsHeader = By.xpath("//span[text()='Traveller Details']");
		mapHomeTab = By.xpath("//a[text()='Map Home']");
		yearDateRange = By.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']");
		yearValue = By.xpath(
				"//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']/option[text()='2009']");
		monthDateRange = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']");
		monthValue = By.xpath(
				"//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']/option[text()='May']");
		// dayDateRange = By.xpath("//div[@class='pika-single
		// is-bound']//table//button[text()='8']");
		dayDateRange = By.xpath("//div[@class='pika-single is-bound']//table//button[text()='<replaceValue>']");
		fromCalendarIcon = By.xpath("//li[@id='fromCalendarIcon']");
		dateRange = By.xpath("//li[text()='Date Range']");
		flightCount = By.xpath("//div[@id='tileContent']//div[contains(text(),'Flight')]");
		travellersList = By.xpath("//div[@id='travelerList']");
		closeTravellersList = By.xpath("//div[@id='closeTravelerDiv']//img");
		travellerCheckbox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		sortByFilter = By.xpath("//img[@title='Sort By']");
		sortByCountry = By.xpath("//li[@id='alertsLocationNameField']");
		sortByTravellerCount = By.xpath("//li[@id='alertsTravelerCountField']");
		alertsTab = By.xpath("//div[@id='alertsTab']");
		alertTile = By.xpath("//ul[@id='alertItems']/li[1]//h1");
		readMoreLink = By.xpath("//ul[@id='alertItems']/li[1]//span[text()='Read more']");
		alertPopupHeader = By.xpath("//div[@id='container']//h1");
		alertPopupLocation = By.xpath("//i[text()='Location']");
		alertPopupEventDate = By.xpath("//i[text()='Event Date']");
		closeAlertPopup = By.xpath("//div[@id='close-popup']");
		asiaAndPacific = By.xpath("//h1[text()='Asia & the Pacific']");
		mapIFrame = By.id("ctl00_MainContent_mapiframe");
		messageIFrame = By.id("messageIframe");

		/**
		 * Regression test cases
		 */
		findTrains = By.xpath("//input[@id='trainsFind']");
		findResults = By.xpath("//div[@id='tileContent']//li");
		findHotels = By.xpath("//input[@id='hotelsFind']");
		closeBtnSearch = By.xpath("//div[@id='closeBtnSearch']");
		buildingsTab = By.xpath("//div[@id='buildingsTab']");
		addBuilding = By.xpath("//button[@id='addBuilding']");
		buildingName = By.xpath("//input[@id='bname']");
		buildingAddress = By.xpath("//input[@id='baddress']");
		buildingCity = By.xpath("//input[@id='bcity']");
		buildingCountry = By.xpath("//input[@id='bcountry']");
		buildingSave = By.xpath("//button[@id='btnSubmit']");
		flyBtn = By.xpath("//button[@id='btnFly']");
		aerialBtn = By.xpath("//button[text()='Aerial']");
		editBuilding = By.xpath("//button[@id='btnEdit']");
		findBuilding = By.xpath("//input[@id='buildingsFind']");

		searchIcon = By.xpath("//img[@src='images/search-icon_white.png']");
		travelerName = By.xpath("//div[@id='scrollDiv']//li[1]//h2/span");
		sendMessageAtTravellerDetails = By.xpath("//a[@id='messageLink1']/img");
		sendMessageConfirmationlabel = By.id("lblStatus");
		itineraryResultExpandIcon = By.xpath("//img[@src='images/colapse-icon-sm.png']");

		mapPoints = By.xpath("//a[text()='Map Points']");
		buildingsCheckbox = By.xpath("//input[@id='chkBuildings']");
		buildingsIconOnMap = By.xpath("//span[@class='clusterMarkerIcons buildingClusterIcon']");
		buildingsResultPane = By.xpath("//span[contains(text(),'Buildings')]");
		buildingDetailsForm = By.xpath("//form[@id='buildingDetailsForm']");
		buildingOfficeImage = By.xpath("//img[contains(@src,'building_office')]");
		checkInsCheckbox = By.id("chkCheckins");

		filterCountry = By
				.xpath("//div[@class='container search-container']//span[contains(text(),'[COUNTRY] <replaceValue>')]");
		trainName = By.xpath(".//label[contains(text(),'<replaceValue>')]");
		countryGuide = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryGuideHeader = By.xpath("//h1[contains(text(),'<replaceValue>')]");
		travellerFromSearch = By.xpath(".//*[@id='scrollDiv']//*[contains(text(),'<replaceValue>')]");
		flightFromSearch = By.xpath("//label[contains(text(),'<replaceValue>')]");
		hotelName = By.xpath("//*[@id='tileContent']//ul/li[1]/div/div[text()='<replaceValue>']");
		buildingFromResult = By.xpath("//h1[text()='<replaceValue>']");
		medicalRiskIcon = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/medical_small_location_icon_n.png']");
		travelRiskIcon = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/travel_small_location_icon_n.png']");
		countryGuideIcon = By.xpath(
				"//h1[text()='<replaceValue>']/../..//following-sibling::div//img[@src='images/info_small_location_icon.png']");
		countryCount = By.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h2");
		presetSelect = By.xpath("//select[@id='presetSelect']");

		arrowDownFilter = By.className("arrow-down_filter");
		vipTraveller = By.id("vip");
		nonVipTraveller = By.id("nonVip");
		ticketed = By.id("ticketed");
		nonTicketed = By.id("unticketed");

		travellerNameInTravellerList = By.xpath(".//div[@id='scrollDiv']//li[1]//h2/span");
		vipStatusIndicator = By.xpath("//span[contains(text(),'VIP Status:Yes')]");
		nonVipStatusIndicator = By.xpath("//span[contains(text(),'VIP Status: No')]");
		datePresetList = By.xpath("//li[@title='Date Presets']");

		travellerNameDetails = By.xpath("//div[@id='scrollDiv']//li[1]/div/div//h2/span");
		travellerCountryDetails = By.xpath("//div[@id='scrollDiv']/ul/li[1]/div/div/span");
		travellerEmailDetails = By.xpath("//div[@id='scrollDiv']/ul/li[1]/div/div//a");

		sortByTitle = By.xpath("//li[@id='alertsTitleField']");
		sortByDate = By.xpath("//li[@id='alertsDateField']");

		vismoTab = By.id("vismoCheckInsTab");
		vismoNormalLabel = By.id("refeshVismoList");
		alertItemsCount = By.xpath("//ul[@id='alertItems']/li");
		countryNameInAlertsList = By.xpath(".//div[@id='tileContent']//li[<replaceValue>]//h1");

		intlSOSResourcesTab = By.id("resourcesTab");
		findIntlSOSResources = By.id("resourcesFind");
		intlSOSResourceAddress = By.xpath("//label[text()='Address']");
		intlSOSResourcePhone = By.xpath("//label[text()='Contact Phone']");
		intlSOSCheckBox = By.id("chkResources");
		intlSOSResourceImage = By.xpath("//img[contains(@src,'resources_ass')]");
	}
}
