package com.isos.tt.page;

import org.openqa.selenium.By;

public class ManualTripEntryPage {

	/**
	 * Below Locators are related to Creating New Traveler in the Manual Trip Entry Page
	 */
	public static By firstName;
	public static By middleName;
	public static By lastName;
	public static By homeCountryList;
	public static By homeSite;
	public static By phonePriority;
	public static By phoneType;
	public static By countryCode;
	public static By phoneNumber;
	public static By emailPriority;
	public static By emailType;
	public static By emailAddress;
	public static By contractorId;
	public static By department;
	public static By documentCountryCode;
	public static By documentType;
	public static By comments;
	public static By saveTravellerDetails;
	public static By TravellerDetailsSuccessMessage;
	public static By travellerSearchBtn;
	public static By travellersDropDownPanel;
	public static By profileLookup;
	public static By selectTravellerBtn;
	public static By verifyTripName;
	public static By addTravellerbtn;
	public static By addTravellersDropDownPanel;
	public static By addTraveller;
	public static By copyTrip;
	public static By createNewTripTab;
	public static By autoComplete;
	public static By manualTripEntryLink;
	public static By selectButton;
	public static By createNewTravellerBtn;
	public static By selectTraveller;
	public static By addProfileButton;
	public static By profileLookupDropDown;
	
	
	public void manualTripEntryPage()

	{
		manualTripEntryLink = By.xpath("//a[text()='Manual Trip Entry']");
		selectButton = By.xpath("//input[@id='ctl00_MainContent_btnAddProfile']");
		createNewTravellerBtn = By.xpath("//input[@id='ctl00_MainContent_btnCreateTraveler']");
		selectTraveller = By.xpath("//td[contains(text(),'Select Traveller')]");
		firstName = By.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtFirstName']");
		middleName = By.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtMiddleName']");
		lastName = By.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName']");
		homeCountryList = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeCountry']");
		homeSite = By.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeSite']");
		phonePriority = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlPhonePriority']");
		phoneType = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlMobileType']");
		countryCode = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlCountry']");
		phoneNumber = By.xpath(
				"//input[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_txtPhoneNo']");
		emailPriority = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailPriority']");
		emailType = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailType']");
		emailAddress = By.xpath(
				"//input[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_txtEmailAddress']");
		contractorId = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_txtEmpId']");
		department = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_ddlDept']");
		documentCountryCode = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlCountry']");
		documentType = By.xpath(
				"//select[@id='ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlDocumentTypeId']");
		comments = By.xpath("//textarea[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtComments']");
		saveTravellerDetails = By.xpath("//a[@id='ctl00_MainContent_btnUpdate']");
		TravellerDetailsSuccessMessage = By.xpath("//span[@id='ctl00_MainContent_ucCreateProfile_lblMessage']");
		travellerSearchBtn = By.xpath("//a[@id='ctl00_MiddleBarControl_linkTravelerSearch']");

		profileLookup = By.xpath("//input[@id='ctl00_MainContent_ucProfileLookup_autoCompleteTextProfilelookup']");
		selectTravellerBtn = By.xpath("//input[@id='ctl00_MainContent_btnAddProfile']");
		verifyTripName = By.xpath("//table[@id='ctl00_MainContent_ucTripList_gvTripDetail']");
		addTravellerbtn = By.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_btnAddTraveller']");
		addTravellersDropDownPanel = By.xpath(
				"//ul[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_autoComplete_completionListElem']/div[2]");
		addTraveller = By.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_txtAddTraveller']");
		copyTrip = By.xpath("//a[@id='ctl00_MainContent_ucTripList_gvTripDetail_ctl02_linkBtnCopyTripNameOrPnr']");
		createNewTripTab = By.xpath("//a[@id='ctl00_MiddleBarControl_linkCreateNewTrip']");
		autoComplete = By.xpath("//input[@id='hfAutoCompleteExtenderResultCount']");
		addProfileButton = By.xpath("//input[@id='ctl00_MainContent_btnAddProfile']");
		profileLookupDropDown = By.xpath("//*[@id='ctl00_MainContent_ucProfileLookup_autocompleteDropDownPanel']/div");

	}
}
