package com.isos.tt.page;

import org.openqa.selenium.By;



public class ContentEditorPage {

	public static By ContentTree;
	public static By PandemicSiteTree;
	public static By PandemicTree;
	public static By HomeTree;
	public static By NewsEditorialsTree;
	public static By NewsTree;
	public static By Inserticon;
	public static By NewsReport;
	public static By Headline;
	public static By SummaryBody;
	public static By ManagerBodyHtml;
	public static By ManagerFrame;
	public static By Acceptbutton;
	public static By Geocode;
	public static By Disease;
	public static By CreatedDate;
	public static By CreatedTime;
	public static By UpdatedDate;
	public static By UpdatedTime;
	public static By SaveButton;
	public static By SubmitButton;
	public static By PublishButton;
	public static By Newsroom;
	public static By News;
	public static By PublishedNews;
	public static By ManagerBodyField;
	public static By RightImage;
	public static By ReviewTab;
	public static By Menuicon;
	public static By Exiticon;
	public static By Username;
	public static By Password;
	public static By LoginButton;
	public static By PandemicLatestNews;
	public static By PandemicMembershipNo;
	public static By PandemicLogIn;
	public static By LatestNewsHeader;
	public static By PandemicMember;
	public static By PandemicLogout;
	public static By MicrositeTree;
	public static By SMicrositeTree;
	public static By HomeIcon;
	public static By HeadlineField;
	public static By PublishTab;
	public static By PublishIcon;
	public static By PublishItem;
	public static By MPublishButton;
	public static By FinishButton;

	// sampath

	public static By corporateSite;
	public static By corporateTree;
	public static By corporate_homeTree;
	public static By homeLink;
	public static By publishMainMenu;
	public static By publish_pageEditor;
	public static By componentButton;
	public static By addHere_caseDesc;
	public static By userStoriesShowCase;
	public static By Okbtn;
	public static By createNewContent;
	public static By contentName;
	public static By siteModuleBLocksTree;
	public static By contentTitle;
	public static By saveIcon;
	public static By reviewMenu;
	public static By submitMenu;
	public static By approveMenu;
	public static By publishIcon;
	public static By publishItem;

	public static By publishButton;
	public static By finishButton;
	public static By microsite_titleVerify;
	public static By microsite_DescVerify;

	public static By ReportSearch;
	public static By ReportRightImage;
	public static By ApproveButton;
	public static By Search;
	public static By SearchRightImage;

	public void contentEditorPage()

	{
		ContentTree = By
				.xpath("//*[@id='Tree_Glyph_0DE95AE441AB4D019EB067441B7C2450']");
		PandemicSiteTree = By
				.xpath("//*[@id='Tree_Glyph_7BB41307D7F54552AF954DBDF00A60EA']");
		PandemicTree = By
				.xpath("//*[@id='Tree_Glyph_64F54E90744443669BA0897B2EA5C55F']");
		HomeTree = By
				.xpath("//*[@id='Tree_Glyph_CDD1DE320BC147F6950DE1622088ABB7']");
		NewsEditorialsTree = By
				.xpath("//*[@id='Tree_Glyph_480D257775454EB9A92864940739B884']");
		NewsTree = By
				.xpath("//a[contains(@class,'scContentTreeNode')]//*[text()='News']");
		Inserticon = By
				.xpath("//table[@class='scMenu lang_en']//*[text()='Insert']");
		NewsReport = By
				.xpath("(//table[@class='scMenu lang_en']//*[text()='News Report'])[2]");

		Headline = By
				.xpath("//*[text()='Headline:']//parent::div//following-sibling::div//input");
		SummaryBody = By
				.xpath("//*[text()='SummaryBody:']//parent::div//following-sibling::div//textarea");
		ManagerBodyHtml = By.xpath("(//a[text()='Edit Html'])[1]");
		ManagerFrame = By.xpath("//*[@id='scContentIframeId0']");
		ManagerBodyField = By.xpath("//*[@id='ctl00_ctl00_ctl04_Html']");
		Acceptbutton = By.xpath("//*[@id='OK']");
		Geocode = By
				.xpath("//*[text()='Geocode:']//parent::div//following-sibling::div//input");
		Disease = By
				.xpath("(//*[@class='scContentControlMultilistBox']/option[2])[3]");
		RightImage = By.xpath("(//img[contains(@id,'btnRightFIELD')])[3]");
		CreatedDate = By
				.xpath("//*[contains(text(),'CreatedDate')]//parent::div//following-sibling::div//table[1]//input");
		CreatedTime = By
				.xpath("//*[contains(text(),'CreatedDate')]//parent::div//following-sibling::div//table[2]//input");
		UpdatedDate = By
				.xpath("//*[contains(text(),'UpdatedDate')]//parent::div//following-sibling::div//table[1]//input");
		UpdatedTime = By
				.xpath("//*[contains(text(),'UpdatedDate')]//parent::div//following-sibling::div//table[2]//input");
		SaveButton = By
				.xpath("//a[@class='scRibbonToolbarLargeButton']//span[text()='Save']");
		ReviewTab = By.xpath("//*[@id='Ribbon_Nav_ReviewStrip']");
		SubmitButton = By
				.xpath("//a[@class='scRibbonToolbarSmallButton']//span[text()='Submit']");
		PublishButton = By
				.xpath("//a[@class='scRibbonToolbarSmallButton']//span[text()='Publish']");
		Newsroom = By.xpath("//*[@id='nav-container']/nav/ul/li[5]/a");
		News = By.xpath("(//ul[@class='right-align']/li[1]/a)[1]");
		PublishedNews = By
				.xpath("//ul[@class='scrollable-container']//li[1]//a");

		// Logout
		Menuicon = By.xpath("//*[@id='SystemMenu']");
		Exiticon = By.xpath("//*[text()='Exit']");

		// Login
		Username = By.xpath("//*[@id='Login_UserName']");
		Password = By.xpath("//*[@id='Login_Password']");
		LoginButton = By.xpath("//*[@id='Login_Login']");

		PandemicLatestNews = By.xpath("//*[@class='vert-scroll']//li[1]/a");
		PandemicMembershipNo = By
				.xpath("//*[@id='site-container']//*[@id='MemberId']");
		PandemicLogIn = By
				.xpath("//*[@id='site-container']//*[@class='submit']");
		LatestNewsHeader = By
				.xpath("//*[@id='content-container']//*[@class='meta']//h2");
		PandemicMember = By.xpath("//*[text()='Member']");
		PandemicLogout = By
				.xpath("//*[@id='login-form-header']//*[text()='Logout']");

		MicrositeTree = By
				.xpath("//*[@id='Tree_Glyph_B5B7D28BE5CA41C5A102CDA6274B37A4']");
		SMicrositeTree = By
				.xpath("//*[@id='Tree_Glyph_5772A649F35D471B8430FE4B933423BE']");
		HomeIcon = By
				.xpath("//*[@id='Tree_Node_FA7D29C47FB746FEBFBAFA21A3A3EEC4']/span");
		HeadlineField = By
				.xpath("(//*[text()='Headline:']//parent::div//following-sibling::div//input)[1]");
		PublishTab = By.xpath("//*[@id='Ribbon_Nav_PublishStrip']");
		PublishIcon = By
				.xpath("//*[@id='B414550BADAF4542C9ADF44BED5FA6CB3E_menu_button']");
		PublishItem = By
				.xpath("//div[@class='scPopup']//*[text()='Publish Item']");
		MPublishButton = By.xpath("//*[@id='NextButton']");
		FinishButton = By.xpath("//*[@id='CancelButton']");

		// sampath
		corporateSite = By
				.xpath(".//*[@id='Tree_Glyph_542BEA6EF29D4B6CAE3719C64388664F']");
		corporateTree = By
				.xpath(".//*[@id='Tree_Glyph_45233090C4E84C57B718AF174D2F3E37']");
		corporate_homeTree = By
				.xpath(".//*[@id='Tree_Glyph_CECEC97F44E84D1187C4211B8DE78211']");

		homeLink = By
				.xpath(".//*[@id='Tree_Node_CECEC97F44E84D1187C4211B8DE78211']/span");
		publishMainMenu = By.xpath(".//*[@id='Ribbon_Nav_PublishStrip']");

		publish_pageEditor = By.xpath(".//span[text()='Page Editor']");
		componentButton = By
				.xpath("//a[@id='scRibbonButton_Insert Component']/img");
		addHere_caseDesc = By.xpath(".//*[@id='home']/div[37]/div[2]/span");
		userStoriesShowCase = By.xpath("//span[text()='UserStoriesShowcase']");
		Okbtn = By.xpath(".//*[@id='OK']");
		createNewContent = By.xpath(".//*[@id='CreateNew']");
		contentName = By.xpath(".//*[@id='NewDatasourceName']");
		siteModuleBLocksTree = By
				.xpath(".//*[@id='Tree_Glyph_BB5DABD17D5B4F7390DF00E836FD69BE']");
		contentTitle = By.xpath("//*[text()='Title:']/parent::td/div[2]/input");
		saveIcon = By.xpath("//span[text()='Save']");
		reviewMenu = By.xpath(".//*[@id='Ribbon_Nav_ReviewStrip']");
		submitMenu = By.xpath("//span[text()='Submit']");
		approveMenu = By.xpath("//span[text()='Approve']");
		publishIcon = By
				.xpath(".//*[@title='Publish the item in all languages to all publishing targets.']/parent::div/a[2]");
		publishItem = By
				.xpath("//*[@class='scPopup']//td[text()='Publish Item']");
		publishButton = By.xpath(".//*[@id='NextButton']");
		finishButton = By.xpath(".//*[@id='CancelButton']");
		microsite_titleVerify = By
				.xpath(".//*[@id='site-container']/div[6]/div/div/h1");
		microsite_DescVerify = By
				.xpath(".//*[@id='site-container']/div[8]/div/div[1]/h1");

		ReportSearch = By
				.xpath("//*[@class='scEditorSections']/table[2]//*[@class='scContentControlMultilistBox']//*[text()='Events (Taxonomy Keyword - Corporate - 1 - en)']");
		ReportRightImage = By
				.xpath("//*[@class='scEditorSections']/table[2]//img[contains(@id,'btnRightFIELD')]");
		ApproveButton = By
				.xpath("//a[@class='scRibbonToolbarSmallButton']//span[text()='Approve']");
		Search = By
				.xpath("//*[@class='scContentControlMultilistBox']//*[text()='Emerging Safety and Security Issues (Taxonomy Keyword - Corporate - 1 - en)']");
		SearchRightImage = By
				.xpath("(//img[contains(@id,'btnRightFIELD')])[1]");
	}

}
