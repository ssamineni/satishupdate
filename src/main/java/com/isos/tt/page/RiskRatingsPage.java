package com.isos.tt.page;

import org.openqa.selenium.By;

public class RiskRatingsPage {

	
	public static By riskRatingsLink;
	public static By riskRatingsHeader;
	public static By riskRatingsCustomer;
	public static By searchCountry;
	public static By medicalCheckBox;
	public static By travelCheckbox;
	public static By mediumMedical;
	public static By mediumTravel;
	public static By applyRiskRatingsBtn;
	public static By lastUpdatedTime;
	public static By riskRatingSuccessMsg;
	public static By verifyMedical;
	public static By crrprogressImage;
	public static By selectedCountryMedical;
	public static By selectedCountryTravel;

	public void riskRatingsPage()

	{

		riskRatingsLink = By.xpath("//a[@id='ctl00_lnkRiskRatingsLink']");
		riskRatingsHeader = By.xpath("//span[text()='TravelTracker Risk Rating']");
		riskRatingsCustomer = By
				.xpath("//select[@id='ctl00_MultiCustomerTTMasterSilverLight_drodownMultiCustomer']");
		searchCountry = By
				.xpath("//input[@id='txtautoCompleteCountry']");
		medicalCheckBox = By
				.xpath("//input[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating_ctl222_chkCustomMedicalRiskAdd']");
		travelCheckbox = By
				.xpath("//input[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating_ctl222_chkCustomTravelRiskAdd']");
		mediumMedical = By
				.xpath("//input[@id='rbtMM']");
		mediumTravel = By
				.xpath("//input[@id='rbtTM']");	
		applyRiskRatingsBtn = By
				.xpath("//input[@id='ctl00_MainContent_CustomRiskRatingUserControl1_btnApplySettings']");
		riskRatingSuccessMsg = By
				.xpath("//span[@id='lblStatusMessages']");
		lastUpdatedTime = By
				.xpath("//span[@id='ctl00_MainContent_CustomRiskRatingUserControl1_lblLastUpdatedTime']");
		verifyMedical =By 
				.xpath("//table[@id='ctl00_MainContent_CustomRiskRatingUserControl1_gvCustomRiskRating']//td[contains(text(),'<replaceValue>')]//following-sibling::td//img[@src='Images/Medical_M.png']");
		crrprogressImage = By
				.xpath("//div[@id='ctl00_MainContent_CustomRiskRatingUserControl1_updateProgressCRR']");
		selectedCountryMedical = By
				.xpath("//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnMedical']//td[text()='<replaceValue>']");
		selectedCountryTravel = By
				.xpath("//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnTravel']//td[text()='<replaceValue>']");
		
	}
}
