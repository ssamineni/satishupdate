package com.isos.tt.page;

import org.openqa.selenium.By;

public class SiteAdminPage {

	
	public static By siteAdminLink;
	public static By generaltab;
	public static By customerName;
	public static By selectcustomerDropDown;
	public static By myTripsTab;
	public static By manualTripEntryTab;
	public static By profileGrouptab;
	public static By profileFieldtab;
	public static By segmentationTab;
	public static By assignGrpUserTab;
	
	/**
	 * Below Locators are related to UI elements in the General tab of Site Admin page
	 */

	public static By userTab;
	public static By assignRolesToUsersTab;
	public static By assignUsersToRoleTab;
	public static By profileOptionsTab;
	public static By userMigrationTab;
	public static By promptedCheckInExclusionsTab;
	public static By selectUserUserTab;
	public static By userNameUserTab;
	public static By firstNameUserTab;
	public static By lastNameUserTab;
	public static By emailAddressUserTab;
	public static By emailStatus;
	public static By updateBtnUserTab;
	public static By successMsgUserTab;
	public static By selectUserRoleTab;
	public static By availableRole;
	public static By selectedRole;
	public static By appRoleName;
	public static By appRoleDesc;
	public static By appRoleDropDown;
	public static By roleDesc;
	public static By availableUsers;
	public static By selectedUsers;
	public static By profileField;
	public static By dropDownOptions;
	public static By migrateUsersBtn;
	public static By headerMsg;
	public static By progressImage;
	
	/**
	 * Below Locators are related to UI elements in the Manual Trip Entry tab of Site Admin page
	 */
	public static By tripSegmentsTab;
	public static By unmappedProfileFieldsTab;
	public static By metadataTripQuestionTab;
	public static By customTripQuestionTab;
	public static By selectProfileGroup;
	public static By profileGroupLabel;
	public static By updateLabelButton;
	public static By groupNameSuccessMsg;
	public static By profileGroupSaveBtn;
	public static By profileGroupSuccessMsg;
	public static By attributeGroup;
	public static By availableProfileFields;
	public static By selectedProfileFields;
	public static By labelForProfileField;
	public static By businessLocation;
	public static By isRequiredOnFormCheckbox;
	public static By profileFieldHomeSite;
	public static By profileFieldUpdateLabel;
	public static By profileFieldUpdateSuccessMsg;
	public static By profileFieldSaveBtn;
	public static By profileFieldSuccessMsg;
	public static By availableSegments;
	public static By selectedSegments;
	public static By availableFields;
	public static By selectedFields;
	public static By accountNo;
	public static By addBtn;
	public static By umappedProFldSaveBtn;
	public static By umappedProFldSuccessMsg;
	public static By availableMetadataTripQstns;
	public static By selectedMetadataTripQstns;
	public static By labelName;
	public static By responseType;
	public static By newBtn;
	public static By defaultQstn;
	public static By qstnText;
	public static By responseTypeCTQ;
	public static By applySettings;
	public static By updateCustomTripQstn;
	public static By saveChangesBtn;
	public static By CTQSuccessMsg;
	
	/**
	 * Below Locators are related to UI elements in the Segmentation tab of Site Admin page
	 */
	
	public static By assignUserToGrpTab;
	public static By addEditGrpTab;
	public static By addEditFilterTab;
	public static By assignFiltersToGrpTab;
	public static By selectGrpDropdown;
	public static By saveaddEditGrpBtn;
	public static By addEditGrpSuccessMsg;
	public static By filterDropdown;
	public static By saveaddEditFilterBtn;
	public static By addEditFilterSuccessMsg;
	public static By grpDropdown;
	public static By availableFilter;
	public static By moveFilter;
	public static By saveAssignToFiltersBtn;
	public static By assignFiltersToGrpSuccessMsg;
	public static By user;
	public static By availableGrp;
	public static By moveGrp;
	public static By saveAssignGrpToUserBtn;
	public static By assignGrpToUserSuccessMsg;
	public static By group;
	public static By availableUser;
	public static By moveUser;
	public static By saveAssignUserToGrpBtn;
	public static By assignUserToGrpSuccessMsg;
	
	/**
	 * Below Locators are related to users tab inside General tab of Site Admin Page.
	 */
	public static By emulateUserBtn;
	public static By welcomeCust;
	public static By stopEmulationBtn;
	/*
	 * 
	 */
	public static By assistanceAppCheckBox;
	public static By updateBtnCustTab;
	public static By checkInsTab;
	public static By blueManImagecheckInsTab;
	
	public void siteAdminPage()

	{

		siteAdminLink = By.xpath("//li[@id='Admin']");
		generaltab = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_labelGeneral']");
		selectcustomerDropDown = By
				.xpath("//button[@id='ctl00_ContentPlaceHolder1_MainContent_cbCustomerName_Button']");
		customerName = By.xpath("//li[text()='International SOS']");

		myTripsTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkPTL']");
		manualTripEntryTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkMTE']");
		profileGrouptab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE']");
		profileFieldtab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE']");
		segmentationTab = By
				.xpath("//a[@id='ctl00_ContentPlaceHolder1_AdminMiddleBarControl_lnkSegmentation']");
		assignGrpUserTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser']");
		userTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser']");
		selectUserUserTab = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_ddlUser']");
		userNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtUserName']");
		firstNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtFirstName']");
		lastNameUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtLastName']");
		emailAddressUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_txtEmailAddress']");
		emailStatus = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_rdStatus_0']");
		updateBtnUserTab = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnUserSave']");
		successMsgUserTab = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_lblMessage']");
		assignRolesToUsersTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment']");
		selectUserRoleTab = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_drpUser']");
		availableRole = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstAvailableRole']");
		selectedRole = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_lstSelectedRole']");
		appRoleName = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtRoleName']");
		appRoleDesc = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserRoleAssignment_ctl01_txtDescription']");
		assignUsersToRoleTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole']");
		appRoleDropDown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_drpUserRole']");
		roleDesc = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_txtDescription']");
		availableUsers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstAvailableUsers']");
		selectedUsers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabAssignUsersToRole_ctl01_lstSelectedUsers']");
		profileOptionsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabProfileOptions']");
		profileField = By.xpath("//select[@id='drpProfileField']");
		dropDownOptions = By.xpath("//input[@id='txtOptions']");
		userMigrationTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration']");
		migrateUsersBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUserMigration_ctl01_btnMigrate']");
		promptedCheckInExclusionsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions']");
		headerMsg = By
				.xpath("//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabPromptedCheckInExclusions_panPromptedCheckInExclusions']/h3");
		progressImage = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_UpdateProgress1']");
		selectProfileGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_listBoxSelectedGroups']/option[1]");
		profileGroupLabel = By.xpath("//input[@id='txtLabelValue']");
		updateLabelButton = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnUpdateAttributeGroupLabel']");
		groupNameSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_lblLabelMsg']");
		profileGroupSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_btnSave']");
		profileGroupSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrGroupMTE_ctl01_labelErrorMessage']");
		attributeGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_ddlAttributeGroup']");
		availableProfileFields = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_lstbAvailableProfileAttributes']");
		selectedProfileFields = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']");
		labelForProfileField = By.xpath("//input[@id='txtFieldValue']");
		businessLocation = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[1]");
		isRequiredOnFormCheckbox = By
				.xpath("//input[@id='chkIsRequiredProfileField']");
		profileFieldHomeSite = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[4]");
		profileFieldUpdateLabel = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnUpdateLabel']");
		profileFieldUpdateSuccessMsg = By
				.xpath("//span[@id='ProfileAttributelblLabelMsg']");
		profileFieldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfAttrFieldMTE_ctl01_btnSave']");
		profileFieldSuccessMsg = By.xpath("//span[@id='lblMessage']");
		tripSegmentsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE']");
		availableSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxAvailableSegment']");
		selectedSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlViewSegmentMTE_ctl01_listBoxSelectedSegment']");
		unmappedProfileFieldsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE']");
		availableFields = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_panelProfileExtendedAttributesGroupsMTE']/table//tr[1]/td/table/tbody/tr[2]/td[1]");
		selectedFields = By
				.xpath("//select[@id='listBoxSelectedExtendedAttributes']");
		accountNo = By
				.xpath("//select[@id='listBoxAvailableExtendedAttributes']/option[1]");
		addBtn = By.xpath("//input[@id='btnAdd']");
		umappedProFldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlProfileExtendedAttributesGroupsMTE_ctl01_btnSave']");
		umappedProFldSuccessMsg = By
				.xpath("//span[@id='labelExtendedAttributeErrorMessage']");
		metadataTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE']");
		availableMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbAvailableProfileAttributes']");
		selectedMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_lstbSelectedProfileAttributes']");
		labelName = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_txtLabelValue']");
		responseType = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmMetaMTE_ctl01_ddlResponseType']");
		customTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE']");
		newBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnNew']");
		defaultQstn = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lstbSelectedProfileAttributes']/option[text()='Default Question Text']");
		qstnText = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_txtCustomTripQuestionValue']");

		responseTypeCTQ = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_ddlResponseType']");
		applySettings = By.xpath("//input[@id='chkSetOnToClone']");
		updateCustomTripQstn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnUpdateLabel']");
		saveChangesBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_btnSave']");
		CTQSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerMTE_tabPnlCustmQtnMTE_ctl01_lblMessage']");
		
		assignUserToGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup']");
		addEditGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup']");
		addEditFilterTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter']");
		assignFiltersToGrpTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup']");
		selectGrpDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_drpGroup']");
		saveaddEditGrpBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_btnSave']");
		addEditGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAddEditGroup_ctl01_lblMessage']");
		filterDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_drpFilter']");
		saveaddEditFilterBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_btnFilter']");
		addEditFilterSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPaneAddEditFilter_ctl01_lblErrorMessage']");
		grpDropdown = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_drpGroup']");
		availableFilter = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lstAvailableFilters']/option[10]");
		moveFilter = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnAdd']");
		saveAssignToFiltersBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_btnSave']");
		assignFiltersToGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignFilterToGroup_ctl01_lblMessage']");
		user = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_drpUser']");
		availableGrp = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lstAvailableGroup']/option[10]");
		moveGrp = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnAdd']");
		saveAssignGrpToUserBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_btnSave']");
		assignGrpToUserSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignGroupToUser_ctl01_lblMessage']");
		group = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_drpGroup']");
		availableUser = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lstAvailableUsers']/option[11]");
		moveUser = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnAdd']");
		saveAssignUserToGrpBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_btnSave']");
		assignUserToGrpSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerSegmentation_tabPanelAssignUserToGroup_ctl01_lblMessage']");
		
		emulateUserBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStartEmulation']");
		welcomeCust = By
				.xpath(".//span[contains(text(),'Welcome')]");
		stopEmulationBtn = By
				.xpath(".//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabUser_ctl01_btnStopEmulation']");
		
		assistanceAppCheckBox = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_chkLocationTrackingEnabled");
		updateBtnCustTab = By
				.id("ctl00_ContentPlaceHolder1_MainContent_tabContainerGeneral_tabClient_ctl01_btnSave");
		checkInsTab = By
				.id("checkInsTab");
		blueManImagecheckInsTab = By
				.xpath("//img[contains(@src,'blue_man')]");
		
	}
}
