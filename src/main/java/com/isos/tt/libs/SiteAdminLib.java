package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class SiteAdminLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();

	@SuppressWarnings("unchecked")
	public boolean verifySiteAdminpage(String custName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			System.out.println("customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page", 60));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(click(SiteAdminPage.customerName, "Customer Name"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab", 60));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Site Admin Page verification is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Site Admin Page is failed.");
		}
		return flag;
	}

	/**
	 * This method is used to add two integers. This is a the simplest form of a
	 * class method, just to show the usage of various javadoc Tags.
	 * 
	 * @return boolean This returns boolean value.
	 * @throws SiteAdminLib
	 *             throws an exception
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntryTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Manual TripEntry Tab"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(assertElementPresent(SiteAdminPage.profileGrouptab, "Profile Group Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Manual Trip Entry successfully.");
			LOG.info("verifyManualTripEntryTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Manual Trip Entry verificatin failed.");
			componentActualresult.add("verifyManualTripEntryTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyManualTripEntryTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMyTripsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMyTripsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(click(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileFieldsTab, "Profile Fields Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab, "Profile Fields Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified MyTrips page successfully.");
			LOG.info("verifyMyTripsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("MyTrips page verification failed.");
			componentActualresult.add("verifyMyTripsPage verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyMyTripsPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifySegmentationPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySegmentationPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab, "segmentation Tab"));
			flags.add(click(SiteAdminPage.segmentationTab, "segmentation Tab"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.assignGrpUserTab, "Assign Group To User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.assignGrpUserTab, "Assign Group To User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Segmentation page successfully.");
			LOG.info("verifySegmentationPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Segmentation page verification failed.");
			componentActualresult.add("verifySegmentationPage verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifySegmentationPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean getNewUserRegURL() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("getNewUserRegURL component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToDefaultFrame());
			Shortwait();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(click(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOS, "International SOS"));
			flags.add(click(TravelTrackerHomePage.intlSOS, "International SOS"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSURL, "International SOS URL"));
			String url = getText(TravelTrackerHomePage.intlSOSURL, "International SOS URL");
			
			flags.add(switchToDefaultFrame());
			Driver.navigate().to(url);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration URL is fetched successfully.");
			LOG.info("getNewUserRegURL component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration URL fetching failed.");
			componentActualresult.add("getNewUserRegURL verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("getNewUserRegURL component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickSiteAdmin() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSiteAdmin component execution Started");
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			LOG.info("clickSiteAdmin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("clickSiteAdmin verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("clickSiteAdmin component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickSegmentationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSegmentationTab component execution Sttarted");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab, "Segmentation Tab"));
			flags.add(click(SiteAdminPage.segmentationTab, "Segmentation Tab"));
			flags.add(waitForElementPresent(SiteAdminPage.assignGrpUserTab, "Assign Group User Tab", 60));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Segmentation tab is clicked successfully.");
			LOG.info("clickSegmentationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Segmentation tab failed.");
			componentActualresult.add("clickSegmentationTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("clickSegmentationTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateUserTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "Select User from User Tab"));
			flags.add(selectByIndex(SiteAdminPage.selectUserUserTab, 3, "Select User from User Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.userNameUserTab, "User Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.firstNameUserTab, "First Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.lastNameUserTab, "Last Name from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailAddressUserTab, "Email Address from User Tab"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus, "Email Status from User Tab"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastNameUserTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddressUserTab));

			flags.add(click(SiteAdminPage.updateBtnUserTab, "Update button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.successMsgUserTab, "Success Message in User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User tab is successful");
			LOG.info("navigateUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab failed.");
			componentActualresult.add("navigateUserTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigateUserTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateAssignRolesToUsersTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateAssignRolesToUsersTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(click(SiteAdminPage.assignRolesToUsersTab, "Assign Roles To Users Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.selectUserRoleTab, "Select User from Role Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.selectUserRoleTab, "Select User from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.availableRole, "Available Role from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.selectedRole, "Selected Role from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleName, "Application Role Name from Role Tab"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDesc, "Application Role Description from Role Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Roles To Users Tab is successful");
			LOG.info("navigateAssignRolesToUsersTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Roles To Users Tab failed");
			componentActualresult.add("navigateAssignRolesToUsersTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigateAssignRolesToUsersTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateAssignUsersToRolesTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateAssignUsersToRolesTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.assignUsersToRoleTab, "Assign Users To Role Tab"));
			flags.add(click(SiteAdminPage.assignUsersToRoleTab, "Assign Users To Role Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.appRoleDropDown, "appRoleDropDown", 60));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDropDown, "Application Role Drop Down"));
			flags.add(assertElementPresent(SiteAdminPage.roleDesc, "Role Description"));
			flags.add(assertElementPresent(SiteAdminPage.availableUsers, "Available Users"));
			flags.add(assertElementPresent(SiteAdminPage.selectedUsers, "Selected Users"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Users To Roles Tab is successful");
			LOG.info("navigateAssignUsersToRolesTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Users To Roles Tab failed");
			componentActualresult.add("navigateAssignUsersToRolesTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigateAssignUsersToRolesTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateProfileOptionsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateProfileOptionsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			flags.add(click(SiteAdminPage.profileOptionsTab, "Profile Options Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileField, "Profile Field in Profile Options Tab"));
			flags.add(assertElementPresent(SiteAdminPage.dropDownOptions, "Drop Down Options in Profile Options Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Options Tab is successful");
			LOG.info("navigateProfileOptionsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Options Tab failed");
			componentActualresult.add("navigateProfileOptionsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigateProfileOptionsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateUserMigrationTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigateUserMigrationTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.userMigrationTab, "User Migration Tab"));
			flags.add(click(SiteAdminPage.userMigrationTab, "User Migration Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.migrateUsersBtn, "Migrate Users Button in User Migration Tab",
					60));
			flags.add(
					assertElementPresent(SiteAdminPage.migrateUsersBtn, "Migrate Users Button in User Migration Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User Migration Tab is successful");
			LOG.info("navigateUserMigrationTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User Migration Tab failed");
			componentActualresult.add("navigateUserMigrationTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigateUserMigrationTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigatePromptedCheckinExclusionsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("navigatePromptedCheckinExclusionsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.promptedCheckInExclusionsTab, "Prompted Check-in Exclusions Tab"));
			flags.add(click(SiteAdminPage.promptedCheckInExclusionsTab, "Prompted Check-in Exclusions Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.headerMsg,
					"Header Message in Prompted Check-in Exclusions Tab", 60));
			flags.add(isElementPresent(SiteAdminPage.headerMsg, "Header Message in Prompted Check-in Exclusions Tab"));
			flags.add(assertElementPresent(SiteAdminPage.headerMsg,
					"Header Message in Prompted Check-in Exclusions Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Prompted CheckIn Exclusions Tab is successful");
			LOG.info("navigatePromptedCheckinExclusionsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Prompted CheckIn Exclusions Tab failed");
			componentActualresult.add("navigatePromptedCheckinExclusionsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("navigatePromptedCheckinExclusionsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileGroupTab(String profileGroupLabelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileGroupTab component execution Completed");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab, "Manual Trip Entry"));
			flags.add(click(SiteAdminPage.manualTripEntryTab, "Manual Trip Entry"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(waitForElementPresent(SiteAdminPage.profileGrouptab, "Profile Group Tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldtab, "Profile Field Tab"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegmentsTab, "Trip Segments"));
			flags.add(assertElementPresent(SiteAdminPage.unmappedProfileFieldsTab, "Unmapped Profile Fields tab"));
			flags.add(assertElementPresent(SiteAdminPage.metadataTripQuestionTab, "Metadata Trip Question Tab"));
			flags.add(assertElementPresent(SiteAdminPage.customTripQuestionTab, "Custom Trip Question Tab"));
			flags.add(click(SiteAdminPage.profileGrouptab, "Profile Group Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.selectProfileGroup, "Select Profile Group", 60));
			flags.add(click(SiteAdminPage.selectProfileGroup, "Select Profile Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(isElementPopulated_byValue(SiteAdminPage.profileGroupLabel));
			flags.add(type(SiteAdminPage.profileGroupLabel, profileGroupLabelName, "Profile Group Label"));
			flags.add(click(SiteAdminPage.updateLabelButton, "Update Label Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.groupNameSuccessMsg, "Group Name Success Message"));
			flags.add(click(SiteAdminPage.profileGroupSaveBtn, "Profile Group Save Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.profileGroupSuccessMsg, "Profile Group Success Message", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileGroupSuccessMsg, "Profile Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Group Tab is successful");
			LOG.info("verifyProfileGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Group Tab failed");
			componentActualresult.add("verifyProfileGroupTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyProfileGroupTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileFieldsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.tripSegmentsTab, "Trip Segments Tab under Manual Trip Entry tab"));
			flags.add(click(SiteAdminPage.profileFieldtab, "Profile Field Tab under Manual Trip Entry tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableProfileFields,
					"Available Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.selectedProfileFields,
					"Selected Profile Fields under Manual Trip Entry tab"));
			flags.add(assertElementPresent(SiteAdminPage.labelForProfileField, "Label For Profile Field"));
			flags.add(selectByIndex(SiteAdminPage.attributeGroup, 1, "Attribute Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.availableProfileFields, "Available Profile Fields", 60));
			flags.add(isElementPopulated_byText(SiteAdminPage.availableProfileFields));
			flags.add(isElementPopulated_byText(SiteAdminPage.selectedProfileFields));
			flags.add(click(SiteAdminPage.profileFieldHomeSite, "Profile Field Home Site"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(isElementPopulated_byValue(SiteAdminPage.labelForProfileField));
			flags.add(click(SiteAdminPage.businessLocation, "Business Location"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.isRequiredOnFormCheckbox, "IsRequired On Form Checkbox", 60));
			flags.add(click(SiteAdminPage.isRequiredOnFormCheckbox, "IsRequired On Form Checkbox"));
			flags.add(click(SiteAdminPage.profileFieldUpdateLabel, "Profile Field Update Label"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldUpdateSuccessMsg,
					"Profile Field Update Success Message"));
			flags.add(click(SiteAdminPage.profileFieldSaveBtn, "profileFieldSaveBtn"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Profile Field Success Message", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldSuccessMsg, "Profile Field Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Fields Tab is successful");
			LOG.info("verifyProfileFieldsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Fields Tab failed");
			componentActualresult.add("verifyProfileFieldsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyProfileFieldsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTripSegmentsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifytripSegmentsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.tripSegmentsTab, "Trip Segments Tab under Manual Trip Entry tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.availableSegments, "Available Segments", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableSegments, "Available Segments"));
			flags.add(assertElementPresent(SiteAdminPage.selectedSegments, "Selected Segments"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Trip Segments Tab is successful");
			LOG.info("verifytripSegmentsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Trip Segments Tab Failed.");
			componentActualresult.add("verifytripSegmentsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifytripSegmentsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUnmappedProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnmappedProfileFieldsTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.unmappedProfileFieldsTab, "Unmapped Profile Fields Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.availableFields, "Available Fields", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableFields, "Available Fields"));
			flags.add(assertElementPresent(SiteAdminPage.selectedFields, "Selected Fields"));
			flags.add(click(SiteAdminPage.accountNo, "Account Number"));
			flags.add(click(SiteAdminPage.addBtn, "Add Button"));
			flags.add(click(SiteAdminPage.umappedProFldSaveBtn, "Umapped ProFld Save Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(
					waitForElementPresent(SiteAdminPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message", 60));
			flags.add(assertElementPresent(SiteAdminPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
			LOG.info("verifyUnmappedProfileFieldsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Unmapped Profile Fields Tab Failed.");
			componentActualresult.add("verifyUnmappedProfileFieldsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyUnmappedProfileFieldsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMetadataTripQstnTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMetadataTripQstnTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.metadataTripQuestionTab, "Metadata Trip Question Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.availableMetadataTripQstns,
					"Available Metadata Trip Questions", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableMetadataTripQstns,
					"Available Metadata Trip Questions"));
			flags.add(
					assertElementPresent(SiteAdminPage.selectedMetadataTripQstns, "Selected Metadata Trip Questions"));
			flags.add(assertElementPresent(SiteAdminPage.labelName, "Label Name"));
			flags.add(assertElementPresent(SiteAdminPage.responseType, "Response Type"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of MetaData Trip Questions Tab is successful");
			LOG.info("verifyMetadataTripQstnTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of MetaData Trip Questions Tab Failed.");
			componentActualresult.add("verifyMetadataTripQstnTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyMetadataTripQstnTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCustomTripQstnTab(String defaultQstnText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomTripQstnTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.customTripQuestionTab, "customTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.newBtn, "New Button", 60));
			flags.add(click(SiteAdminPage.newBtn, "New Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.defaultQstn, "Default Question ", 60));
			flags.add(click(SiteAdminPage.defaultQstn, "Default Question "));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.qstnText, "Question Text", 60));
			flags.add(type(SiteAdminPage.qstnText, defaultQstnText, "Question Text"));
			flags.add(selectByIndex(SiteAdminPage.responseTypeCTQ, 1, "Response Type Drop Down"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.applySettings, "Apply Settings Checkbox", 60));
			flags.add(click(SiteAdminPage.applySettings, "Apply Settings Checkbox"));
			flags.add(click(SiteAdminPage.updateCustomTripQstn, "Update Custom Trip Question Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.saveChangesBtn, "Save Changes Button", 60));
			flags.add(click(SiteAdminPage.saveChangesBtn, "Save Changes Button"));
			flags.add(waitForAlertToPresent());
			flags.add(accecptAlert());
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message", 60));
			flags.add(assertElementPresent(SiteAdminPage.CTQSuccessMsg, "Custom Trip Question Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
			LOG.info("verifyCustomTripQstnTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Custom Trip Questions Tab Failed.");
			componentActualresult.add("verifyCustomTripQstnTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyCustomTripQstnTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileGroupTabMyTrips(String profileGroupLabelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileGroupTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			Shortwait();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab, "General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab, "General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown, "Customer drop down"));
			flags.add(click(SiteAdminPage.customerName, "Customer Name"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(SiteAdminPage.myTripsTab, "My Trips Tab", 60));
			flags.add(isElementPresent(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(click(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab, "Profile Field Tab"));
			flags.add(assertElementPresent(MyTripsPage.tripSegmentsTab, "Trip Segments Tab"));
			flags.add(assertElementPresent(MyTripsPage.unmappedProfileFieldsTab, "Unmapped Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.metadataTripQuestionTab, "Metadata Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.customTripQuestionTab, "Custom Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.authorizedMyTripsCustTab, "Custom Trip Question"));
			flags.add(click(MyTripsPage.profileGroupTab, "Profile Group Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.selectProfileGroup, "Select Profile Group", 60));
			flags.add(click(MyTripsPage.selectProfileGroup, "Select Profile Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.profileGroupLabel, "Profile Group Label", 60));
			flags.add(isElementPopulated_byValue(MyTripsPage.profileGroupLabel));
			flags.add(type(MyTripsPage.profileGroupLabel, profileGroupLabelName, "Profile Group Label"));
			flags.add(click(MyTripsPage.updateLabelButton, "Update Label Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.groupNameSuccessMsg, "Group Name Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.groupNameSuccessMsg, "Group Name Success Message"));
			flags.add(click(MyTripsPage.profileGroupSaveBtn, "Profile Group Save Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.profileGroupSuccessMsg, "Profile Group Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.profileGroupSuccessMsg, "Profile Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Group Tab is successful");
			LOG.info("verifyProfileGroupTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Profile Group Tab Failed.");
			componentActualresult.add("verifyProfileGroupTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyProfileGroupTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileFieldsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyProfileFieldsTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.profileFieldsTab, "Profile Field Tab under My Trips Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields", 60));
			flags.add(assertElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.selectedProfileFields, "Selected Profile Fields"));
			flags.add(assertElementPresent(MyTripsPage.labelForProfileField, "Label For Profile Field"));
			flags.add(selectByIndex(MyTripsPage.attributeGroup, 1, "Attribute Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.availableProfileFields, "Available Profile Fields", 60));
			flags.add(isElementPopulated_byText(MyTripsPage.availableProfileFields));
			flags.add(isElementPopulated_byText(MyTripsPage.selectedProfileFields));
			flags.add(click(MyTripsPage.genderOption, "profileFieldHomeSite"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.labelForProfileField, "Label For Profile Field", 60));
			flags.add(isElementPopulated_byValue(MyTripsPage.labelForProfileField));
			flags.add(click(MyTripsPage.profileFieldMiddleName, "Business Location"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.isRequiredOnFormCheckbox, "Is Required On Form Checkbox", 60));
			flags.add(click(MyTripsPage.isRequiredOnFormCheckbox, "Is Required On Form Checkbox"));
			flags.add(click(MyTripsPage.profileFieldUpdateLabel, "Profile Field Update Label"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldUpdateSuccessMsg,
					"Profile Field Update Success Message"));
			flags.add(click(MyTripsPage.profileFieldSaveBtn, "Profile Field Save Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.profileFieldSuccessMsg, "Profile Field Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.profileFieldSuccessMsg, "Profile Field Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Fields Tab is successful");
			LOG.info("verifyProfileFieldsTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Profile Fields Tab Failed.");
			componentActualresult.add("verifyProfileFieldsTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyProfileFieldsTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTripSegmentsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifytripSegmentsTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.tripSegmentsTab, "tripSegmentsTab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.availableSegments, "Available Segments", 60));
			flags.add(assertElementPresent(MyTripsPage.availableSegments, "Available Segments"));
			flags.add(assertElementPresent(MyTripsPage.selectedSegments, "Selected Segments"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Trip Segments Tab is successful");
			LOG.info("verifytripSegmentsTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Trip Segments Tab Failed.");
			componentActualresult.add("verifytripSegmentsTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifytripSegmentsTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUnmappedProfileFieldsTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUnmappedProfileFieldsTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.unmappedProfileFieldsTab, "Unmapped ProfileFields Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.availableFields, "Available Fields", 60));
			flags.add(assertElementPresent(MyTripsPage.availableFields, "Available Fields"));
			flags.add(assertElementPresent(MyTripsPage.selectedFields, "Selected Fields"));
			flags.add(click(MyTripsPage.accountNo, "Account Number"));
			flags.add(click(MyTripsPage.arrowToMoveRight, "Arrow To Move Right Button"));
			flags.add(click(MyTripsPage.umappedProFldSaveBtn, "Umapped ProFld Save Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.umappedProFldSuccessMsg, "Umapped ProFld Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
			LOG.info("verifyUnmappedProfileFieldsTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Unmapped Profile Fields Tab Failed.");
			componentActualresult.add("verifyUnmappedProfileFieldsTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyUnmappedProfileFieldsTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMetadataTripQstnTabMyTrips() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMetadataTripQstnTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.metadataTripQuestionTab, "Metadata Trip Question Tab under My Trips Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.availableMetadataTripQstns, "Available Metadata Trip Question",
					60));
			flags.add(assertElementPresent(MyTripsPage.availableMetadataTripQstns, "Available Metadata Trip Question"));
			flags.add(assertElementPresent(MyTripsPage.selectedMetadataTripQstns, "Selected Metadata Trip Questions"));
			flags.add(assertElementPresent(MyTripsPage.labelName, "Label Name"));
			flags.add(assertElementPresent(MyTripsPage.responseType, "Response Type"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Metadata Trip Questions Tab is successful");
			LOG.info("verifyMetadataTripQstnTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Metadata Trip Questions Tab Failed.");
			componentActualresult.add("verifyMetadataTripQstnTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyMetadataTripQstnTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCustomTripQstnTabMyTrips(String defaultQstnText) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCustomTripQstnTab_MyTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.customTripQuestionTab, "Custom Trip Question Tab under My Trips Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.newTripQstnBtn, "New Trip Question Button", 60));
			flags.add(click(MyTripsPage.newTripQstnBtn, "New Trip Question Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.defaultQstnText, "Default Question Text", 60));
			flags.add(click(MyTripsPage.defaultQstnText, "Default Question Text"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.qstnTextArea, "Question Text Area", 60));
			flags.add(type(MyTripsPage.qstnTextArea, defaultQstnText, "Question Text Area"));
			flags.add(selectByIndex(MyTripsPage.responseTypeCTQ, 1, "Response Type Custom Trip Question"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.applySettings, "Apply Settings Checkbox", 60));
			flags.add(click(MyTripsPage.applySettings, "Apply Settings Checkbox"));
			flags.add(click(MyTripsPage.updateCustomTripQstn, "Update Custom Trip Question"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.saveChangesBtn, "Save Changes Button", 60));
			flags.add(click(MyTripsPage.saveChangesBtn, "Save Changes Button"));
			flags.add(waitForAlertToPresent());
			flags.add(accecptAlert());
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.CTQSuccessMsg, "Custom Trip Question Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.CTQSuccessMsg, "Custom Trip Question Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
			LOG.info("verifyCustomTripQstnTab_MyTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Custom Trip Questions Tab Failed.");
			componentActualresult.add("verifyCustomTripQstnTab_MyTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyCustomTripQstnTab_MyTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifAuthorizedMyTripsCustomers() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifAuthorizedMyTripsCustomers component execution Completed");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(MyTripsPage.authorizedMyTripsCustTab, "Custom Trip Question Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.intlSOS, "IntlSOS Customer", 60));
			flags.add(click(TravelTrackerHomePage.intlSOS, "intlSOS"));
			flags.add(assertElementPresent(MyTripsPage.availableCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.selectedCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.encryptedCustomerId,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSURL,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Authorized My Trips Customers is successful");
			LOG.info("verifAuthorizedMyTripsCustomers component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Authorized My Trips Customers Failed.");
			componentActualresult.add("verifAuthorizedMyTripsCustomers verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifAuthorizedMyTripsCustomers component execution Completed");
		}
		return flag;
	}

	/*
	 * public static void setComponentStartTimer() { SimpleDateFormat sdf = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date date = new Date();
	 * component_startTime = sdf.format(date); }
	 * 
	 * public static void setComponentEndTimer() { SimpleDateFormat sdf = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date date = new Date();
	 * component_endTime = sdf.format(date); }
	 */

	@SuppressWarnings("unchecked")
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("typeNewsReportName component execution Started");
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				
			} else {
				enterTextIntoAlert(chars);
			}
			LOG.info("typeNewsReportName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("typeNewsReportName verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("typeNewsReportName component execution Completed");

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAssignUserToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyassignUserToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignUserToGrpTab, "Assign User To Group Tab under Segmentation Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.group, "Group"));
			flags.add(selectByIndex(SiteAdminPage.group, 3, "Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.availableUser, "Available User"));
			flags.add(click(SiteAdminPage.moveUser, "Move User"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn, "Save Assign User To Group Btn"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.assignUserToGrpSuccessMsg,
					"Assign User To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigated to Assign User To Group Tab successfully.");
			LOG.info("verifyassignUserToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign User To Group Tab failed.");
			componentActualresult.add("verifyassignUserToGroupTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyassignUserToGroupTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAssignGroupToUserTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignGroupToUserTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignGrpUserTab, "assignGrpUserTab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByIndex(SiteAdminPage.user, 3, "User"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.availableGrp, "Available Group"));
			flags.add(click(SiteAdminPage.moveGrp, "Move Group"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn, "Save Assign Group To User Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.assignGrpToUserSuccessMsg,
					"Assign Group To User Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigated to Assign Group To User Tab successfully.");
			LOG.info("verifyassignGroupToUserTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Group To User Tab failed.");
			componentActualresult.add("verifyassignGroupToUserTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyAssignGroupToUserTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAddEditGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddEditGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditGrpTab, "addEditGrpTab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDropdown, "Select Group Drop Down"));
			flags.add(selectByIndex(SiteAdminPage.selectGrpDropdown, 3, "Select Group Drop Down"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.saveaddEditGrpBtn, "Save Add Edit Group Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.addEditGrpSuccessMsg, "Add Edit Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigated to Add/Edit Group Tab successfully.");
			LOG.info("verifyAddEditGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Add/Edit Group Tab failed.");
			componentActualresult.add("verifyAddEditGroupTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyAddEditGroupTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAddEditFilterTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAddEditFilterTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.addEditFilterTab, "Add Edit Filter Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown, "Filter Dropdown"));
			flags.add(selectByIndex(SiteAdminPage.filterDropdown, 5, "Filter Dropdown"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.saveaddEditFilterBtn, "Save add Edit Filter Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.addEditFilterSuccessMsg, "Add Edit Filter Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigated to Add/Edit Filter Tab successfully.");
			LOG.info("verifyAddEditFilterTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Add/Edit Filter Tab failed.");
			componentActualresult.add("verifyAddEditFilterTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyAddEditFilterTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAssignFiltersToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAssignFiltersToGroupTab component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab, "Assign Filters To Group Tab"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown, "Group Drop Down"));
			flags.add(selectByIndex(SiteAdminPage.grpDropdown, 6, "Group Drop Down"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.availableFilter, "Available Filter"));
			flags.add(click(SiteAdminPage.moveFilter, "Move Filter"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn, "Save Assign To Filters Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(assertElementPresent(SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"Assign Filters To Group Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigated to Assign Filters To Group Tab successfully.");
			LOG.info("verifyAssignFiltersToGroupTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Filters To Group Tab failed.");
			componentActualresult.add("verifyAssignFiltersToGroupTab verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("verifyAssignFiltersToGroupTab component execution Completed");
		}
		return flag;
	}	

}
