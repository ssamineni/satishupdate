package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class NewsLib extends CommonLib {

	private static final Logger LOG = Logger.getLogger(NewsLib.class);

	@SuppressWarnings("unchecked")
	public boolean openContextEditorTree() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("openContextEditorTree component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.ContentTree, "Content Tree"));
			flags.add(click(ContentEditorPage.PandemicSiteTree, "Pandemic Site Tree"));
			flags.add(click(ContentEditorPage.PandemicTree, "Pandemic Tree"));
			flags.add(click(ContentEditorPage.HomeTree, "Home Tree"));
			flags.add(click(ContentEditorPage.NewsEditorialsTree, "News Editorials Tree"));

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("openContextEditorTree component execution Completed");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("openContextEditorTree verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("openContextEditorTree component execution Completed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createReport(String NewsReport) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createReport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			WebElement ele = Driver.findElement(ContentEditorPage.NewsTree);
			if (ele.isDisplayed()) {
				Actions act = new Actions(Driver).contextClick(ele);
				act.build().perform();
				waitForElementPresent(ContentEditorPage.Inserticon, "Insert Icon", 3);
			}
			flags.add(click(ContentEditorPage.Inserticon, "Insert Icon"));
			flags.add(click(ContentEditorPage.NewsReport, "News Report Icon"));
			flags.add(typeNewsReportName(NewsReport));

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("createReport component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("createReport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("createReport component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean fillNewsDetails(String Headline, String ManagerBody, String SummaryBody, String Geocode,
			String CreatedDate, String CreatedTime, String UpdatedDate, String UpdatedTime) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("fillNewsDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.ReportSearch, "Select Search Keyword"));
			flags.add(click(ContentEditorPage.ReportRightImage, "Right Image"));
			waitForVisibilityOfElement(By.id("jqueryModalDialogsFrame"), "Modal frame");
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Switch to ModalDialogs Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Switch to Content IFrame"));
			
			waitForElementPresent(ContentEditorPage.Acceptbutton, "Accept Button", 3);
			flags.add(click(ContentEditorPage.Acceptbutton, "Accept Button"));
			flags.add(switchToDefaultFrame());

			flags.add(type(ContentEditorPage.Geocode, Geocode, "Geocode field"));
			flags.add(click(ContentEditorPage.Disease, "Select Disease"));
			flags.add(click(ContentEditorPage.RightImage, "Right Image"));
			flags.add(type(ContentEditorPage.CreatedDate, CreatedDate, "Created date field"));
			flags.add(type(ContentEditorPage.CreatedTime, CreatedTime, "Created time field"));
			flags.add(type(ContentEditorPage.UpdatedDate, UpdatedDate, "Updated date field"));
			flags.add(type(ContentEditorPage.UpdatedTime, UpdatedTime, "Updated time field"));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			flags.add(click(ContentEditorPage.reviewMenu, "Review Tab"));
			flags.add(click(ContentEditorPage.SubmitButton, "Submit button"));
			flags.add(click(ContentEditorPage.ApproveButton, "Approve button"));
			flags.add(click(ContentEditorPage.publishTab, "Publish Tab"));
			flags.add(click(ContentEditorPage.publishIcon, "Publish Icon"));
			flags.add(click(ContentEditorPage.publishItem, "Publish Item"));
			sleep(6000);
			/*WebElement RFrame = Driver.findElement(By.id("jqueryModalDialogsFrame"));
			Driver.switchTo().frame(RFrame);
			WebElement sRFrame = Driver.findElement(By.id("scContentIframeId0"));
			Driver.switchTo().frame(sRFrame);*/
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Switch to ModalDialogs Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Switch to Content IFrame"));
			flags.add(click(ContentEditorPage.publishButton, "Publish Button"));
			flags.add(click(ContentEditorPage.finishButton, "Finish Button"));
			//Driver.switchTo().defaultContent();
			flags.add(switchToDefaultFrame());

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("News details are filled successfully.");
			LOG.info("fillNewsDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("Issue with filling News details.");
			componentActualresult.add("fillNewsDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("fillNewsDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			LOG.info("typeNewsReportName component execution Started");
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// LOG.info("createReport took " + duration +
				// " milliseconds");
			} else {
				/*Alert alert = Driver.switchTo().alert();
				alert.sendKeys(chars);
				alert.accept();*/
				enterTextIntoAlert(chars);
			}
			LOG.info("typeNewsReportName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("typeNewsReportName verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("typeNewsReportName component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyData(String Headline) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyData component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			String winname = Driver.getWindowHandle();
			JavascriptExecutor js = (JavascriptExecutor) Driver;
			js.executeScript("window.open('http://ecms-qa-pandemic.intlsos.com/','_blank');");
			ArrayList<String> windows = new ArrayList<String>(Driver.getWindowHandles());
			Driver.switchTo().window(windows.get(1));
			flags.add(click(ContentEditorPage.Newsroom, "News room tab"));
			flags.add(click(ContentEditorPage.News, "News icon"));
			flag = assertTextMatching(ContentEditorPage.PublishedNews, Headline, "Published News verification");
			Driver.close();
			Driver.switchTo().window(winname);

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Data is verified successfully.");
			LOG.info("verifyData component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("Data verification is failed.");
			componentActualresult.add("verifyData verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyData component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public boolean accessAlertReport(String PandemicMember) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("accessAlertReport component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			String LNewsHeader = getText(ContentEditorPage.PandemicLatestNews, "latest news");
			// Driver.findElement(ContentEditorPage.PandemicLatestNews).getText();
			flags.add(click(ContentEditorPage.PandemicLatestNews, "Latest News"));
			flags.add(type(ContentEditorPage.PandemicMembershipNo, PandemicMember, "Pandemic Membership Number"));
			flags.add(click(ContentEditorPage.PandemicLogIn, "Log in"));
			String[] header = LNewsHeader.split(":");
			flag = assertTextMatching(ContentEditorPage.LatestNewsHeader, header[1].toUpperCase(),
					"Latest News Header Verification");

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("accessAlertReport component execution Completed");
		} catch (Exception e) {
			flag = false;
			componentStartTimer.add(getCurrentTime());
			e.printStackTrace();
			componentActualresult.add("accessAlertReport verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("accessAlertReport component execution Completed");
		}
		return flag;

	}

	@SuppressWarnings("unchecked")
	public boolean openMicrositeTree() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("openMicrositeTree component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.ContentTree, "Content Tree"));
			flags.add(click(ContentEditorPage.MicrositesTree, "Microsite Tree"));
			flags.add(click(ContentEditorPage.SMicrositeTree, "Sub Tree"));
			flags.add(click(ContentEditorPage.HomeIcon, "Home Icon"));

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("openMicrositeTree component execution Completed");

		} catch (Exception e) {
			flag = false;
			componentStartTimer.add(getCurrentTime());
			e.printStackTrace();
			componentActualresult.add("openMicrositeTree verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("openMicrositeTree component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean editSite(String headlineParam) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("editSite component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			new ContentEditorPage().ContentEditor_Page();
			List<Boolean> flags = new ArrayList<>();
			flags.add(type(ContentEditorPage.HeadlineField, headlineParam, "Headline Field"));
			flags.add(click(ContentEditorPage.Search, "Select Search Keyword"));
			flags.add(click(ContentEditorPage.SearchRightImage, "Search Right Image"));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			flags.add(click(ContentEditorPage.publishTab, "Publish Tab"));
			flags.add(click(ContentEditorPage.publishIcon, "Publish Icon"));
			flags.add(click(ContentEditorPage.publishItem, "Publish Item"));
			sleep(6000);
			/*WebElement MFrame = Driver.findElement(By.id("jqueryModalDialogsFrame"));
			Driver.switchTo().frame(MFrame);
			WebElement sFrame = Driver.findElement(By.id("scContentIframeId0"));
			Driver.switchTo().frame(sFrame);*/
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Switch to ModalDialogs Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Switch to Content IFrame"));
			
			flags.add(click(ContentEditorPage.publishButton, "Publish Button"));
			flags.add(click(ContentEditorPage.finishButton, "Finish Button"));
			//Driver.switchTo().defaultContent();
			flags.add(switchToDefaultFrame());

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("editSite component execution Completed");
		} catch (Exception e) {
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			flag = false;
			componentActualresult.add("editSite verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("editSite component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUpdate(String headLineParam) throws IOException {

		boolean flag = true;
		try {
			LOG.info("verifyUpdate component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.ContentTree, "Content Tree"));
			flags.add(click(ContentEditorPage.MicrositesTree, "Microsite Tree"));
			flags.add(click(ContentEditorPage.SMicrositeTree, "Sub Tree"));
			flags.add(click(ContentEditorPage.HomeIcon, "Home Icon"));
			flag = assertTextMatchingWithAttribute(ContentEditorPage.HeadlineField, headLineParam,
					"Updated Headline Value");

			componentStartTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyUpdate component execution Completed");
		} catch (Throwable e) {
			flag = false;
			componentStartTimer.add(getCurrentTime());
			e.printStackTrace();
			componentActualresult.add("verifyUpdate verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUpdate component execution Completed");
		}

		return flag;
	}

	public void checkPageIsReadyUsingJavaScript() {
		JavascriptExecutor js = (JavascriptExecutor) Driver;
		// Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString().equals("complete")) {
			LOG.info("Page Is loaded.");
			return;
		}

		// This loop will rotate for 25 times to check If page Is ready after
		// every 1 second.
		// You can replace your value with 25 If you wants to Increase or
		// decrease wait time.
		for (int i = 0; i < 25; i++) {
			try {
				Thread.sleep(1000);
				Shortwait();
			} catch (InterruptedException e) {
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
	}

}
