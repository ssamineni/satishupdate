package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.runtime.directive.Parse;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class TTLib extends CommonLib {

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the Traveler count under default Locations tab
	 */
	public boolean clickTravelerCount() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickTravelercount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(TravelTrackerHomePage.travellerCount, "Traveler Count on the Locations Pane"));
			Shortwait();
			flags.add(click(TravelTrackerHomePage.travellerCount, "Traveler Count on the Locations Pane"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers", 60));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Traveler count.");
			LOG.info("clickTravelercount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Failed to click on Traveler count.");
			componentActualresult.add("clickTravelercount verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickTravelercount component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will logout from the application
	 */
	public boolean logOut() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logOut component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(click(TravelTrackerHomePage.logOff, "LogOff in the Home Page"));
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logOut component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logout failed.");
			componentActualresult.add("logOut verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.info("logOut component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will validate the UI elements of Send Message screen
	 */
	public boolean validateMessageWindow() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateMessageWindow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(TravelTrackerHomePage.messageIcon, "Message Icon in the Traveler Pane"));
			flags.add(click(TravelTrackerHomePage.messageIcon, "Message Icon in the Traveler Pane"));
			flags.add(switchToFrame(TravelTrackerHomePage.messageIFrame, "Switch to messageIFrame"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.subject, "Message Subject in the Send Message Screen"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.messageBody, "Message Body in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.twoSMSResponse,
					"Two SMS Response in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sendMessageButton,
					"Send Message Button in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerName,
					"Traveller Name in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.emailCheckbox, "Email in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.emailCheckbox,
					"Email Checkbox in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.phoneCheckbox, "Phone in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.phoneCheckbox,
					"Phone Checkbox in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageLength,
					"Message Length in the Send Message Screen"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Validate Message Window is successful.");
			LOG.info("validateMessageWindow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Failed to validate Message Window.");
			componentActualresult.add("validateMessageWindow verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("validateMessageWindow component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will send message by providing the message body and subject values
	 * and click on Send Message button
	 */
	public boolean sendMessage(String subject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("sendMessage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			Driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.messageIFrame, "Switch to messageIFrame"));
			flags.add(type(TravelTrackerHomePage.subject, subject, "Message Subject in the Send Message Screen"));
			flags.add(type(TravelTrackerHomePage.messageBody, msgBody, "Message Body in the Send Message Screen"));
			flags.add(click(TravelTrackerHomePage.twoSMSResponse, "Two Way SMS Response in the Send Message Screen"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sendMessageButton,
					"Send Message Button in the Send Message Screen"));
			flags.add(
					JSClick(TravelTrackerHomePage.sendMessageButton, "Send Message Button in the Send Message Screen"));

			Shortwait();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.sendMessageConfirmationlabel,
					"Message sent successfully"));
			flags.add(assertTextMatching(TravelTrackerHomePage.sendMessageConfirmationlabel,
					"Message sent successfully", "Send Message Confirmation"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Send message is successful.");
			LOG.info("sendMessage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Send message is failed.");
			componentActualresult.add("sendMessage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("sendMessage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will close the Message Window
	 */
	public boolean closeMessageWindow() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("closeMessageWindow component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.closePopup, "Close Message Window"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Close Message Window is successful.");
			LOG.info("closeMessageWindow component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Close Message Window is failed.");
			componentActualresult.add("closeMessageWindow verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("closeMessageWindow component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click the Communication History link under Tools link
	 */
	public boolean openCommunicationHistory() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("openCommunicationHistory component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			flags.add(click(TravelTrackerHomePage.communicationHistory, "Communication History Link in the Home Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("open Communication History is successful.");
			LOG.info("openCommunicationHistory component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("open Communication History is failed.");
			componentActualresult.add("openCommunicationHistory verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("openCommunicationHistory component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will validate the message content in the Communication History page.
	 */
	public boolean validateMessageContent(String subject) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("validateMessageContent component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(assertElementPresent(TravelTrackerHomePage.messageType,
					"Message Type in the Communication History Page"));
			flags.add(click(TravelTrackerHomePage.searchBtn, "Search Button in the Communication History Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchResults,
					"Search Results in the Communication History Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchResults,
					"Search Results in the Communication History Page"));
			flags.add(assertTextMatching(TravelTrackerHomePage.searchResults, subject,
					"Message Subject in search Results"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Validate Message Content is successful.");
			LOG.info("validateMessageContent component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Validate Message Content is failed.");
			componentActualresult.add("validateMessageContent verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("validateMessageContent component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check all the 4 checkboxes under Filters section of Date
	 * Presets tab with InLocation as the filter and verifies the Traveler count
	 */
	public boolean updateRefineByTravelInfoInLocation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByTravelInfoInLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			int countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply :" + countryCount_BeforeApply);
			flags.add(selectByIndex(TravelTrackerHomePage.location, 1, "Location Dropdown under Filters Button"));
			Shortwait();
			flags.add(isElementPresent(TravelTrackerHomePage.last31Days, "Last 31 Days Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.last31Days, "Last 31 Days Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.next24Hours, "Next 24 Hours Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.next1to7Days, "Next 1 to 7 Days Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.next8to31Days, "Next 8 to 31 Days Checkbox under Filters Button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply : " + countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply)) {

				componentActualresult.add("The traveller count under the left location pane is updated successfully");
			} else
				componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("updateRefineByTravelInfoInLocation component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			componentActualresult.add("updateRefineByTravelInfo_InLocation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByTravelInfoInLocation component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check all the 4 checkboxes under Filters section of Date
	 * Presets tab with Arriving as the filter and verifies the Traveler count
	 */
	public boolean updateRefineByTravelInfoArriving() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByTravelInfoArriving component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			int countryCount_BeforeApply = getCountryCount();

			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.UP);

			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			int countryCount_AfterApply = getCountryCount();

			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("updateRefineByTravelInfoArriving component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			componentActualresult.add("updateRefineByTravelInfo_Arriving verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByTravelInfoArriving component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check all the 4 checkboxes under Filters section of Date
	 * Presets tab with Departing as the filter and verifies the Traveler count
	 */
	public boolean updateRefineByTravelInfoDeparting() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByTravelInfoDeparting component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			int countryCount_BeforeApply = getCountryCount();

			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.DOWN);
			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			int countryCount_AfterApply = getCountryCount();

			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("updateRefineByTravelInfoDeparting component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			componentActualresult.add("updateRefineByTravelInfo_Departing verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByTravelInfoDeparting component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check all the 4 checkboxes under Filters section of Date
	 * Presets tab with Departing & Arriving as the filter and verifies the
	 * Traveler count
	 */
	public boolean updateRefineByTravelInfoDepartingAndArriving() throws Throwable {
		boolean flag = true;
		int countryCount_BeforeApply;
		int countryCount_AfterApply;
		try {
			LOG.info("updateRefineByTravelInfoDepartingAndArriving component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			countryCount_BeforeApply = getCountryCount();

			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.DOWN);
			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.DOWN);
			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			countryCount_AfterApply = getCountryCount();

			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("updateRefineByTravelInfoDepartingAndArriving component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The traveller count under the left location pane is NOT updated ");
			componentActualresult.add("updateRefineByTravelInfo_DepartingAndArriving verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByTravelInfoDepartingAndArriving component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will return the Traveler count to the calling method
	 */
	public int getCountryCount() throws Throwable {
		int total_count = 0;
		String countryIndex = "";
		try {
			for (int i = 1; i < 6; i++) {

				// List<WebElement> count =
				// Driver.findElements(By.xpath(".//div[@id='tileContent']//li["
				// + i + "]//h2"));
				countryIndex = Integer.toString(i);
				List<WebElement> count = Driver
						.findElements(createDynamicEle(TravelTrackerHomePage.countryCount, countryIndex));

				for (WebElement e : count) {
					total_count = total_count + Integer.parseInt(e.getText());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return total_count;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check the Travel & medical risks checkboxes and verifies if the
	 * application refreshes the counts on map accordingly
	 */
	public boolean updateRefineByRisksTravelAndMedical(String riskType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByRisksTravelAndMedical component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			boolean extremeTravel;
			boolean highTravel;
			boolean mediumTravel;
			boolean lowTravel;
			boolean insignificantTravel;
			boolean extremeMedical;
			boolean highMedical;
			boolean mediumMedical;
			boolean lowMedical;

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));

			switch (riskType) {
			case "travel_extreme":
				extremeTravel = isElementSelected(TravelTrackerHomePage.extremeTravelChkbox);
				if (extremeTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.extremeTravelChkbox,
							"Extreme Travel Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_high":
				highTravel = isElementSelected(TravelTrackerHomePage.highTravelChkbox);
				if (highTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.highTravelChkbox,
							"High Travel Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_medium":
				mediumTravel = isElementSelected(TravelTrackerHomePage.mediumTravelChkbox);
				if (mediumTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.mediumTravelChkbox,
							"Medium Travel Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "travel_low":
				lowTravel = isElementSelected(TravelTrackerHomePage.lowTravelChkbox);
				if (lowTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.lowTravelChkbox,
							"Low Travel Checkbox under Refine by Risk header inside Filters button"));
				break;
			case "travel_insignificant":
				insignificantTravel = isElementSelected(TravelTrackerHomePage.insignificantTravelChkbox);
				if (insignificantTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.insignificantTravelChkbox,
							"Insignificant Travel Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_extreme":
				extremeMedical = isElementSelected(TravelTrackerHomePage.extremeMedicalChkbox);
				if (extremeMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.extremeMedicalChkbox,
							"Extreme Medical Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_high":
				highMedical = isElementSelected(TravelTrackerHomePage.highMedicalChkbox);
				if (highMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.highMedicalChkbox,
							"High Medical Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_medium":
				mediumMedical = isElementSelected(TravelTrackerHomePage.mediumMedicalChkbox);
				if (mediumMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.mediumMedicalChkbox,
							"Medium Medical Checkbox under Refine by Risk header inside Filters button"));
				break;

			case "medical_low":
				lowMedical = isElementSelected(TravelTrackerHomePage.lowMedicalChkbox);
				if (lowMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.lowMedicalChkbox,
							"Low Medical Checkbox under Refine by Risk header inside Filters button"));
				break;
			}

			int travellersCount = getCountryCount();

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User is able to select " + riskType
					+ " Check box and the Application has refreshed counts on map accordingly.");
			LOG.info("updateRefineByRisksTravelAndMedical component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User is NOT able to select " + riskType
					+ " Check box and the Application has NOT refreshed counts on map.");
			componentActualresult.add("updateRefineByRisksTravelAndMedical verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByRisksTravelAndMedical component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check the checkboxes based on the locationType and verifies if
	 * the application refreshes the counts on map accordingly
	 */
	public boolean updateRefineByLocation(String locationType) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.dateRange, "Date Range under Filters button", 60));
			flags.add(click(TravelTrackerHomePage.dateRange, "Date Range under Filters button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));

			/*
			 * flags.add(assertElementPresent(TravelTrackerHomePage.
			 * fromCalendarIcon, "From Calendar Icon inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
			 * "From Calendar Icon inside the Date Range"));
			 * flags.add(isElementPresent(TravelTrackerHomePage.yearDateRange,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.yearDateRange, 9,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.monthDateRange, 4,
			 * "Month inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.dayDateRange,
			 * "Day inside the Date Range"));
			 * flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.
			 * loadingImage, "Loading Image in the Map Home Page"));
			 * flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
			 * "Filters Button on the left Pane of the Home Page", 60));
			 */
			flags.add(setFromDateInDateRangeTab(9, 4, "8"));

			if (locationType == "International") {
				flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
						"Extreme Travel Checkbox under Refine by Risk inside Filters button"));
				flags.add(JSClick(TravelTrackerHomePage.expatriateCheckbox,
						"Expatriate Checkbox under Refine by Risk inside Filters button"));
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in the Map Home Page"));
				flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
						"Filters Button on the left Pane of the Home Page", 60));
			} else if (locationType == "Domestic") {
				flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
						"Domestic Checkbox under Refine by Risk inside Filters button"));
				flags.add(JSClick(TravelTrackerHomePage.internationalCheckbox,
						"International Checkbox under Refine by Risk inside Filters button"));
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in the Map Home Page"));
				flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
						"Filters Button on the left Pane of the Home Page", 60));
			} else {
				flags.add(JSClick(TravelTrackerHomePage.expatriateCheckbox,
						"Expatriate Checkbox under Refine by Risk inside Filters button"));
				flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
						"Domestic Checkbox under Refine by Risk inside Filters button"));
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in the Map Home Page"));
				flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
						"Filters Button on the left Pane of the Home Page", 60));
			}
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The checkbox " + locationType
					+ "is selected successfully and the application is refreshed counts on map accordingly.");
			LOG.info("updateRefineByLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The checkbox " + locationType
					+ "is NOT selected and the application is has not refreshed counts on map.");
			componentActualresult.add("updateRefineByLocation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByLocation component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the given location and click on it
	 */
	public boolean viewTravellerByLocation(String location) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("viewTravellerByLocation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(type(TravelTrackerHomePage.searchBox, location, "Home Country under filters button"));
			flags.add(click(TravelTrackerHomePage.goButton, "Search box in Home page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(JSClick(TravelTrackerHomePage.countryName, "Country Name in the search box"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.locationsTravellerCount,
					"Traveller Count on the Locations Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("People panel is displayed with list of all travelers for the selected region successfully.");
			LOG.info("viewTravellerByLocation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("People panel is NOT displayed with list of all travelers for the selected region .");
			componentActualresult.add("viewTravellerByLocation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("viewTravellerByLocation component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the given location and validates the export and send
	 * message buttons
	 */
	public boolean performLocationSearch(String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performLocationSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.searchBox, "Search Box in the Map Home Page", 60));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName, "Search Box in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.filterCountry, countryName),
					"Select country from search results", 60));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.filterCountry, countryName),
					"Country Name in Home page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.locationsTravellerCount,
					"Travellers Count at Search Result", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.locationsTravellerCount,
					"Travellers Count at Search Result"));
			flags.add(assertElementPresent(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Location Search is performed successfully.");
			LOG.info("performLocationSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Location Search is failed.");
			componentActualresult.add("performLocationSearch verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("performLocationSearch component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the given travellerName and validates the export and
	 * send message buttons
	 */
	public boolean performTravellerSearch(String travellerName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performTravellerSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.searchBox, travellerName,
					"Search Dropdown for selecting Travellers Option in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "search Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.travellerFromSearch, travellerName),
					"Select Traveller from Traveller Search results"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("performTravellerSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("performTravellerSearch verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("performTravellerSearch component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the given flightName and validates the export and
	 * send message buttons
	 */
	public boolean performFlightSearch(String flightName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performFlightSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Flights Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 1,
					"Search Dropdown for selecting Flights Option in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));

			/*
			 * flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
			 * "From Calendar Icon inside the Date Range"));
			 * flags.add(isElementPresent(TravelTrackerHomePage.yearDateRange,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.yearDateRange, 9,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.monthDateRange, 4,
			 * "month inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.dayDateRange,
			 * "day inside the Date Range"));
			 * flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.
			 * loadingImage, "Loading Image in the Map Home Page"));
			 */
			flags.add(setFromDateInDateRangeTab(9, 4, "8"));

			flags.add(type(TravelTrackerHomePage.searchBox, flightName, "Search Box in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "Search Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.flightCount,
					"Number of travellers traveling in Flight"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.flightFromSearch, flightName),
					"Flight from Search Result"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Flight Search is performed successfully.");
			LOG.info("performFlightSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight Search is failed.");
			componentActualresult.add("performFlightSearch verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("performFlightSearch component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the given country and verifies if it`s displayed
	 */
	public boolean mapZoom(String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("mapZoom component execution Completed");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"Search Dropdown for selecting Locations Option in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName, "Search Box in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "Search Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.filterCountry, countryName), "Select country"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Map zoom is successful.");
			LOG.info("mapZoom component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Map zoom is failed.");
			componentActualresult.add("mapZoom verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("mapZoom component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify the Travelers list panel upon clicking the filtered
	 * results
	 */
	public boolean verifyTravellerCount(String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravellerCount component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.travellersList, "List of Travellers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellersList, "List of Travellers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.locationsTravellerCount, "Number of Travellers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("verifyTravellerCount component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("verifyTravellercount verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTravellerCount component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check the checkbox of the traveler and validates the contents
	 * in the downloaded file
	 */
	public boolean checkTravellerCheckboxTravellerPanel() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("CheckTravellerCheckboxTravellerPanel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(JSClick(TravelTrackerHomePage.travellerCheckbox,
					"Traveller Check Box in the List of Travellers Pane"));
			flags.add(click(TravelTrackerHomePage.exportPeoplePane, "Export Link in the Travelers List Pane"));

			Robot robot = new Robot();
			Shortwait();
			robot.mouseMove(510, 390);
			robot.delay(1500);
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press left click
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release left
																// click
			robot.delay(1500);
			robot.keyPress(KeyEvent.VK_ENTER);
			Longwait();

			List<String> results = new ArrayList<String>();
			File[] files = new File(ReporterConstants.downloaded_file_path).listFiles();
			for (File file : files) {
				if (file.isFile()) {
					results.add(file.getName());
				}
			}

			String filename = ReporterConstants.downloaded_file_path + "/" + results.get(0);
			List<String> columnNames = new ArrayList<String>();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				XSSFRow row = sheet.getRow(2);
				int noOfRows = sheet.getLastRowNum();
				int noOfTravellers = noOfRows - 3;

				for (int i = 0; i < row.getLastCellNum(); i++) {

					columnNames.add(i, row.getCell(i).getStringCellValue().trim());

				}
				for (int j = 0; j < columnNames.size(); j++)
					if (columnNames.get(j).equalsIgnoreCase("First Name"))
						;

			} catch (Exception e) {
				e.printStackTrace();
			}
			flags.add(click(TravelTrackerHomePage.closeTravellersList, "Close Travellers List"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("CheckTravellerCheckboxTravellerPanel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("CheckTravellerCheckboxTravellerPanel verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("CheckTravellerCheckboxTravellerPanel component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the Alerts tab, click on the sort by link and
	 * validates if country and traveler count options are present
	 */
	public boolean verifyAlertTile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertTile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.alertsTab,
					"Alerts Tab on the let Pane of the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.alertsTab, "Alerts Tab on the let Pane of the Map Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.sortByFilter,
					"Sort By Filter inside the Alerts Tab"));
			flags.add(click(TravelTrackerHomePage.sortByFilter, "Sort By Filter inside the Alerts Tab"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sortByCountry, "Country inside the Sort By Filter"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sortByTravellerCount,
					"Traveller Count Sort inside the Sort By Filter"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sortByTitle, "Title Sort inside the Sort By Filter"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sortByDate, "Date Sort inside the Sort By Filter"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("verifyAlertTile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("verifyAlertTile verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyAlertTile component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the country name that is displayed inside alerts tab,
	 * validate the export & Message buttons and then close the Travelers list
	 */
	public boolean selectAlertTile() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectAlertTile component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.alertTile, "Country Name inside Alerts Tab"));
			flags.add(click(TravelTrackerHomePage.alertTile, "Country Name inside Alerts Tab"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(assertElementPresent(TravelTrackerHomePage.exportPeoplePane,
					"Export Link in the Travelers List Pane"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"Message Button in the Travelers List Pane"));
			flags.add(click(TravelTrackerHomePage.closeTravellersList, "Close Travellers List Pane"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("selectAlertTile component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("selectAlertTile verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectAlertTile component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click the Read More link present below the country name inside
	 * alerts tab and validates the pop up
	 */
	public boolean verifyAlertDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAlertDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.readMoreLink,
					"Read More Link below the Country Name inside Alerts Tab", 60));
			flags.add(click(TravelTrackerHomePage.readMoreLink,
					"Read More Link below the Country Name inside Alerts Tab"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.alertPopupHeader,
					"Country Header Name inside the Country Pop up", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.alertPopupHeader,
					"Country Header Name inside the Country Pop up"));
			flags.add(assertElementPresent(TravelTrackerHomePage.alertPopupLocation,
					"Country Location Name inside the Country Pop up"));
			flags.add(assertElementPresent(TravelTrackerHomePage.alertPopupEventDate,
					"Country Location Name inside the Country Pop up"));
			flags.add(click(TravelTrackerHomePage.closeAlertPopup, "close Country Pop up"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("verifyAlertDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("verifyAlertDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyAlertDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the create new traveler button
	 */
	public boolean clickSearchTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSearchTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new ManualTripEntryPage().manualTripEntryPage();
			flags.add(click(ManualTripEntryPage.travellerSearchBtn,
					"Search Traveler Button inside Manual Trip Entry Page"));
			flags.add(assertElementPresent(ManualTripEntryPage.createNewTravellerBtn,
					"Create New Traveller Button inside Manual Trip Entry Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("clickSearchTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("clickSearchTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickSearchTraveller component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the existing traveler and click on it
	 */
	public boolean searchExistingTraveller(String travellername) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchExistingTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			new ManualTripEntryPage().manualTripEntryPage();
			flags.add(type(ManualTripEntryPage.profileLookup, travellername,
					"Traveller Name to be Searched inside the Manual Trip Entry Page"));
			List<WebElement> elements = Driver.findElements(ManualTripEntryPage.profileLookupDropDown);

			for (WebElement element : elements) {

			}
			flags.add(waitForInVisibilityOfElement(ManualTripEntryPage.autoComplete,
					"Auto Complete to select the Traveler Name"));
			Driver.findElement(ManualTripEntryPage.profileLookup).sendKeys(Keys.DOWN);
			Driver.findElement(ManualTripEntryPage.profileLookup).sendKeys(Keys.ENTER);
			flags.add(isElementPresent(ManualTripEntryPage.selectTravellerBtn,
					"Select Button inside Manual Trip Entry Page"));
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(ManualTripEntryPage.addProfileButton)).click().perform();
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the Manual Trip Entry Page"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			LOG.info("searchExistingTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
			componentActualresult.add("searchExistingTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("searchExistingTraveller component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the Create New Trip link inside Manual Trip Entry page
	 */
	public boolean createNewTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new ManualTripEntryPage().manualTripEntryPage();
			flags.add(isElementPresent(ManualTripEntryPage.createNewTripTab,
					"Create New Trip Tab Manual Trip Entry Page"));
			flags.add(click(ManualTripEntryPage.createNewTripTab, "Create New Trip Tab Manual Trip Entry Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create New Trip is successful.");
			LOG.info("createNewTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create New Trip is failed.");
			componentActualresult.add("createNewTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("createNewTrip component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will check the checkbox based on 'checkBx' parameter and gets the
	 * country count
	 */
	public boolean selectPeopleFilter(String checkBx, String location) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectPeopleFilter component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			switch (checkBx) {
			case "Last 31 days":
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				flags.add(click(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				Shortwait();
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ENTER);
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.last31Days,
						"Last 31 days Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.last31Days,
						"Last 31 days Checkbox under Filters Button on the Left Pane"));
				break;

			case "Next 24 hours":
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				Shortwait();
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ENTER);
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ESCAPE);
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in the Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.next24Hours,
						"Next 24 Hours Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.next24Hours,
						"Next 24 Hours Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.last31Days,
						"Last 31 Days Checkbox under Filters Button on the Left Pane"));
				break;

			case "Next 1 to 7 days":
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				Shortwait();
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ENTER);
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ESCAPE);
				flags.add(isElementPresent(TravelTrackerHomePage.next1to7Days,
						"Next 1 to 7 days Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
						"Next 1 to 7 days Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.next24Hours,
						"Next 24 hours Checkbox under Filters Button on the Left Pane"));
				break;

			case "Next 8 to 31 days":
				flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.location,
						"Drop Down for selecting In Location Option under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.location,
						"Drop Down for selecting Arriving Option under Filters Button on the Left Pane"));
				Shortwait();
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.UP);
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ENTER);
				Driver.findElement(TravelTrackerHomePage.location).sendKeys(Keys.ESCAPE);
				flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
						"Loading Image in Map Home Page"));
				flags.add(isElementPresent(TravelTrackerHomePage.next8to31Days,
						"Next 8 to 31 days Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.next8to31Days,
						"Next 8 to 31 days Checkbox under Filters Button on the Left Pane"));
				flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
						"Next 1 to 7 days Checkbox under Filters Button on the Left Pane"));
				break;

			default:
				break;
			}

			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page."));
			int travellersCount = getCountryCount();

			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"The application is refreshed successfully and the appropriate travellers count in Map-UI is displayed.");
			LOG.info("selectPeopleFilter component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"The application is NOT refreshed and the appropriate travellers count in Map-UI is NOT displayed");
			componentActualresult.add("selectPeopleFilter verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectPeopleFilter component execution Completed");
		}
		return flag;
	}

	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);

			} else {
				enterTextIntoAlert(chars);
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

		}
		return flag;
	}

	/**
	 * This will search for a train in the MapUI Home page
	 */

	@SuppressWarnings("unchecked")
	public boolean performTrainSearch(String trainName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performTrainSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown to select Trains in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 4,
					"Search Dropdown to select Trains in the Map Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchBox, "Search Box in the Map Home Page"));
			Shortwait();
			flags.add(type(TravelTrackerHomePage.searchBox, trainName, "Search Box in the Map Home Page"));
			/*
			 * flags.add(assertElementPresent(TravelTrackerHomePage.
			 * fromCalendarIcon, "From Calendar Icon inside the Date Range"));
			 * flags.add(JSClick(TravelTrackerHomePage.fromCalendarIcon,
			 * "fromCalendarIcon Button"));
			 * flags.add(isElementPresent(TravelTrackerHomePage.yearDateRange,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.yearDateRange, 9,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.monthDateRange, 4,
			 * "Month inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.dayDateRange, "day"));
			 * flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.
			 * loadingImage, "Loading Image in the Map Home Page"));
			 */
			flags.add(setFromDateInDateRangeTab(9, 4, "8"));

			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Train Search is performed successfully.");
			LOG.info("performTrainSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train Search is failed.");
			componentActualresult.add("performTrainSearch verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("performTrainSearch component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will select the train based on search
	 */
	public boolean selectTrain(String trainName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectTrain component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(
					click(createDynamicEle(TravelTrackerHomePage.trainName, trainName), "Train Name from the result"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.travellersList, "List of Trains in the Search Results"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Train name successfully.");
			LOG.info("selectTrain component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Train name is failed.");
			componentActualresult.add("selectTrain verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectTrain component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the Hotel in the MapUI Home page
	 */
	public boolean performHotelSearch(String hotelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("performHotelSearch component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown to select Hotels in the Map Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 5,
					"Search Dropdown to select Hotels in the Map Home Page"));
			Shortwait();
			flags.add(type(TravelTrackerHomePage.searchBox, hotelName, "Search Box in the Map Home Page"));

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.dateRange, "Date Range under Filters button"));
			flags.add(JSClick(TravelTrackerHomePage.dateRange, "Date Range under Filters button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));

			/*
			 * flags.add(assertElementPresent(TravelTrackerHomePage.
			 * fromCalendarIcon, "From Calendar Icon inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
			 * "From Calendar Icon inside the Date Range"));
			 * flags.add(isElementPresent(TravelTrackerHomePage.yearDateRange,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.yearDateRange, 9,
			 * "Year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.monthDateRange, 4,
			 * "Month inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.dayDateRange, "day"));
			 * flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.
			 * loadingImage, "Loading Image in the Map Home Page"));
			 */

			flags.add(setFromDateInDateRangeTab(9, 4, "8"));

			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Hotel Search is performed successfully.");
			LOG.info("performHotelSearch component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Hotel Search is failed.");
			componentActualresult.add("performHotelSearch verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("performHotelSearch component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will select the Hotel based on search
	 */
	public boolean selectHotel(String hotelName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectHotel component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.hotelName, hotelName), "Select Hotel"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.travellersList, "List of Hotels in the Search Results"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Train name successfully.");
			LOG.info("selectHotel component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Train name is failed.");
			componentActualresult.add("selectHotel verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectHotel component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify whether the trains are displayed using Find Filter
	 */
	public boolean verifyTrainResults(String result) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTrainResults component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(type(TravelTrackerHomePage.findTrains, result, "Train Name in the Search Box"));
			Driver.findElement(TravelTrackerHomePage.findTrains).sendKeys(Keys.ENTER);
			flags.add(assertElementPresent(TravelTrackerHomePage.findResults, "List of Trains in the Search Results"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Train name successfully.");
			LOG.info("verifyTrainResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Train name is failed.");
			componentActualresult.add("verifyTrainResults verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTrainResults component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify whether the Hotels are displayed using Find Filter
	 */
	public boolean verifyHotelResults(String result) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyHotelResults component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(type(TravelTrackerHomePage.findHotels, result, "Hotel Name in the Search Box"));
			Driver.findElement(TravelTrackerHomePage.findHotels).sendKeys(Keys.ENTER);
			flags.add(assertElementPresent(TravelTrackerHomePage.findResults, "List of Hotels in the Search Results"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Train name successfully.");
			LOG.info("verifyHotelResults component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Train name is failed.");
			componentActualresult.add("verifyHotelResults verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyHotelResults component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify if 'X' icon is displayed when we are doing a search in
	 * the MapUI home page
	 */
	public boolean verifyXIconDisplayed(String data) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyXIconDisplayed component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"Search Dropdown in the Map Home Page."));
			Shortwait();
			flags.add(type(TravelTrackerHomePage.searchBox, data, "Search Box in the Map Home Page"));
			Driver.findElement(TravelTrackerHomePage.searchBox).sendKeys(Keys.ENTER);
			flags.add(assertElementPresent(TravelTrackerHomePage.closeBtnSearch,
					"X icon in the Map Home Page after Search."));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Train name successfully.");
			LOG.info("verifyXIconDisplayed component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Train name is failed.");
			componentActualresult.add("verifyXIconDisplayed verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyXIconDisplayed component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the 'X' icon that is displayed after search in the
	 * MapUI Home page
	 */
	public boolean clickXIcon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickXIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.closeBtnSearch, "X icon in the Map Home Page after Search."));
			String text = getText(TravelTrackerHomePage.searchBox, "Search Box in the Map Home Page");

			if (text.isEmpty())
				componentActualresult
						.add("User is able to clear text in the search text box on clicking on 'x' icon successfully");
			else
				componentActualresult
						.add("User is NOT able to clear text in the search text box on clicking on 'x' icon");
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("clickXIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User is NOT able to clear text in the search text box on clicking on 'x' icon");
			componentActualresult.add("clickXIcon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickXIcon component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will create a New Building in the MapUI Home Page
	 */
	public boolean addNewBuilding(String buildingName, String buildingAddress, String buildingCity,
			String buildingCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("addNewBuilding component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.searchDropdown, "Search Button in the Map Home Page."));
			flags.add(click(TravelTrackerHomePage.buildingsTab, "Buildings Tab on the left Pane in the Map Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.addBuilding,
					"Add A New Building Button inside the Buildings Tab"));
			flags.add(click(TravelTrackerHomePage.addBuilding, "Add A New Building Button inside the Buildings Tab"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingName, "Building Name"));
			flags.add(type(TravelTrackerHomePage.buildingName, buildingName, "Building Name"));
			flags.add(type(TravelTrackerHomePage.buildingAddress, buildingAddress, "Building Address"));
			flags.add(type(TravelTrackerHomePage.buildingCity, buildingCity, "Building City"));
			flags.add(type(TravelTrackerHomePage.buildingCountry, buildingCountry, "Building Country"));
			flags.add(click(TravelTrackerHomePage.buildingSave, "Save Button in Add building page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.addBuilding,
					"Add A New Building Button inside the Buildings Tab"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New Building is created successfully.");
			LOG.info("addNewBuilding component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New Building creation is failed.");
			componentActualresult.add("addNewBuilding verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("addNewBuilding component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will search for the created building in the above method
	 */
	public boolean searchAndSelectBuilding(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("searchAndSelectBuilding component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.buildingsTab,
					"Buildings Tab on the left Pane in the Map Home Page", 60));
			flags.add(click(TravelTrackerHomePage.buildingsTab, "Buildings Tab on the left Pane in the Map Home Page"));
			flags.add(type(TravelTrackerHomePage.findBuilding, buildingName, "Building Name"));
			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result", 60));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.buildingFromResult, buildingName),
					"Building From Result"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selected the Created Building successfully.");
			LOG.info("searchAndSelectBuilding component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting the Created Building is Failed.");
			componentActualresult.add("searchAndSelectBuilding verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("searchAndSelectBuilding component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the 'Fly To' icon in the Building Details Page of
	 * MapUI home page
	 */
	public boolean clickFlyToIcon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickFlyToIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.flyBtn, "Fly To Icon in the Building Details Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the FlyTo Icon successfully.");
			LOG.info("clickFlyToIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the FlyTo Icon is Failed.");
			componentActualresult.add("clickFlyToIcon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickFlyToIcon component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the 'Aerial button' that is present in the map of
	 * MapUI home page
	 */
	public boolean clickAerialButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickAerialButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.aerialBtn, "Aerial Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the Aerial button successfully.");
			LOG.info("clickAerialButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the Aerial button is Failed.");
			componentActualresult.add("clickAerialButton verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickAerialButton component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will edit the building details in the Edit Building page of MapUI
	 * Home Page
	 */
	public boolean clickEditAndUpdateBuildingDetails(String buildingName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEditAndUpdateBuildingDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.editBuilding, "Edit Building Button in the Building Details Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingName, "Building Name"));
			flags.add(type(TravelTrackerHomePage.buildingName, buildingName, "Building Name"));
			flags.add(click(TravelTrackerHomePage.buildingSave, "Save button in Edit Building Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Building details are updated successfully.");
			LOG.info("clickEditAndUpdateBuildingDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Building details updation is Failed.");
			componentActualresult.add("clickEditAndUpdateBuildingDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickEditAndUpdateBuildingDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will select the User tab and select a customer in the Site Admin
	 * Page.
	 */
	public boolean selectUserTabAndSelectCust() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectUserTabandSelectCust component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(isElementPresent(SiteAdminPage.userTab,
					"User Tab inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.userTab, "User Tab inside General Tab of Site Admin Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(
					waitForVisibilityOfElement(SiteAdminPage.selectUserUserTab, "User from the Select User Drop down"));
			flags.add(selectByIndex(SiteAdminPage.selectUserUserTab, 3, "User from the Select User Drop down"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image inside the User Tab of General Tab in Site Admin Page"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selected the Customer successfully.");
			LOG.info("selectUserTabandSelectCust component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting the Customer is Failed.");
			componentActualresult.add("selectUserTabandSelectCust verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectUserTabandSelectCust component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the 'Emulate User' button Under User tab of General
	 * tab in the Site Admin Page
	 */
	public boolean clickEmulateUserButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickEmulateUserButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.emulateUserBtn,
					"Emulate User Button inside the User Tab of General Tab in Site Admin Page"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);

					flags.add(waitForVisibilityOfElement(SiteAdminPage.welcomeCust, "Welcome Customer"));
					flags.add(assertElementPresent(SiteAdminPage.welcomeCust, "Welcome Customer"));
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(SiteAdminPage.userTab, "User Tab inside General Tab of Site Admin Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the Emulator User button successfully.");
			LOG.info("clickEmulateUserButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the Emulator User button is Failed.");
			componentActualresult.add("clickEmulateUserButton verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickEmulateUserButton component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the 'Stop Emulation' button Under User tab of General
	 * tab in the Site Admin Page
	 */
	public boolean clickStopEmulationButton() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickStopEmulationButton component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.stopEmulationBtn,
					"Stop Emulation Button inside the User Tab of General Tab in Site Admin Page"));
			flags.add(click(SiteAdminPage.stopEmulationBtn,
					"Stop Emulation Button inside the User Tab of General Tab in Site Admin Page "));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image inside the User Tab of General Tab in Site Admin Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the Stop Emulation button successfully.");
			LOG.info("clickStopEmulationButton component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the Stop Emulation button is Failed.");
			componentActualresult.add("clickStopEmulationButton verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickStopEmulationButton component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will verify the Search Icon at Home Page
	 */
	public boolean verifySearchIcon() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifySearchIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.searchIcon, "Search Icon in Home page", 60));
			flags.add(isElementPresent(TravelTrackerHomePage.searchIcon, "Search Icon in Home page"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Search Icon verification is successful.");
			LOG.info("verifySearchIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search Icon verification is failed.");
			componentActualresult.add("verifySearchIcon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifySearchIcon component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will verify the Medical Risk & Secutiry Risk Icon at Locations
	 * search result
	 */
	public boolean verifyMedicalRiskAndSecurityRiskIcons(String CountryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyMedicalRiskAndSecurityRiskIcons component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.medicalRiskIcon, CountryName),
					"Medical Risk Icon in Location search result", 60));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.medicalRiskIcon, CountryName),
					"Medical Risk Icon in Location search result"));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.travelRiskIcon, CountryName),
					"Travel Risk Icon in Location search result"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Medical Risk & Security Risk Icon verification is successful.");
			LOG.info("verifyMedicalRiskAndSecurityRiskIcons component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Medical Risk & Security Risk Icon verification is failed.");
			componentActualresult.add("verifyMedicalRiskAndSecurityRiskIcons verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyMedicalRiskAndSecurityRiskIcons component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will verify the Country Guide Icon at Locations search result
	 */
	public boolean verifyCountryGuideIcon(String CountryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCountryGuideIcon component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuideIcon, CountryName),
					"Country Guide Icon in Location search result", 60));
			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuideIcon, CountryName),
					"Country Guide Icon in Location search result"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Country Guide Icon verification is successful.");
			LOG.info("verifyCountryGuideIcon component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Country Guide Icon verification is failed.");
			componentActualresult.add("verifyCountryGuideIcon verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCountryGuideIcon component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will click on Country Guide option and verifies the header of
	 * Country Guide which is opened in new window
	 */
	public boolean verifyCountryGuide(String CountryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCountryGuide component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(isElementPresent(createDynamicEle(TravelTrackerHomePage.countryGuide, CountryName),
					"Country Guide Icon in Location search result"));
			flags.add(click(createDynamicEle(TravelTrackerHomePage.countryGuide, CountryName),
					"Country Guide Icon in Location search result"));

			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();
			Shortwait();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					flags.add(waitForElementPresent(
							createDynamicEle(TravelTrackerHomePage.countryGuideHeader, CountryName),
							"Country Guide Header in Location search result", 300));
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Country Guide verification is successful.");
			LOG.info("verifyCountryGuide component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Country Guide verification is failed.");
			componentActualresult.add("verifyCountryGuide verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCountryGuide component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will click on 1st Traveler Name at People pane
	 */
	public boolean clickTravelerAtPeoplePane() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickTravelerAtPeoplePane component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.travelerName, "Traveler Name at People Pane", 60));
			flags.add(click(TravelTrackerHomePage.travelerName, "Traveler Name at People Pane"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Traveler at People pane is successful.");
			LOG.info("clickTravelerAtPeoplePane component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Traveler at People pane is failed.");
			componentActualresult.add("clickTravelerAtPeoplePane verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickTravelerAtPeoplePane component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will clicks on Send Message option at Traveller Details pane
	 */
	public boolean clickSendMessageAtTravellerDetailsPane() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickSendMessageAtTravellerDetailsPane component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.sendMessageAtTravellerDetails,
					"Send Message in Traveler Details pane", 60));
			flags.add(click(TravelTrackerHomePage.sendMessageAtTravellerDetails,
					"Send Message in Traveler Details pane"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("click on send message at traveller details pane is successful.");
			LOG.info("clickSendMessageAtTravellerDetailsPane component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("click on send message at traveller details pane is failed.");
			componentActualresult.add("clickSendMessageAtTravellerDetailsPane verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickSendMessageAtTravellerDetailsPane component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will search for the traveler with the option Itinerary and by
	 * entering the Itinerary number
	 */
	public boolean updateRefineByItinerary(String ItineraryNumber) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateRefineByItinerary component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));

			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown, "Search Drop down in Home Page"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 3, "Search Drop down in Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.dateRange, "Date Range under Filters button", 60));
			flags.add(click(TravelTrackerHomePage.dateRange, "Date Range under Filters button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));

			/*
			 * flags.add(assertElementPresent(TravelTrackerHomePage.
			 * fromCalendarIcon, "From Calendar Icon inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
			 * "From Calendar Icon Button"));
			 * flags.add(isElementPresent(TravelTrackerHomePage.yearDateRange,
			 * "year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.yearDateRange, 9,
			 * "year inside the Date Range"));
			 * flags.add(selectByIndex(TravelTrackerHomePage.monthDateRange, 4,
			 * "month inside the Date Range"));
			 * flags.add(click(TravelTrackerHomePage.dayDateRange,
			 * "day inside the Date Range"));
			 * flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.
			 * loadingImage, "Application loading Image"));
			 */
			flags.add(setFromDateInDateRangeTab(9, 4, "8"));

			flags.add(waitForElementPresent(TravelTrackerHomePage.searchBox, "Search Box in the Map Home Page", 60));
			flags.add(type(TravelTrackerHomePage.searchBox, ItineraryNumber, "Search Box in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.goButton, "Go Button in Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Search by Itinerary number is successful.");
			LOG.info("updateRefineByItinerary component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Search by Itinerary number is failed.");
			componentActualresult.add("updateRefineByItinerary verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateRefineByItinerary component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will verify whether the Itinerary search result is in expanded view
	 * or not
	 */
	public boolean verifyItinerarySearchResult() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyItinerarySearchResult component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.itineraryResultExpandIcon,
					"Expand Icon in Itinerary Search result", 100));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Itinerary search result is successful.");
			LOG.info("verifyItinerarySearchResult component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Itinerary search result is failed.");
			componentActualresult.add("verifyItinerarySearchResult verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyItinerarySearchResult component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will verify whether the Buildings option is displayed under Map
	 * Points Tab
	 */
	public boolean verifyBuildingsOptionsInHeader() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBuildingsOptionsInHeader component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapPoints, "Map Points drop down", 100));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingsCheckbox,
					"Buildings Check box under Map Points drop down in the Home page"));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Buildings option in Header is successful.");
			LOG.info("verifyBuildingsOptionsInHeader component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Buildings option in Header is failed.");
			componentActualresult.add("verifyBuildingsOptionsInHeader verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyBuildingsOptionsInHeader component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will click on Buildings option which is under Map Points Tab
	 */
	public boolean clickOnBuildingsCheckBoxInHeader() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickOnBuildingsCheckBoxInHeader component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapPoints, "Map Points drop down", 100));

			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingsCheckbox,
					"Buildings Check box under Map Points drop down in the Home page"));
			flags.add(JSClick(TravelTrackerHomePage.buildingsCheckbox,
					"Buildings Check box under Map Points drop down in the Home page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image"));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(waitForPresenceOfAllElements(TravelTrackerHomePage.buildingsIconOnMap, "Buildings Icons on Map"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Buildings CheckBox in Header is successful.");
			LOG.info("clickOnBuildingsCheckBoxInHeader component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Buildings CheckBox in Header is failed.");
			componentActualresult.add("clickOnBuildingsCheckBoxInHeader verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickOnBuildingsCheckBoxInHeader component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will click on Reset Map button
	 */
	public boolean clickResetMap() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickResetMap component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page", 100));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image"));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Reset Map is successful.");
			LOG.info("clickResetMap component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Reset Map is failed.");
			componentActualresult.add("clickResetMap verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickResetMap component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/*
	 * This will click on Buildings Tab which is at extreme left on the home
	 * page and verifies if buildings result pane displayed
	 */
	public boolean clickBuildingsTab() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickBuildingsTab component execution Completed");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.buildingsTab,
					"Buildings Tab on the Ledt Pane in the Home Page", 100));
			flags.add(click(TravelTrackerHomePage.buildingsTab, "Buildings Tab on the Ledt Pane in the Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Application loading Image"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.buildingsResultPane, "Buildings Result Pane", 100));
			flags.add(switchToDefaultFrame());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Buildings is successful.");
			LOG.info("clickBuildingsTab component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Buildings is failed.");
			componentActualresult.add("clickBuildingsTab verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickBuildingsTab component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify and select the Buildings check box present under the Map
	 * Tools link in the MapUI Home Page.
	 */
	public boolean verifyAndSelectBuildingsCheckBox() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyAndSelectBuildingsCheckBox component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page",
					60));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingsCheckbox,
					"Buildings Check box under Map Points drop down in the Home page"));
			flags.add(JSClick(TravelTrackerHomePage.buildingsCheckbox,
					"Buildings Check box under Map Points drop down in the Home page"));

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the Stop Emulation button successfully.");
			LOG.info("verifyAndSelectBuildingsCheckBox component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the Stop Emulation button is Failed.");
			componentActualresult.add("verifyAndSelectBuildingsCheckBox verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyAndSelectBuildingsCheckBox component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify the building details, click on 'Fly To' icon and then
	 * verify the Building Office Image in the MapUI Home page.
	 */
	public boolean verifyBuildingDetails() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyBuildingDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingDetailsForm,
					"Buildings details under Buildings tab"));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingDetailsForm,
					"Buildings details under Buildings tab"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.buildingOfficeImage,
					"Building Office Image in the MapUI Page"));
			flags.add(isElementPresent(TravelTrackerHomePage.buildingOfficeImage,
					"Building Office Image in the MapUI Page"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on the Stop Emulation button successfully.");
			LOG.info("verifyBuildingDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on the Stop Emulation button is Failed.");
			componentActualresult.add("verifyBuildingDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyBuildingDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will uncheck the Assistance App Check-Ins Enabled check box in the
	 * Customer tab of SiteAdmin Page. click on Update button and then verify
	 * the success message
	 */
	public boolean uncheckAssistanceAppCheckInsCheckbx() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("uncheckAssistanceAppCheckInsCheckbx component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(SiteAdminPage.assistanceAppCheckBox,
					"Assistance App Check-Ins checkbox in the Customer tab of SiteAdmin Page"));
			if (!isElementSelected(SiteAdminPage.assistanceAppCheckBox))
				flags.add(true);
			else
				flags.add(click(SiteAdminPage.assistanceAppCheckBox,
						"Assistance App Check-Ins checkbox in the Customer tab of SiteAdmin Page"));
			flags.add(click(SiteAdminPage.updateBtnCustTab, "Update button in the Customer tab of SiteAdmin Page"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage,
					"Progress Image in the User Tab inside the General Tab of Site Admin page"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileFieldSuccessMsg,
					"Updated International SOS Customer Message in the Customer tab of SiteAdmin Page"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldSuccessMsg,
					"Updated International SOS Customer Message in the Customer tab of SiteAdmin Page"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Unchecked the Assistance App Check-Ins Enabled check box in the Customer tab of SiteAdmin Page successfully.");
			LOG.info("uncheckAssistanceAppCheckInsCheckbx component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Unchecking the Assistance App Check-Ins Enabled check box in the Customer tab of SiteAdmin Page is Failed.");
			componentActualresult.add("uncheckAssistanceAppCheckInsCheckbx verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("uncheckAssistanceAppCheckInsCheckbx component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify the existence of Check-Ins tab on the left panel of the
	 * Map Home Page existence of Blue Man icon on the map existence of
	 * Check-Ins Checkbox under Map Points drop down in the Map Home Page
	 */
	public boolean verifyCheckInsTabMapHomePage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCheckInsTabMapHomePage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().siteAdminPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			if (isElementNotPresent(SiteAdminPage.blueManImagecheckInsTab, "Blue Man icon on the map"))
				componentActualresult
						.add("The Check-Ins tab on the left panel of the Map Home Page is not displayed successfully.");
			else
				componentActualresult.add("The Check-Ins tab on the left panel of the Map Home Page is displayed.");

			if (isElementNotPresent(SiteAdminPage.blueManImagecheckInsTab, "Blue Man icon on the map"))
				componentActualresult.add("Blue Man icon on the map is not displayed successfully.");
			else
				componentActualresult.add("Blue Man icon on the map is displayed.");
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapPoints, "Map Points drop down", 100));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Map Home Page"));
			if (isElementNotPresent(TravelTrackerHomePage.checkInsCheckbox,
					"Check-Ins Checkbox under Map Points drop down in the Map Home Page"))
				componentActualresult.add(
						"Check-Ins Checkbox under Map Points drop down in the Map Home Page is not displayed successfully.");
			else
				componentActualresult
						.add("Check-Ins Checkbox under Map Points drop down in the Map Home Page is displayed.");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyCheckInsTabMapHomePage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of the Check-Ins tab on the lest side of the Map Home Page is Failed.");
			componentActualresult.add("verifyCheckInsTabMapHomePage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCheckInsTabMapHomePage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will select the VIP Traveler option inside the Additional Filters
	 * section of the Home Page and verifies if only the travelers who have the
	 * VIP indicator selected in their profiles are displayed.
	 */
	public boolean selectVIPTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectVIPTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));
			flags.add(JSClick(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerCount,
					"Traveller Count in the Locations Panel"));
			flags.add(click(TravelTrackerHomePage.travellerCount, "Traveller Count in the Locations Panel"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel", 60));
			flags.add(click(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.vipStatusIndicator,
					"Vip Status Indicator in the Traveller List Panel"));
			flags.add(assertElementPresent(TravelTrackerHomePage.vipStatusIndicator,
					"Vip Status Indicator in the Traveller List Panel"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selected VIP Indicator Successful.");
			LOG.info("selectVIPTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting the VIP Indicator is Failed.");
			componentActualresult.add("selectVIPTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectVIPTraveller component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will select the Non VIP Traveler option inside the Additional
	 * Filters section of the Home Page and verifies if only the travelers who
	 * does not have the VIP indicator selected in their profiles are displayed.
	 */
	public boolean selectNonVIPTraveller() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("selectNonVIPTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));
			flags.add(JSClick(TravelTrackerHomePage.vipTraveller,
					"VIP Traveller Option in the Additional Filters section"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerCount,
					"Traveller Count in the Locations Panel"));
			flags.add(click(TravelTrackerHomePage.travellerCount, "Traveller Count in the Locations Panel"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel", 60));
			flags.add(click(TravelTrackerHomePage.travellerNameInTravellerList,
					"Traveller Name in the Traveller List Panel"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.nonVipStatusIndicator,
					"Non Vip Status Indicator in the Traveller List Panel"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipStatusIndicator,
					"Non Vip Status Indicator in the Traveller List Panel"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Selected Non VIP Traveller Successful.");
			LOG.info("selectNonVIPTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Selecting the Non VIP Indicator is Failed.");
			componentActualresult.add("selectNonVIPTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("selectNonVIPTraveller component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify the Date Range and Date Presets inside the Filters
	 * section of the Home Page
	 */
	public boolean verifyPresetsAndRangeOptions() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyPresetsAndRangeOptions component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.dateRange,
					"Date Range inside the Filters section of the Home Page."));
			flags.add(assertElementPresent(TravelTrackerHomePage.datePresetList,
					"Date Preset inside the Filters section of the Home Page."));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Date Range and Date Presets successfully.");
			LOG.info("verifyPresetsAndRangeOptions component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Date Range and Date Presets is Failed.");
			componentActualresult.add("verifyPresetsAndRangeOptions verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyPresetsAndRangeOptions component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify if the Additional Filters are expanded by default inside
	 * the Filters section of the Home Page.
	 */
	public boolean verifyAdditionalFiltersExpanded() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn, "filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			flags.add(assertElementPresent(TravelTrackerHomePage.arrowDownFilter,
					"Arrow Down Filter in Additional Filters"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("The Additional Filters are expanded by default successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The Additional Filters are NOT expanded by default");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify if the VIP And Ticketing Status Options are displayed.
	 */
	public boolean verifyVIPAndTicketingStatusOptions() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.vipTraveller,
					"VIP Traveller Option in the Additional Filters section"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));
			flags.add(assertElementPresent(TravelTrackerHomePage.ticketed,
					"Ticketed Option in the Additional Filters section"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonTicketed,
					"Non Ticketed Option in the Additional Filters section"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of the VIP and Ticketing Status Options are Successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of the VIP and Ticketing Status Options is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify if the VIP Options are displayed.
	 */
	public boolean verifyVIPStatusOptions() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.vipTraveller,
					"VIP Traveller Option in the Additional Filters section"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonVipTraveller,
					"Non VIP Traveller Option in the Additional Filters section"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of the VIP Options are Successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of the VIP Options is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify if the Ticketing Status Options are displayed.
	 */
	public boolean verifyTicketingStatusOptions() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.ticketed,
					"Ticketed Option in the Additional Filters section"));
			flags.add(assertElementPresent(TravelTrackerHomePage.nonTicketed,
					"Non Ticketed Option in the Additional Filters section"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of the Ticketing Status Options are Successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of the Ticketing Status Options is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click the Alerts tab present on the left Pane in the Home Page
	 * and verifies the Traveler details.
	 */
	public boolean clickAlertsTabAndVerifyTravDetails() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.alertsTab, "Alerts Tab on the let Pane of the Map Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.sortByFilter,
					"Sort By Filter inside the Alerts Tab"));
			flags.add(click(TravelTrackerHomePage.alertTile, "Country Name inside Alerts Tab"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.tileContent, "List of Travelers", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerNameDetails,
					"Traveller Name inside the Travellers List"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerEmailDetails,
					"Traveller Email inside the Travellers List"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerCountryDetails,
					"Traveller Home Country inside the Travellers List"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Alerts tab click and the Travellers Details verification is Successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Alerts tab click and the Travellers Details verification is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will Select Dropdown in Date Presets tab present on the left Pane in
	 * the Home Page and verifies the Traveler details based on the Checkbox
	 * option(Last 31 days) selected below the Dropdown.
	 */
	public boolean verifyArrivingOptionSelected() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyArrivingOptionselected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(click(TravelTrackerHomePage.location, "Selecting the Dropdown"));
			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.UP);
			Driver.findElement(TravelTrackerHomePage.presetSelect).sendKeys(Keys.ENTER);
			Longwait();
			flags.add(isElementPresent(TravelTrackerHomePage.last31Days, "Last 31 Days Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.last31Days, "Last 31 Days Checkbox under Filters Button"));
			Longwait();
			int countryCount_AfterApply = getCountryCount();
			if ((countryCount_AfterApply != 0))
				componentActualresult.add("The traveller count under the left location pane is not Zero");
			else
				componentActualresult.add("The traveller count under the left location pane is Zero ");

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyArrivingOptionselected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The Additional Filters are NOT expanded by default");
			LOG.error("verifyArrivingOptionselected component execution Completed");
		}
		return flag;
	}

	/*
	 * This will Select Dropdown in Date-Presets tab with InLocation option,
	 * present on the left Pane in the Home Page and verifies the Traveler
	 * details based on the Checkbox option Next 8-31 days selected below the
	 * Dropdown.
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyNext8To31daysSelected() throws Throwable {
		boolean flag = true;
		int countryCount_BeforeApply = 0;
		int countryCount_AfterApply = 0;
		try {
			LOG.info("verifyNext8To31daysSelected component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();

			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply : " + countryCount_BeforeApply);
			flags.add(isElementPresent(TravelTrackerHomePage.next8to31Days,
					"Next 8-31 days Checkbox under Filters Button"));
			flags.add(JSClick(TravelTrackerHomePage.next8to31Days, "Next 8-31 days Checkbox under Filters Button"));

			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn, "filtersBtn", 60));

			countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply : " + countryCount_AfterApply);

			if (!(countryCount_BeforeApply == countryCount_AfterApply)) {

				componentActualresult.add("The traveller count under the left location pane is updated successfully");
			} else
				componentActualresult.add("The traveller count under the left location pane is NOT updated ");

			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("verifyNext8To31daysSelected component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("The Additional Filters are NOT expanded by default");
			componentActualresult.add("verifyNext8To31DaysSelected verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyNext8To31daysSelected component execution Ended with Fail");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will Navigate to Filters tab, select Date Range and validates the
	 * Traveler count
	 */
	public boolean selectDateRangeAndValidateNoOfTrav() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn,
					"Filters Button on the left Pane of the Home Page", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.dateRange, "Date Range insdied the Filters button",
					60));
			flags.add(click(TravelTrackerHomePage.dateRange, "Date Range inside the Filters button"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			setFromDateInDateRangeTab(9, 4, "8");
			int noOfTravellers = getCountryCount();
			System.out.println("noOfTravellers : " + noOfTravellers);
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add(
					"Verification of the count of Travelers on the location pin should not be truncated is Successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add(
					"Verification of the count of Travelers on the location pin should not be truncated is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will Navigate to Vismo Tab, click on the Reset Map button and verify
	 * if the state of the Vismo Tab is set to default
	 */
	public boolean navigateVismoTabAndVerifyDefaultStatus() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(assertElementPresent(TravelTrackerHomePage.vismoTab,
					"Vismo Tab on the left Panel in the Map Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.vismoTab, "Vismo Tab on the left Panel in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.vismoNormalLabel, "Normal Label inside the Vismo Tab",
					5));
			flags.add(
					assertElementPresent(TravelTrackerHomePage.vismoNormalLabel, "Normal Label inside the Vismo Tab"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button in the Map Home Page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage,
					"Loading Image in the Map Home Page"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.filtersBtn, "Filters button in the Home Page", 10));
			flags.add(isElementNotPresent(TravelTrackerHomePage.vismoNormalLabel, "Normal Label inside the Vismo Tab"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verification of the State of the left panel should be set to default is Successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of the State of the left panel should be set to default is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify and select the Intl. SOS Resources check box present
	 * under the Map Tools link in the MapUI Home Page.
	 */
	public boolean verifyAndSelectIntlSOSResourcesCheckBox() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(switchToDefaultFrame());
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForElementPresent(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page",
					60));
			flags.add(JSClick(TravelTrackerHomePage.mapPoints, "Map Points drop down in the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSCheckBox,
					"Intl. SOS Resources Check box under Map Points drop down in the Home page"));
			flags.add(JSClick(TravelTrackerHomePage.intlSOSCheckBox,
					"Intl. SOS Resources Check box under Map Points drop down in the Home page"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified and checked Intl. SOS Resources checkbox successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification and checking of Intl. SOS Resources checkbox is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will click on the Intl. SOS Resources tab present on the left panel
	 * of the MapUI Home Page Search for the existing Intl. SOS Resources
	 * building, click on it Validate the Address and Phone number
	 */
	public boolean selectIntlSOSResourcesTabAndClickOnBuilding(String intlResourceName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(JSClick(TravelTrackerHomePage.intlSOSResourcesTab,
					"Intl.SOS Resources Tab on the left panel of the Home page"));
			flags.add(isElementPresent(TravelTrackerHomePage.findIntlSOSResources,
					"Intl. SOS Resources Search box inside the Intl.SOS Resources Tab"));
			flags.add(type(TravelTrackerHomePage.findIntlSOSResources, intlResourceName, "Message Subject"));
			//
			flags.add(isElementPresent(By.xpath("//h1[text()='" + intlResourceName + "']"),
					"Intl. SOS Resources Search box inside the Intl.SOS Resources Tab"));
			Driver.findElement(By.xpath("//h1[text()='" + intlResourceName + "']")).click();
			//
			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.intlSOSResourceAddress, "Intl Resources Address"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSResourceAddress, "Intl Resources Address"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSResourcePhone, "Intl Resources Phone"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified and checked Intl. SOS Resources checkbox successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification and checking of Intl. SOS Resources checkbox is Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This will verify the Intl. SOS Resources Image on the map
	 */
	public boolean verifyintlSOSResourcesImage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame"));
			flags.add(isElementPresent(TravelTrackerHomePage.flyBtn, "Fly To Icon"));
			flags.add(click(TravelTrackerHomePage.flyBtn, "Fly To Icon"));
			flags.add(
					waitForVisibilityOfElement(TravelTrackerHomePage.intlSOSResourceImage, "Intl.SOS Resource Image"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSResourceImage, "Intl.SOS Resource Image"));
			flags.add(switchToDefaultFrame());
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified the Intl.SOS Resource Image successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification Intl. SOS Resources Image is Failed.");
		}
		return flag;
	}

}
