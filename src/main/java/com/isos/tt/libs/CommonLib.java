package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestScriptDriver;
import com.isos.ecms.libs.CommonECMSLib;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class CommonLib extends ActionEngine{

	//private final Logger LOG = Logger.getLogger(CommonLib.class);
	static String methodName;

	public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();
	public static List componentActualresult = new ArrayList();	

	public static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return sdf.format(date);
	}
	
	@SuppressWarnings("unused")
	public String getListOfScreenShots(String screenShotDirectory_testCasePath, final String methodname) {
		String finalString = "";
		try {
			File f = new File(screenShotDirectory_testCasePath);
			List<String> list = Arrays.asList(f.list(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.startsWith(methodname);

				}
			}

			));
			String ScreenShotPath = screenShotDirectory_testCasePath;
			ScreenShotPath = ScreenShotPath.replace(".\\", "\\");

			if (!list.isEmpty()) {
				String[] files = (String[]) list.toArray();
				for (int i = 0; i < files.length; i++) {
					String filess = files[i];
					filess = filess.replaceAll(" ", "%20");

					finalString = finalString + "[ Failed screeenshot: " + methodname + "] [i]" + "\n" + "[i]: "
							+ ReporterConstants.Jenkins_path + ScreenShotPath + "\\" + filess + "\n";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return finalString;
	}
	
	@SuppressWarnings("unchecked")
	public boolean doPandemicLogout() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("doPandemicLogout component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.PandemicMember, "Member icon"));
			flags.add(click(ContentEditorPage.PandemicLogout, "Logout  icon"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			LOG.info("doPandemicLogout component execution Completed");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("doPandemicLogout verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("doPandemicLogout component execution Completed");			
		}
		return flag;
	}


	public void checkPageIsReadyUsingJavaScript() {
		LOG.info("checkPageIsReadyUsingJavaScript component execution Started");
		JavascriptExecutor js = (JavascriptExecutor) Driver;
		// Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString()
				.equals("complete")) {
			
			return;
		}
		
		// This loop will rotate for 25 times to check If page Is ready after
		// every 1 second.
		// You can replace your value with 25 If you wants to Increase or
		// decrease wait time.
		for (int i = 0; i < 25; i++) {
			try {
				Thread.sleep(1000);				
			} catch (InterruptedException e) {
				LOG.error("checkPageIsReadyUsingJavaScript component execution failed");
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString()
					.equals("complete")) {
				break;
			}
		}
		LOG.info("checkPageIsReadyUsingJavaScript component execution Completed");
	}

	@SuppressWarnings("unchecked")
	public static void setMethodName(String MethodName) throws IOException {
		methodName = MethodName;
		//	componentNamesList.add(MethodName);
	}

	public static String getMethodName() throws IOException {
		return methodName;

	}

	@SuppressWarnings("unchecked")
	public boolean openBrowser(String url) throws IOException,
	InterruptedException {
		boolean flag = true;
		try {
			LOG.info("openBrowser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			driverInitiation(url);
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser opened successfully.");
			LOG.info("openBrowser component execution Completed");
		} catch (Exception e) {
			flag = false;
			// TODO Auto-generated catch block
			e.printStackTrace();
			componentActualresult.add("Failed to open the browser.");
			componentActualresult.add("openBrowser verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			componentEndTimer.add(getCurrentTime());
			LOG.error("openBrowser component execution Completed");			
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean quitBrowser() throws Exception {
		boolean flag = true;
		try {
			LOG.info("quitBrowser component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Driver.close();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser Closed successfully.");
			LOG.info("quitBrowser component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser failed to Close.");
			componentActualresult.add("quitBrowser verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("quitBrowser component execution Completed");
		}		
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean login(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("login component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			flags.add(type(TravelTrackerHomePage.userName, userName, "User Name"));
			flags.add(typeSecure(TravelTrackerHomePage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(TravelTrackerHomePage.loginButton, "Login Button"));
			
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
			LOG.info("login component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User login failed.");
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User login verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("login component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean logOut() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("logOut component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			
			flags.add(click(TravelTrackerHomePage.logOff, "Logoff"));

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
			LOG.info("logOut component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User logout failed.");
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User login verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodName()));
			LOG.error("logOut component execution Completed");
		}
		return flag;
	}

	
	public String getBrowser() {
		return this.browser;
	}
	
	public boolean switchToFrame(By by, String locator) throws IOException{
		boolean status=false;
		try{
			WebElement Frame = Driver.findElement(by);
			Driver.switchTo().frame(Frame);
			status = true;
			
			LOG.info("Switched to : " + locator);
		}
		catch(Exception e){
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locator);
		}
		return status;
	}
	
	public boolean switchToDefaultFrame() throws IOException{
		boolean status=false;
		try{			
			Driver.switchTo().defaultContent();
			status = true;
			System.out.println("Switched to Default Frame");
			LOG.info("Switched to Default Frame");
		}
		catch(Exception e){
			LOG.error(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Switch to Default Frame");
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public boolean setFromDateInDateRangeTab(int indexOfYear, int indexOfMonth, String DayOfFromDate) throws Throwable {
		boolean flag = true;
		try {
			new TravelTrackerHomePage().travelTrackerHomePage();

			assertElementPresent(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			click(TravelTrackerHomePage.fromCalendarIcon, "From Calendar Icon inside the Date Range");
			switchToDefaultFrame();
			switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to mapIFrame");
			selectByIndex(TravelTrackerHomePage.yearDateRange, indexOfYear, "Year inside the Date Range");
			selectByIndex(TravelTrackerHomePage.monthDateRange, indexOfMonth, "Month inside the Date Range");
			click(createDynamicEle(TravelTrackerHomePage.dayDateRange, DayOfFromDate), "Day inside the Date Range");
			waitForInVisibilityOfElement(TravelTrackerHomePage.loadingImage, "Loading Image in the Map Home Page");
			waitForElementPresent(TravelTrackerHomePage.filtersBtn, "Filters Button on the left Pane of the Home Page",
					60);

			LOG.info("From Date in Date Range Tab is selected");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Selection of From Date in Date Range Tab is Failed");
		}
		return flag;
	}

}
