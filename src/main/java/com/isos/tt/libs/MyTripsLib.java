package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class MyTripsLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();

	int randomNumber = generateRandomNumber();
	String tripName = "InternationalSOS" + randomNumber;

	@SuppressWarnings("unchecked")
	public boolean myTripsLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("myTripsLogin component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			new TravelTrackerHomePage().travelTrackerHomePage();

			flags.add(type(MyTripsPage.userName, userName, "User Name"));
			flags.add(type(MyTripsPage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(MyTripsPage.loginButton, "Login Button"));
			componentActualresult.add("My Trips Login is successful.");
			componentEndTimer.add(getCurrentTime());
			LOG.info("myTripsLogin component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("My Trips Login is failed.");
			componentActualresult.add("myTripsLogin verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("myTripsLogin component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickCreateNewTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.createNewTripTab, "Create New Trip Tab"));
			flags.add(click(MyTripsPage.createNewTripTab, "Create New Trip Tab"));
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create New Trip is successful.");
			LOG.info("clickCreateNewTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create New Trip is failed.");
			componentActualresult.add("clickCreateNewTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickCreateNewTrip component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMyTrips(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripName_myTrips component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));

			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "ticketCountry"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Trip name is entered successfully.");
			LOG.info("enterTripName_myTrips component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name entry is failed.");
			componentActualresult.add("enterTripName_myTrips verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterTripName_myTrips component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterFlightDetails(String airline, String departureCity, String arrivalCity, String flightNumber)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterFlightDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(isElementPresent(MyTripsPage.airline, "Airline"));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.airlineOption, "Airline Option"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]"))
					.sendKeys(Keys.ARROW_DOWN);
			;
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]"))
					.sendKeys(Keys.ENTER);

			flags.add(type(MyTripsPage.flightDepartureCity, departureCity, "Departure City"));
			flags.add(
					waitForVisibilityOfElement(MyTripsPage.flightDepartureCityOption, "Flight Departure City Option"));

			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ARROW_DOWN);
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.flightArrivalCityOption, "Flight Arrival City Option"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ARROW_DOWN);
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.flightNumber, flightNumber, "Flight Number"));
			flags.add(type(MyTripsPage.departureDate, getCurrentDate(), "Departure Date"));
			flags.add(type(MyTripsPage.arrivalDate, getFutureDate(), "Arrival Date"));
			flags.add(selectByValue(MyTripsPage.departureHr, "21", "Departure Hour"));
			flags.add(selectByValue(MyTripsPage.departureMin, "10", "Departure Min"));
			flags.add(selectByValue(MyTripsPage.arrivalHr, "09", "Arrival Hour"));
			flags.add(selectByValue(MyTripsPage.arrivalMin, "15", "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.flightSuccessMessage, "Flight Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.flightSuccessMessage, "Flight Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage, "Flight Details successfully saved"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Flight details are entered successfully.");
			componentActualresult.add("enterFlightDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.info("enterFlightDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details entry is failed.");
			LOG.error("enterFlightDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterAccommodationDetails(String hotelName, String city, String country, String accommodationType)
			throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterAccommodationDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(click(MyTripsPage.addAccommodationTab, "Add Accommodation Tab"));
			flags.add(isElementPresent(MyTripsPage.hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getCurrentDate(), "Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getFutureDate(), "Check-Out Date"));
			flags.add(click(MyTripsPage.accommodationAddress, "Accommodation Address"));
			flags.add(isElementPresent(MyTripsPage.accommodationCity, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCity, city, "Accommodation City"));
			flags.add(type(MyTripsPage.accommodationCountry, country, "Accommodation Country"));
			flags.add(click(MyTripsPage.findButton, "Find Button"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image"));
			flags.add(click(MyTripsPage.suggestedAddress, "Suggested Address"));
			flags.add(click(MyTripsPage.selectAddress, "Select Address"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.spinnerImage, "Spinner Image"));
			flags.add(selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
			flags.add(isElementPresent(MyTripsPage.saveAccommodation, "Save Accommodation Button"));
			flags.add(click(MyTripsPage.saveAccommodation, "Save Accommodation Details"));

			flags.add(waitForVisibilityOfElement(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Success Message"));
			flags.add(assertElementPresent(MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Accommodation details are entered successfully.");
			LOG.info("enterAccommodationDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation details entry is failed.");
			componentActualresult.add("enterAccommodationDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterAccommodationDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTrainDetails(String departureCity, String arrivalCity, String trainNo) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTrainDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(click(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(isElementPresent(MyTripsPage.trainCarrier, "Train Carrier"));
			flags.add(selectByIndex(MyTripsPage.trainCarrier, 4, "Train Carrier"));
			flags.add(click(MyTripsPage.trainCarrier, "Train Carrier"));
			flags.add(click(MyTripsPage.trainCarrierValue, "Train Carrier Value"));
			flags.add(type(MyTripsPage.trainDepartureCity, departureCity, "Departure City"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainDepartureCityOption, "trainDepartureCityLoading"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.trainArrivalCity, arrivalCity, "Arrival City"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainArrivalCityOption, "Train Arrival City Option"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));
			flags.add(type(MyTripsPage.trainDepartureDate, getCurrentDate(), "Departure Date"));
			flags.add(type(MyTripsPage.trainArrivalDate, getFutureDate(), "Arrival Date"));

			flags.add(selectByValue(MyTripsPage.trainDepartureHr, "23", "Departure Hour"));
			flags.add(selectByValue(MyTripsPage.trainDepartureMin, "10", "Departure Min"));
			flags.add(selectByValue(MyTripsPage.trainArrivalHr, "14", "Arrival Hour"));
			flags.add(selectByValue(MyTripsPage.trainArrivalMin, "10", "Arrival Min"));

			flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainSuccessMessage, "Train Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.trainSuccessMessage, "Train Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage, "Train Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Train details are entered successfully.");
			LOG.info("enterTrainDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details entry is failed.");
			componentActualresult.add("enterTrainDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterTrainDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterGroundTransportationDetails(String transportName, String pickUpCityCountry,
			String dropOffCityCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("entergroundTransportationDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(click(MyTripsPage.addGroundTransportTab, "Add Ground Transportation Tab"));
			flags.add(isElementPresent(MyTripsPage.transportName, "Transport Name"));
			flags.add(type(MyTripsPage.transportName, transportName, "Transport Name"));
			flags.add(type(MyTripsPage.pickUpCityCountry, pickUpCityCountry, "Pickup City Country"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.pickUpCityCountryOption, "PickUp City Country Option"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.dropOffCityCountry, dropOffCityCountry, "DropOff City Country"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.dropOffCityCountryOption, "DropOff City Country Option"));
			Driver.findElement(By
					.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.pickUpDate, getCurrentDate(), "Pickup Date"));
			flags.add(type(MyTripsPage.dropOffDate, getFutureDate(), "DropOff Date"));
			flags.add(isElementPresent(MyTripsPage.pickUpHr, "Pickup Hour"));
			flags.add(selectByValue(MyTripsPage.pickUpHr, "22", "Pickup Hour"));
			flags.add(selectByValue(MyTripsPage.pickUpMin, "10", "Pickup Min"));
			flags.add(selectByValue(MyTripsPage.dropOffHr, "12", "Dropoff Hour"));
			flags.add(selectByValue(MyTripsPage.dropOffMin, "10", "Dropoff Min"));
			flags.add(JSClick(MyTripsPage.saveTransport, "Save Ground Transportation Button"));

			flags.add(waitForVisibilityOfElement(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message"));
			flags.add(waitForElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Ground Transportation details are entered successfully.");
			LOG.info("entergroundTransportationDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Ground Transportation details entry is failed.");
			componentActualresult.add("entergroundTransportationDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("entergroundTransportationDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTravelInfoDetails(String abroadPhoneNumber, String contactName, String contactRelationship,
			String emergencyPhoneNo, String overseasProgram, String deanName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTravelInfoDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(type(MyTripsPage.contactName, contactName, "Contact Name"));
			flags.add(type(MyTripsPage.emergencyPhoneNo, emergencyPhoneNo, "Emergency Phone Number"));
			flags.add(selectByIndex(MyTripsPage.visitType, 2, "Visit Type"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.saveTripInfo, "Save Trip Information Button"));
			flags.add(click(MyTripsPage.saveTripInfo, "Save Trip Information Button"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress"));
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 60));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Travel Information details are entered successfully.");
			LOG.info("enterTravelInfoDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Travel Information details entry.");
			componentActualresult.add("enterTravelInfoDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterTravelInfoDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean saveTripInformation() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("saveTripInformation component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress, "Loading Image Progress"));
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 60));
			flags.add(
					assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information is successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Saving Trip Information is successful.");
			LOG.info("saveTripInformation component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Saving Trip Information is failed.");
			componentActualresult.add("saveTripInformation verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("saveTripInformation component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyCreatedTrip component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(click(MyTripsPage.myProfileTab, "My Profile Trips Tab"));
			Driver.switchTo().alert().accept();
			flags.add(isElementPresent(MyTripsPage.verifyTripNameMyTrips, "Trip Name"));
			String tripNameText = getText(MyTripsPage.verifyTripNameMyTrips, "Trip Name");

			flags.add(assertTextMatching(MyTripsPage.verifyTripNameMyTrips, tripName, "Trip Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create Trip verification is successful.");
			LOG.info("verifyCreatedTrip component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("Create Trip verification is failed.");
			componentActualresult.add("verifyCreatedTrip verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyCreatedTrip component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNewUserLink component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.newUserRegistration, "New User Registration Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Link verification is successful.");
			LOG.info("verifyNewUserLink component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link verification is failed.");
			componentActualresult.add("verifyNewUserLink verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyNewUserLink component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("clickNewUserLink component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(click(MyTripsPage.newUserRegistration, "New User Registration Link"));
			Shortwait();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Link is clicked successfully.");
			LOG.info("clickNewUserLink component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link is NOT clicked.");
			componentActualresult.add("clickNewUserLink verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("clickNewUserLink component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean fillNewUserDetailsAndSave(String firstName, String lastName, String email, String pwd,
			String reenterPwd, String answer1, String answer2) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("fillNewUserDetailsAndSave component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();

			if (Driver.getCurrentUrl().contains("Registration")) {
				flags.add(waitForVisibilityOfElement(MyTripsPage.titleDropDown, "Title Drop Down"));
				flags.add(selectByValue(MyTripsPage.titleDropDown, "Mr.", "Title Drop Down"));
				flags.add(type(MyTripsPage.firstNameMyTripsReg, firstName, "First Name My Trips Registration"));
				flags.add(type(MyTripsPage.lastNameMyTripsReg, lastName, "Last Name My Trips Registration"));
				flags.add(type(MyTripsPage.emailMyTripsReg, email, "Email My Trips Registration"));
				flags.add(type(MyTripsPage.pwdMyTripsReg, pwd, "Password My Trips Registration"));
				flags.add(type(MyTripsPage.reenterPwdMyTripsReg, reenterPwd, "Reenter Password My Trips Registration"));
				flags.add(click(MyTripsPage.securityQstn1, "Security Question 1"));
				flags.add(click(MyTripsPage.securityQstn1Option, "Security Question1 option"));
				flags.add(type(MyTripsPage.securityAnswer1, answer1, "Security Answer 1"));
				flags.add(selectByIndex(MyTripsPage.securityQstn2, 9, "Security Question 2"));
				flags.add(type(MyTripsPage.securityAnswer2, answer2, "Security Answer 2"));
				flags.add(click(MyTripsPage.submitButton, "Submit Button"));
				flags.add(accecptAlert());
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User details are filled successfully.");
			LOG.info("fillNewUserDetailsAndSave component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Issue with filling New User details .");
			componentActualresult.add("fillNewUserDetailsAndSave verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("fillNewUserDetailsAndSave component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyNewUserRegistrationSuccessMsg() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyNewUserRegistrationSuccessMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(assertElementPresent(MyTripsPage.myTripsSuccessRegMsg, "New User is registered successfully"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Success Message verification is successful.");
			LOG.info("verifyNewUserRegistrationSuccessMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Success Message verification is failed.");
			componentActualresult.add("verifyNewUserRegistrationSuccessMsg verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyNewUserRegistrationSuccessMsg component execution Completed");
		}
		return flag;
	}

}
