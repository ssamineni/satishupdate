package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import mx4j.log.Log;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class RiskRatingsLib extends CommonLib {

	TestEngineWeb tWeb = new TestEngineWeb();

	@SuppressWarnings("unchecked")
	public boolean verifyRiskRatingsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyRiskRatingsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().riskRatingsPage();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.mapIFrame, "Switch to Map I Frame"));
			flags.add(switchToFrame(TravelTrackerHomePage.mapIFrame, "Switch to Map I Frame"));
			flags.add(switchToDefaultFrame());
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(JSClick(RiskRatingsPage.riskRatingsLink, "Risk Ratings Link"));
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.riskRatingsHeader, "Risk Ratings Header"));
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingsHeader, "Medical Risks"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Risk Ratings page successfully.");
			LOG.info("verifyRiskRatingsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Risk Ratings page verification failed.");
			componentActualresult.add("verifyRiskRatingsPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyRiskRatingsPage component execution Completed");
		}
		return flag;
	} // end method

	@SuppressWarnings("unchecked")
	public boolean verifyIntlSOSPortalPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyIntlSOSPortalPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link"));
			flags.add(click(TravelTrackerHomePage.intlSOSPortal, "International SOS Portal Link"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			Shortwait();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);

					flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.welcomePage, "Welcome Page"));
					flags.add(assertElementPresent(TravelTrackerHomePage.welcomePage, "Welcome Page"));
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink, "Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified IntlSOS Portal page successfully.");
			LOG.info("verifyIntlSOSPortalPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("IntlSOS Portal page verification failed.");
			componentActualresult.add("verifyIntlSOSPortalPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyIntlSOSPortalPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyOpenCommunicationHistoryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyOpenCommunicationHistoryPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link"));
			flags.add(click(TravelTrackerHomePage.communicationHistory, "Communication History Link in the Home Page"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.printButton, "Print Button"));
			flags.add(assertElementPresent(TravelTrackerHomePage.printButton, "Print Button"));
			flags.add(isElementEnabled(TravelTrackerHomePage.printButton));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToExcelBtn));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToZipBtn));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Open Communication History page successfully.");
			LOG.info("verifyOpenCommunicationHistoryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Open Communication History page verification failed.");
			componentActualresult.add("verifyOpenCommunicationHistoryPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyOpenCommunicationHistoryPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMessageManagerPage() throws Throwable {
		boolean flag = true;

		try {
			LOG.info("verifyMessageManagerPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(click(TravelTrackerHomePage.messageManagerLink, "Message Manager Link under Tools Link"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.printButton, "Print Button"));
			flags.add(assertElementPresent(TravelTrackerHomePage.printButton, "Print Button"));
			flags.add(isElementEnabled(TravelTrackerHomePage.printButton));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToExcelBtn));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToZipBtn));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Message Manager page successfully.");
			LOG.info("verifyMessageManagerPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Message Manager page verification failed.");
			componentActualresult.add("verifyMessageManagerPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyMessageManagerPage component execution Completed");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyHelpPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyHelpPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.helpLink, "Help Link in the Home page"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();

			Shortwait();
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					LOG.info("Title is : " + Driver.switchTo().window(windowHandle).getCurrentUrl());
					if (Driver.switchTo().window(windowHandle).getCurrentUrl().contains("pdf"))
						LOG.info("PDF is successfully validated");
					else
						LOG.info("PDF is NOT validated");
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink, "Feedback Link"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Help page successfully.");
			LOG.info("verifyHelpPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Help page verification failed.");
			componentActualresult.add("verifyHelpPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyHelpPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyFeedbackPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyFeedbackPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.feedbackLink, "Feedback Link in the Home Page"));

			Longwait();
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);

			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_CONTROL);

			Shortwait();
			accecptAlert();
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink, "Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Feedback page successfully.");
			LOG.info("verifyFeedbackPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Feedback page verification failed.");
			componentActualresult.add("verifyFeedbackPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyFeedbackPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUserSettingsPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyUserSettingsPage component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link in the Home Page"));
			flags.add(click(TravelTrackerHomePage.userSettingsLink, "User Settings Link under Tools Link"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(TravelTrackerHomePage.updateEmailAddress, "Update Email Address"));
			flags.add(isElementEnabled(TravelTrackerHomePage.updateButton));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified User Settings page successfully.");
			LOG.info("verifyUserSettingsPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User Settings page verification failed.");
			componentActualresult.add("verifyUserSettingsPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyUserSettingsPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTravelMedicalRisks(String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelMedicalRisks component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().riskRatingsPage();
			flags.add(selectByIndex(RiskRatingsPage.riskRatingsCustomer, 74, "Risk Ratings Customer"));
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry, "Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName, "Select Countries Search Box"));
			flags.add(click(RiskRatingsPage.medicalCheckBox, "Medical CheckBox"));
			flags.add(click(RiskRatingsPage.travelCheckbox, "Travel Checkbox"));
			
			flags.add(waitForElementPresent(createDynamicEle(RiskRatingsPage.selectedCountryMedical, countryName), "Medical Risk of selected country",60));
			flags.add(waitForElementPresent(createDynamicEle(RiskRatingsPage.selectedCountryTravel, countryName), "Travel Risk of selected country",60));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Travel Medical Risks successfully.");
			LOG.info("verifyTravelMedicalRisks component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Travel Medical Risks is failed.");
			componentActualresult.add("verifyTravelMedicalRisks verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTravelMedicalRisks component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateTravelMedicalRisks() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("updateTravelMedicalRisks component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().riskRatingsPage();
			flags.add(click(RiskRatingsPage.mediumMedical, "Medical Checkbox for Medium"));
			flags.add(click(RiskRatingsPage.mediumTravel, "Travel Checkbox for Medium"));
			flags.add(JSClick(RiskRatingsPage.applyRiskRatingsBtn, "Apply Your Risk Ratings Button"));
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.riskRatingSuccessMsg, "Risk Rating Success Message"));
			flags.add(waitForElementPresent(RiskRatingsPage.riskRatingSuccessMsg, "Risk Rating Success Message", 60));
			flags.add(assertElementPresent(RiskRatingsPage.riskRatingSuccessMsg, "Risk Rating Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Risk Ratings page successfully.");
			LOG.info("updateTravelMedicalRisks component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Risk Ratings page verification failed.");
			componentActualresult.add("updateTravelMedicalRisks verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("updateTravelMedicalRisks component execution Completed");
		}
		return flag;
	} // end method

	@SuppressWarnings("unchecked")
	public boolean verifyLastUpdatedMsg() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyLastUpdatedMsg component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().riskRatingsPage();
			flags.add(assertElementPresent(RiskRatingsPage.lastUpdatedTime, "Last Updated Time"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Last Updated message successfully.");
			LOG.info("verifyLastUpdatedMsg component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Last Updated message is failed.");
			componentActualresult.add("verifyLastUpdatedMsg verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyLastUpdatedMsg component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTravelAndMedicalRatings(String countryName) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTravelAndMedicalRatings component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().riskRatingsPage();

			Shortwait();
			flags.add(waitForVisibilityOfElement(RiskRatingsPage.searchCountry, "Select Countries Search Box"));
			flags.add(isElementPresent(RiskRatingsPage.searchCountry, "Select Countries Search Box"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName, "Select Countries Search Box"));
			
			flags.add(waitForElementPresent(createDynamicEle(RiskRatingsPage.verifyMedical, countryName), "Verify Medical Checkbox", 60));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Last Updated message successfully.");
			LOG.info("verifyTravelAndMedicalRatings component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Last Updated message is failed.");
			componentActualresult.add("verifyTravelAndMedicalRatings verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTravelAndMedicalRatings component execution Completed");
		}
		return flag;
	}

	/**
	 * jhdf.
	 */
	@SuppressWarnings("unchecked")
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("typeNewsReportName component execution Started");
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);

			} else {
				enterTextIntoAlert(chars);
			}
			LOG.info("typeNewsReportName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("typeNewsReportName verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("typeNewsReportName component execution Completed");

		}
		return flag;
	}

}
