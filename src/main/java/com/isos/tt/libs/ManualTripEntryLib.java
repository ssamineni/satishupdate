package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;

import com.automation.accelerators.ActionEngine;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class ManualTripEntryLib extends CommonLib {

	int randomNumber = generateRandomNumber();
	String tripName = "InternationalSOS" + randomNumber;

	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntryPage() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyManualTripEntryPage component execution started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new ManualTripEntryPage().manualTripEntryPage();

			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			flags.add(click(TravelTrackerHomePage.toolsLink, "Tools link in Home Page"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link under Tools link"));
			flags.add(JSClick(ManualTripEntryPage.manualTripEntryLink, "Manual Trip Entry Link under Tools link"));
			flags.add(assertElementPresent(ManualTripEntryPage.selectTraveller, "Select Traveller Header"));
			flags.add(isElementEnabled(ManualTripEntryPage.selectButton));
			flags.add(isElementEnabled(ManualTripEntryPage.createNewTravellerBtn));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified ManualTrip Entry page successfully.");
			LOG.info("verifyManualTripEntryPage component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("ManualTrip Entry page verification failed.");
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("verifyManualTripEntryPage verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyManualTripEntryPage component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTraveller(String firstName, String middleName, String lastName, String comments,
			String phoneNumber, String emailAddress, String contractorId) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("createNewTraveller component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new SiteAdminPage().siteAdminPage();
			new ManualTripEntryPage().manualTripEntryPage();
			flags.add(click(ManualTripEntryPage.createNewTravellerBtn, "Create New Traveller Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.firstName, firstName, "First Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.middleName, middleName, "Middle Name in Create New Traveller"));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "Last Name in Create New Traveller"));
			flags.add(selectByIndex(ManualTripEntryPage.homeCountryList, 13, "Home Country List "));
			flags.add(type(ManualTripEntryPage.comments, comments, "Comments Text Area"));
			flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "Home Site List"));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1, "Phone Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "Phone Type List"));
			flags.add(selectByIndex(ManualTripEntryPage.countryCode, 12, "Country Code List"));
			flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber, "Phone Number"));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1, "Email Priority List"));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "Email Type List"));
			flags.add(type(ManualTripEntryPage.emailAddress, emailAddress, "Email Address "));
			flags.add(type(ManualTripEntryPage.contractorId, contractorId, "Contractor Id"));
			flags.add(selectByIndex(ManualTripEntryPage.department, 3, "Department List"));
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2, "Document Country Code List"));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1, "Document Type List"));
			flags.add(click(ManualTripEntryPage.saveTravellerDetails, "Save Traveller Details"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"First Name in Create New Traveller", 60));
			flags.add(assertElementPresent(ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"Traveller Details Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("createNewTraveller is performed successfully.");
			LOG.info("createNewTraveller component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("createNewTraveller is failed.");
			componentActualresult.add("createNewTraveller verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("createNewTraveller component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean copyTripProfileDetails(String tripName, String addTraveller) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("copyTripProfileDetails component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new MyTripsPage().myTripsPage();
			new ManualTripEntryPage().manualTripEntryPage();
			new SiteAdminPage().siteAdminPage();
			flags.add(click(ManualTripEntryPage.copyTrip, "Copy Trip Link"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.tripName, "Trip Name", 60));
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			flags.add(type(ManualTripEntryPage.addTraveller, addTraveller, "Add Traveller"));
			Shortwait();
			flags.add(
					isElementPresent(ManualTripEntryPage.addTravellersDropDownPanel, "Add Travellers Drop Down Panel"));
			flags.add(click(ManualTripEntryPage.addTravellersDropDownPanel, "Add Travellers Drop Down Panel"));
			flags.add(click(ManualTripEntryPage.addTravellerbtn, "Add Traveller Button"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(waitForElementPresent(MyTripsPage.saveTripInfo, "Save Trip Information", 60));
			flags.add(click(MyTripsPage.saveTripInfo, "Save Trip Information"));
			flags.add(waitForInVisibilityOfElement(SiteAdminPage.progressImage, "Progress Image"));
			flags.add(
					waitForElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message", 60));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage, "Trip Information Success Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("copyTripProfileDetails is performed successfully.");
			LOG.info("copyTripProfileDetails component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("copyTripProfileDetails is failed.");
			componentActualresult.add("copyTripProfileDetails verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("copyTripProfileDetails component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripNameMTE(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("enterTripName_MTE component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().myTripsPage();
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Trip name is entered successfully.");
			LOG.info("enterTripName_MTE component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name entry is failed.");
			componentActualresult.add("enterTripName_MTE verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("enterTripName_MTE component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTripCreated() throws Throwable {
		boolean flag = true;
		try {
			LOG.info("verifyTripCreated component execution Started");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().travelTrackerHomePage();
			new ManualTripEntryPage().manualTripEntryPage();
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.verifyTripName, "Trip Name"));
			flags.add(assertElementPresent(ManualTripEntryPage.verifyTripName, "Trip Name"));
			String tripNameText = getText(ManualTripEntryPage.verifyTripName, "Trip Name");

			flags.add(assertTextMatching(ManualTripEntryPage.verifyTripName, tripName, "Trip Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create Trip verification is successful.");
			LOG.info("verifyTripCreated component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create Trip verification is failed.");
			componentActualresult.add("verifyTripCreated verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("verifyTripCreated component execution Completed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			LOG.info("typeNewsReportName component execution Started");
			// List<Boolean> flags = new ArrayList<>();
			//
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// System.out.println("createReport took " + duration +
				// " milliseconds");
			} else {
				enterTextIntoAlert(chars);
				
			}
			LOG.info("typeNewsReportName component execution Completed");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("typeNewsReportName verification failed."
					+ getListOfScreenShots(TestScriptDriver.getScreenShotDirectory_testCasePath(), getMethodName()));
			LOG.error("typeNewsReportName component execution Completed");
		}
		return flag;
	}

}
