package com.isos.ecms.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.isos.ecms.page.ContentEditorPage;
import com.isos.ecms.page.DeliveryPandemicPage;


public class DeliveryPandemicLib extends CommonECMSLib{
	//select_pandemic_latest_news
	//select_latest_news

	String LatestHeaderNews="";
	
	
	@SuppressWarnings("unchecked")
	public boolean selectPandemicLatestNews() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.PandemicLatestNews, "Latest News", 60));
			String LNewsHeader = getText(DeliveryPandemicPage.PandemicLatestNews,
					"latest news");
			System.out.println("LNewsHeader values : "+LNewsHeader);
			LatestHeaderNews = LNewsHeader;
			flags.add(click(DeliveryPandemicPage.PandemicLatestNews, "Latest News"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Latest News is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Latest News is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyPandemicOpenedNews() throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			new ContentEditorPage().ContentEditor_Page();
			
			String[] header = LatestHeaderNews.split(":");
			flag = assertTextMatching(ContentEditorPage.LatestNewsHeader, header[1].toUpperCase(),
					"Latest News Header Verification");

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Latest News verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Latest News verification is failed.");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean selectDiseasePandemicStie(String DiseaseName) throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			System.out.println("select disease1");
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			By diseaseNameText = By.xpath("//*[@id='sidebar']//a[text()='"+DiseaseName+"']");
			
			flags.add(waitForElementPresent(diseaseNameText, "Item Structure", 10));
			flags.add(click(diseaseNameText, "Item Structure"));
			
			System.out.println("select disease2");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pandemic Site Disease selection is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Pandemic Site Disease selection is failed.");
		}
		return flag;
	}
	
	public boolean verifyingDiseasePandemicSite(String DiseaseName,String DiseaseComment) throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.diseaseDetails, "Disease Detiails", 10));
			flags.add(click(DeliveryPandemicPage.diseaseDetails, "Disease Details"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pandemic Site Disease verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Pandemic Site Disease verification is failed.");
		}
		return flag;
	}
	
	//verifyPandemicHomePageUI
	public boolean verifyPandemicHomePageUI() throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			//flags.add(waitForElementPresent(DeliveryPandemicPage.logo, "Pandemic Site Logo", 30));
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.loginBtn, "Login Button", 30));
			flags.add(isElementPresent(DeliveryPandemicPage.loginBtn, "Login Button"));
			flags.add(isElementPresent(DeliveryPandemicPage.activeThreats1Header, "Active Threats1 Header"));
			flags.add(isElementPresent(DeliveryPandemicPage.latestNewsHeader, "Latest News Header"));
			flags.add(isElementPresent(DeliveryPandemicPage.editorialsHeader, "Editorials Header"));
			flags.add(isElementPresent(DeliveryPandemicPage.pandemicHomeLink, "Pandemic Home Link"));
			flags.add(isElementPresent(DeliveryPandemicPage.contactUsLink, "ContactUs Link"));
			flags.add(isElementPresent(DeliveryPandemicPage.privacyAndCookiesLink, "Privacy And Cookies Link"));
			flags.add(isElementPresent(DeliveryPandemicPage.disclaimerLink, "Disclaimer Link"));
			flags.add(isElementPresent(DeliveryPandemicPage.footerLogo, "Footer Logo"));
			flags.add(isElementPresent(DeliveryPandemicPage.overviewMenu, "Overview Menu"));
			flags.add(isElementPresent(DeliveryPandemicPage.pandemicSubMenu, "Pandemic SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.birdFluSubMenu, "BirdFlu SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.mersCovSubMenu, "Mers Cov SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.seasonalFluSubMenu, "Seasonal Flu SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.subscribersMenu, "Subscribers Menu"));
			flags.add(isElementPresent(DeliveryPandemicPage.referencesSubMenu, "References SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.planningToolsSubMenu, "Planning Tools SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.travelAdviceSubMenu, "TravelAdvice SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.emailSignUpSubMenu, "Email SignUp SubMenu"));
			
			flags.add(isElementPresent(DeliveryPandemicPage.countriesMenu, "Countries Menu"));
			flags.add(isElementPresent(DeliveryPandemicPage.mapListCountriesSubMenu, "MapList Countries SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.newsroomMenu, ""));
			flags.add(isElementPresent(DeliveryPandemicPage.newsSubMenu, "News SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.newsroomEditorialsSubMenu, "Newsroom Editorials SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.faqsSubMenu, "Faqs SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.ebolaMenu, "Ebola Menu"));
			flags.add(isElementPresent(DeliveryPandemicPage.ebolaNewsSubMenu, "Ebola News SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.toolsSubMenu, "Tools SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.factsSubMenu, "Facts SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.ebolatravelAdviceSubMenu, "Ebolatravel Advice SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.affectedCountriesSubMenu, "Affected Countries SubMenu"));
			flags.add(isElementPresent(DeliveryPandemicPage.mapBoxInfo, "Map Box Info"));
			flags.add(isElementPresent(DeliveryPandemicPage.logo, "Pandemic Site Logo"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pandemic Home Page UI verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Pandemic Home Page UI verification is failed.");
		}
		return flag;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean verifyHeaderMenu(String ReportName, String ReporDetails) throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.SearchBox, "SearchBox", 30));
			flags.add(type(DeliveryPandemicPage.SearchBox,ReportName, "Disease Details"));
			
			flags.add(click(DeliveryPandemicPage.SearchBtn, "SearchButton"));
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.DiseaseResult, "ResultPage", 30));
			flags.add(verify(getText(DeliveryPandemicPage.DiseaseResult, "DiseaseResult"), ReporDetails, "ReportName"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pandemic Site Disease verification is successful .");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Pandemic Site Disease verification is failed.");
		}
		return flag;	
	}
	
	@SuppressWarnings("unchecked")
	public boolean verifyMapInPandemic() throws Throwable{
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new DeliveryPandemicPage().DeliveryPandemic_Page();
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.map, "Pandemic Map", 30));
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.mapBoxInfo, "Map Box Info", 30));
			flags.add(click(DeliveryPandemicPage.mapBoxInfo, "Map Box Info"));
			
			flags.add(waitForElementPresent(DeliveryPandemicPage.mapFeedback, "Map Feedback", 30));			
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Pandemic Site Map verification is successful .");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Pandemic Site Map verification is failed.");
		}
		return flag;	
	}
}
