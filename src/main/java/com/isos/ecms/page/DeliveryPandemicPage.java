package com.isos.ecms.page;

import org.openqa.selenium.By;

public class DeliveryPandemicPage {
	/**
	 * FT_TT_Communication_Send_Message_by_Email_SMS_and_ViewHistory
	 */

	public static By PandemicLatestNews;
	public static By diseaseDetails;
	public static By logo;
	public static By loginBtn;
	public static By activeThreats1Header;
	public static By latestNewsHeader;
	public static By editorialsHeader;
	public static By pandemicHomeLink;
	public static By contactUsLink;
	public static By privacyAndCookiesLink;
	public static By disclaimerLink;
	public static By footerLogo;
	public static By overviewMenu;
	public static By pandemicSubMenu;
	public static By birdFluSubMenu;
	public static By mersCovSubMenu;
	public static By seasonalFluSubMenu;
	public static By subscribersMenu;
	public static By referencesSubMenu;
	public static By planningToolsSubMenu;
	public static By travelAdviceSubMenu;
	public static By emailSignUpSubMenu;
	public static By countriesMenu;
	public static By mapListCountriesSubMenu;
	public static By newsroomMenu;
	public static By newsSubMenu;
	public static By newsroomEditorialsSubMenu;
	public static By faqsSubMenu;	
	public static By ebolaMenu;
	public static By ebolaNewsSubMenu;
	public static By toolsSubMenu;
	public static By factsSubMenu;
	public static By ebolatravelAdviceSubMenu;
	public static By affectedCountriesSubMenu;	
	public static By mapBoxInfo;
	
	public static By SearchBox;
	public static By SearchBtn;
	public static By DiseaseResult;
	
	public static By map;
	public static By mapFeedback;

	public void DeliveryPandemic_Page() {

		PandemicLatestNews = By.xpath("//*[@class='vert-scroll']//li[1]/a");
		diseaseDetails = By.xpath("//*[@id='main']/div[1]/h2");

		logo = By.xpath("//a[@href='http://www.internationalsos.com']/img[@src='/~/media/upload/logo_isos_mobile_alt.jpg?h=31&la=en&w=225']");
		loginBtn = By.xpath("//a[text()='Member Log In']");
		
		activeThreats1Header = By.xpath("//div[@id='sidebar']//h3[contains(text(),'Active threats1')]");
		latestNewsHeader = By.xpath("//h2[contains(text(),'Latest News')]");
		editorialsHeader = By.xpath("//h2[contains(text(),'Editorials')]");
		pandemicHomeLink = By.xpath("//a[text()='Pandemic Home']");
		contactUsLink = By.xpath("//a[text()='Contact Us']");
		privacyAndCookiesLink = By.xpath("//a[text()='Privacy and Cookies']");
		disclaimerLink = By.xpath("//a[text()='Disclaimer']");
		footerLogo = By.xpath("//div[@id='footer']//a/img");
		
		overviewMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/overview']");
		pandemicSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/overview/pandemic']");
		birdFluSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/overview/bird-flu-overview']");
		mersCovSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/overview/mers-overview']");
		seasonalFluSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/overview/seasonal-flu']");
		
		subscribersMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers']");
		referencesSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-references']");
		planningToolsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-tools']");
		travelAdviceSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-travel-advice']");
		emailSignUpSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-email-signup']");
		        
		countriesMenu = By.xpath("//div[@id='nav-container']//a[contains(text(),'   Countries')]");
		mapListCountriesSubMenu = By.xpath("//div[@id='nav-container']/nav/ul//ul//a[@href='http://ecms-qa-pandemic.intlsos.com/countries']");

		newsroomMenu = By.xpath("//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-editorials' and contains(text(),'  Newsroom')]");
		newsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-news' and contains(text(),'  News')]");
		newsroomEditorialsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/subscribers/pandemic-editorials' and contains(text(),'  Editorials')]");
		faqsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/mers-faqs']");

		ebolaMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola']");
		ebolaNewsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola/ebola-news']");
		toolsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola/ebola-tools']");
		factsSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola/ebola-facts']");
		ebolatravelAdviceSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola/ebola-travel-advice']");
		affectedCountriesSubMenu = By.xpath("//div[@id='nav-container']//a[@href='http://ecms-qa-pandemic.intlsos.com/ebola/ebola-affected-countries']");
		
		mapBoxInfo = By.xpath("//a[contains(@class,'mapbox-info-toggle')]");
		
		
		//FT_ECMS_110_Pandemic_Add_AlertReport - verifyHeaderMenu
		
		SearchBox = By.xpath("//div[@id='bar-search']/form/input[@id='SearchTerm']");
		SearchBtn = By.xpath("//div[@id='bar-search']//input[@value='Go']");
		DiseaseResult = By.xpath("//div[@id='main']//a");
		
		map = By.id("map");
		mapFeedback = By.xpath("//a[contains(@href,'map-feedback')]");
		
	}

}
