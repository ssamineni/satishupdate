package com.sample.TestCaseCreationUtility;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.automation.report.ReporterConstants;

public class FindMethods {

	static String packageName_test = ReporterConstants.packagename_libs;

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 *
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList<Class> getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 *
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(
						Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}

	@SuppressWarnings("rawtypes")
	public String getClassNameOfMethod(String namePassed) {
		String className = "";
		try {
			ArrayList<Class> cls = getClasses(packageName_test);
			for (Class c : cls) {
				Method[] m = c.getDeclaredMethods();
				for (int i = 0; i < m.length; i++) {
					if (m[i].getName().equalsIgnoreCase(namePassed)) {
						className = c.getSimpleName();
						break;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return className;
	}

	public String getObjectOfClass(String className) {

		String obj = "";
		try {
			if (className.equalsIgnoreCase("NewsLib"))
				obj = "nlib";
			if (className.equalsIgnoreCase("ManualTripEntryLib"))
				obj = "mtelib";
			if (className.equalsIgnoreCase("MyTripsLib"))
				obj = "myTripslib";
			if (className.equalsIgnoreCase("RiskRatingsLib"))
				obj = "risklib";
			if (className.equalsIgnoreCase("SiteAdminLib"))
				obj = "sitelib";
			if (className.equalsIgnoreCase("TTLib"))
				obj = "ttlib";
			if (className.equalsIgnoreCase("CommonLib"))
				obj = "clib";
			if (className.equalsIgnoreCase("CommonECMSLib"))
				obj = "ecmscommonlib";
			if (className.equalsIgnoreCase("DeliveryPandemicLib"))
				obj = "delvpanlib";
			if (className.equalsIgnoreCase("DesktopLib"))
				obj = "dlib";
			if (className.equalsIgnoreCase("ContentEditorLib"))
				obj = "celib";
		} catch (Exception ex) {

		}
		return obj;
	}

}
