package com.automation.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.automation.CSVUility.CsvHandler;
import com.automation.CSVUility.Xls_Reader;

public class Dbupdation extends CsvHandler {

	private static final Logger LOG = Logger.getLogger(Dbupdation.class);

	public static Connection con;
	//	public static int batchID;

	public static int execution_ID;

	public Dbupdation() throws ClassNotFoundException, SQLException {
		String dbUrl = "jdbc:mysql://localhost:3306/isos";
		String username = "root";
		String password = "root";
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection(dbUrl, username, password);
		LOG.info("connection established ");
	}

	public void setDatabaseValuesForExecutionTable_CreateNewExecutionId(Xls_Reader xls)
			throws SQLException, ClassNotFoundException {

		String sheetName = ExecutionDetails;

		String executiondate = xls.getCellData(sheetName, "ExecutionDate", 2);
		String ExecutionStartTime = xls.getCellData(sheetName, "ExecutionStartTime", 2);
		String ExecutionEndTime = xls.getCellData(sheetName, "ExecutionEndTime", 2);
		String Comments = xls.getCellData(sheetName, "Comments", 2);


		// Create Statement Object
		Statement stmt = con.createStatement();
		String query = "INSERT INTO isos.testexecution (EXECUTION_DATE,COMMENTS,EXECUTION_START_TIME,EXECUTION_END_TIME) VALUES"
				+ " ('"
				+ executiondate
				+ "','"
				+ Comments
				+ "','"
				+ ExecutionStartTime
				+ "',"
				+ "'"
				+ ExecutionEndTime
				+ "');";

		stmt.executeUpdate(query);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForProjectTable(int executionId)
			throws SQLException, ClassNotFoundException {

		HashMap projects = CsvHandler.getProjectsMap();

		Set<String> projKey = projects.keySet();
		for(String proj : projKey){
			String projectName = proj;
			LOG.info("projectName - " + projectName);
			int projectId = Integer.parseInt((String) projects.get(proj));
			LOG.info("projectID - " + projectId);

			// Create Statement Object
			Statement stmt = con.createStatement();
			String query = "INSERT INTO isos.testproject (EXECUTION_ID,PROJECT_ID,PROJECT_NAME) VALUES"
					+ " ("
					+ executionId
					+ ","
					+ projectId
					+ ",'"
					+ projectName
					+ "');";
			Statement stmt_count = con.createStatement();
			String query_Count = "Select Count(*) as total from isos.testproject where EXECUTION_ID="+ executionId+" and PROJECT_ID="+projectId+";";
								///
			ResultSet RS = stmt_count.executeQuery(query_Count);
			while (RS.next()) {
	            if(!( RS.getInt("total") > 0) ) 
	            	stmt.executeUpdate(query);
	        }
	        
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForPlanTable(int executionId)
			throws SQLException, ClassNotFoundException {

		//	HashMap plans = CsvHandler.getPlansMap();
		List<Integer> projectIds = getProjectsIdList();
		Set<Integer> projectSet = new HashSet<Integer>(projectIds);
		for(int proj : projectSet ){
			int projId = proj;
			HashMap plans = CsvHandler.getPlansMap(String.valueOf(projId));
			Set<String> planKeyset = plans.keySet();
			for(String planId : planKeyset){
				//String planId = plan;
				LOG.info("planName - " + plans.get(planId));
				String planName =  plans.get(planId).toString();
				//int planId = Integer.parseInt((String) plans.get(plan));
				LOG.info("planID - " + planId);

				LOG.info("projectId - " + projId);
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testplan (EXECUTION_ID,PLAN_ID,PLAN_NAME,PROJECT_ID) VALUES"
						+ " ("
						+ executionId
						+ ","
						+ Integer.parseInt((String) planId)
						+ ",'"
						+ planName
						+ "',"
						+ projId
						+");";
				Statement stmt_count = con.createStatement();
				String query_Count = "Select Count(*) as total from isos.testplan where EXECUTION_ID="+ executionId+" and PROJECT_ID="+projId+" "
						+ "and PLAN_ID="+planId+";";
				ResultSet RS = stmt_count.executeQuery(query_Count);
				while (RS.next()) {
					if(!( RS.getInt("total") > 0) ) 
						stmt.executeUpdate(query);
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void setDatabaseValuesForRunTable(int executionId)
			throws SQLException, ClassNotFoundException {

		//	HashMap plans = CsvHandler.getPlansMap();
		List planIds = getPlanIdList();
		Set<Integer> planSet = new HashSet<Integer>(planIds);
		for(int planId : planSet){
		//	int planId = (int) planIds.get(i);
			HashMap runs = CsvHandler.getRunsMap(String.valueOf(planId));
			Set<String> runsKeyset = runs.keySet();
			for(String runIds : runsKeyset){
				String runName = runs.get(runIds).toString();
				LOG.info("runName - " + runName);
				int runId = Integer.parseInt((String) runIds);
				LOG.info("runID - " + runId);

				LOG.info("planId - " + planId);
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testrun (EXECUTION_ID,PLAN_ID,RUN_ID,RUN_NAME) VALUES"
						+ " ("
						+ executionId
						+ ","
						+ planId
						+ ","
						+ runId
						+ ",'"
						+ runName
						+"');";
				Statement stmt_count = con.createStatement();
				String query_Count = "Select Count(*) as total from isos.testrun where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
						+ "and PLAN_ID="+planId+";";
				ResultSet RS = stmt_count.executeQuery(query_Count);
				while (RS.next()) {
					if(!( RS.getInt("total") > 0) ) 
						stmt.executeUpdate(query);
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void setDatabaseValuesForSuiteTable(int executionId)
			throws SQLException, ClassNotFoundException {

		//	HashMap plans = CsvHandler.getPlansMap();
		List runIds = getRunIdList();
		Set<Integer> runIdsSet = new HashSet<Integer>(runIds);
		for(int runId : runIdsSet){
			//int runId = (int) runIds.get(i);
			HashMap suites = CsvHandler.getSuitesMap(String.valueOf(runId));
			Set<String> suitesKeyset = suites.keySet();
			for(String suiteIds : suitesKeyset){
				String suiteName = suites.get(suiteIds).toString();
				LOG.info("SuiteName - " + suiteName);
				String suiteId = ((String) suiteIds);
				if(suiteId.isEmpty())
					suiteId = "1";
				LOG.info("suiteID - " + suiteId);
				LOG.info("runId - " + runId);
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testsuite (EXECUTION_ID,RUN_ID,SUITE_ID,SUITE_NAME) VALUES"
						+ " ("
						+ executionId
						+ ","
						+ runId
						+ ","
						+ Integer.parseInt(suiteId)
						+ ",'"
						+ suiteName
						+"');";
				Statement stmt_count = con.createStatement();
				String query_Count = "Select Count(*) as total from isos.testsuite where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
						+ "and SUITE_ID="+Integer.parseInt(suiteId)+";";
				ResultSet RS = stmt_count.executeQuery(query_Count);
				while (RS.next()) {
					if(!( RS.getInt("total") > 0) ) 
						stmt.executeUpdate(query);
				}
			}
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForTestCaseTable(int executionId)
			throws SQLException, ClassNotFoundException {

		//	HashMap plans = CsvHandler.getPlansMap();
		List runIds = getRunIdList();
		Set<Integer> runIdsSet = new HashSet<Integer>(runIds);
		for(int runId : runIdsSet){
			//int runId = (int) runIds.get(i);
			List<String> testCases = CsvHandler.getTestCasesForRun(String.valueOf(runId));
			Set<String> testCasesSet = new HashSet<String>(testCases);
			for(String test : testCasesSet){
				String testName = test;
				LOG.info("testcaseName - " + testName);
				Hashtable testinfo = CsvHandler.getTestCasesInfo(String.valueOf(runId), testName);

				String testCaseStatus = (String) testinfo.get("test_Status");
				String testCaseStartTime = (String) testinfo.get("testStartTime");
				String testCaseEndTime = (String) testinfo.get("testEndTime");
				String testCaseId = (String) testinfo.get("testCaseId");
				// Create Statement Object
				Statement stmt = con.createStatement();
				String query = "INSERT INTO isos.testcase (EXECUTION_ID,RUN_ID,TESTCASE_ID,TESTCASE_NAME,TESTCASE_STATUS,"
						+ "TEST_START_TIME,TEST_END_TIME) VALUES"
						+ " ("
						+ executionId
						+ ","
						+ runId
						+ ","
						+ Integer.parseInt(testCaseId)
						+ ",'"+ testName+"','"
						+testCaseStatus+"','"+testCaseStartTime+"','"+testCaseEndTime+"');";
				Statement stmt_count = con.createStatement();
				String query_Count = "Select Count(*) as total from isos.testcase where EXECUTION_ID="+ executionId+" and RUN_ID="+runId+" "
						+ "and TESTCASE_ID="+Integer.parseInt(testCaseId)+";";
				ResultSet RS = stmt_count.executeQuery(query_Count);
				while (RS.next()) {
					if(!( RS.getInt("total") > 0) ) 
						stmt.executeUpdate(query);
				}
			}
		}
	}
	

	public int getLatestExecutionId() throws NumberFormatException, SQLException{
		// to get the batch id
		Statement stmt1 = con.createStatement();
		String query1 = "SELECT MAX(EXECUTION_ID) FROM isos.testexecution;";
		ResultSet rs = stmt1.executeQuery(query1);
		execution_ID = 1;
		while (rs.next()) {
			execution_ID = Integer.parseInt(rs.getString(1));
		}
		LOG.info("latest execution id" + execution_ID);
		
		return execution_ID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDatabaseValuesForComponentTable(Xls_Reader xls, int executionId){ 
		try {
			String sheetName = "TestCaseResult";
			List<Integer> runs = CsvHandler.getRunIdList();
			Set<Integer> runIds = new HashSet<Integer>(runs);
			for(int run : runIds){

				for (int i = 2; i <= xls.getRowCount(sheetName); i++) {

					String run_temp = xls.getCellData(sheetName, " test_run_id", i);

					if(run_temp.equalsIgnoreCase(String.valueOf(run))){
						String currtestcase = xls.getCellData(sheetName, "test_title", i);
						if (!currtestcase.isEmpty()) {
							String testCaseId = xls.getCellData(sheetName, "test_case_id", i);
							// get the method string for the test case
							for (int j = i; j <= xls.getRowCount(sheetName); j++) {
								String command = xls.getCellData(sheetName, "Step", j);
								if (command.isEmpty()) {
									break;
								}

								String componentStatus = xls.getCellData(sheetName, "Step_Status", j);
								String componentId =  xls.getCellData(sheetName, "Step_Id", j);
								String componentStartTime =  xls.getCellData(sheetName, "component_StartTime", j);
								String componentEndTime =  xls.getCellData(sheetName, "component_EndTime", j);

								//
								// Create Statement Object
								Statement stmt = con.createStatement();
								Statement stmt_count = con.createStatement();
								String query = "INSERT INTO isos.testcomponent (EXECUTION_ID,TESTCASE_ID,STEP_NAME,"
										+ "STEP_STATUS,STEP_NUMBER,"
										+ "STEP_START_TIME,STEP_END_TIME) VALUES"
										+ " ("
										+ executionId
										+ ","
										+ Integer.parseInt(testCaseId)
										+ ",'"
										+ command
										+ "','"+ componentStatus+"',"
										+Integer.parseInt(componentId)+",'"+componentStartTime+"','"+componentEndTime+"');";

								String query_Count = "Select Count(*) as total from isos.testcomponent where EXECUTION_ID="+ executionId+""
										+ " and STEP_NUMBER="+Integer.parseInt(componentId)+" "
										+ "and TESTCASE_ID="+Integer.parseInt(testCaseId)+";";
								ResultSet RS = stmt_count.executeQuery(query_Count);
								while (RS.next()) {
									if(!( RS.getInt("total") > 0) ) 
										stmt.executeUpdate(query);
								}
								///	
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
		
	public static boolean enterDetailsInDatabaseAfterExecution(String fileName){
		boolean flag = false;
		try{
			Dbupdation dbObj = new Dbupdation();
			CsvHandler.xls = new Xls_Reader(fileName);

			String execId_inExcel = xls.getCellData("ExecutionDetails", "Execution_ID", 2);
			if(execId_inExcel.isEmpty()){
				//initial insertion in db, execution is generated
				dbObj.setDatabaseValuesForExecutionTable_CreateNewExecutionId(CsvHandler.xls);
				//get the latest execution id generated
				int executionId_DB = dbObj.getLatestExecutionId();
				//set the execution id to the excel sheet
				CsvHandler.setExecutionID(String.valueOf(executionId_DB));
			}
			int executionId = Integer.parseInt(xls.getCellData("ExecutionDetails", "Execution_ID", 2));

			dbObj.setDatabaseValuesForProjectTable(executionId);
			dbObj.setDatabaseValuesForPlanTable(executionId);
			dbObj.setDatabaseValuesForRunTable(executionId);
			dbObj.setDatabaseValuesForSuiteTable(executionId);
			dbObj.setDatabaseValuesForTestCaseTable(executionId);
			dbObj.setDatabaseValuesForComponentTable(CsvHandler.xls, executionId);
			dbObj.closeConnection();
			flag = true;
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return flag;
	}



	public void closeConnection() throws SQLException {
		con.close();
	}
}
