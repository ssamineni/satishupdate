package com.automation.testrail;

import com.isos.ecms.libs.CommonECMSLib;
import com.isos.ecms.libs.DeliveryPandemicLib;
import com.isos.ecms.libs.DesktopLib;
import com.isos.tt.libs.CommonLib;
import com.isos.ecms.libs.ContentEditorLib;
import com.isos.tt.libs.ManualTripEntryLib;
import com.isos.tt.libs.MyTripsLib;
import com.isos.tt.libs.NewsLib;
import com.isos.tt.libs.RiskRatingsLib;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTLib;

import java.util.HashMap;

import com.automation.CSVUility.CsvHandler;

import org.testng.SkipException;
import org.testng.annotations.*;

import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;


/**
 * @author E002149
 *
 */


public class Temp {

	NewsLib nlib = new NewsLib();
	ManualTripEntryLib mtelib= new ManualTripEntryLib();
	MyTripsLib myTripslib = new MyTripsLib();
	RiskRatingsLib risklib = new RiskRatingsLib();
	SiteAdminLib sitelib = new SiteAdminLib();
	TTLib ttlib = new TTLib();
	CommonLib clib = new CommonLib();
	ContentEditorLib celib = new ContentEditorLib();
	DesktopLib dlib = new DesktopLib();
	CommonECMSLib ecmscommonlib=new CommonECMSLib();
	DeliveryPandemicLib delvpanlib = new DeliveryPandemicLib();
	com.automation.testrail.TestRail trl = new TestRail(); 
	//public static String currtestcaseTitle;
	public static HashMap<Integer, Boolean> testStepStatus = new HashMap<Integer, Boolean>();
	public static String  testCaseId="";
	
	
	@AfterClass
	public void afterClass() {
		String fileName = Thread.currentThread().getStackTrace()[1].getFileName();
		TestScriptDriver.currtestcaseTitle = fileName.substring(0, fileName.lastIndexOf("."));
		testCaseId = CsvHandler.getTestCaseId(TestScriptDriver.currtestcaseTitle,
				TestEngineWeb.browser);
		String startTestCaseTime = (CommonLib.getCurrentTime());
		String endTestCaseTime = (CommonLib.getCurrentTime());
		CsvHandler.setTestCaseStatus(testCaseId,
				trl.getTestcaseStatus(testStepStatus), startTestCaseTime,
				endTestCaseTime);
		CsvHandler.setTestStepStatus(testCaseId, testStepStatus,
				CommonLib.componentActualresult, CommonLib.componentStartTimer,
				CommonLib.componentEndTimer);
		
		CsvHandler.setTestStepStatusForUnTestedComponents(testCaseId);
		CsvHandler.setTestCaseStatus(testCaseId,
				CsvHandler.setTestcaseStatusForUnTestedComponents(testCaseId), startTestCaseTime,
				endTestCaseTime);

		CommonLib.componentActualresult.clear();
		CommonLib.componentStartTimer.clear();
		CommonLib.componentEndTimer.clear();
		testStepStatus.clear();

	}
	
}
