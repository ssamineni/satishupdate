package com.automation.CSVUility;

import java.util.List;

public class CreateTestCase {

	private String test_title;
	private String test_suite_id;
	private String test_plan_id;
	private String test_run_id;
	private String project_name;
	

	private String test_run_name;
	private String project_id;
	private String browser;
	private String ID;
	private String test_plan_name;
	private String test_case_id;
	private String test_suite_name;
	private String step;
	
	//private List<String> step;
	private List<String> Expectedstrings;
	
	public CreateTestCase()
	{
		
	}
	
	
	public CreateTestCase(String project_name, String project_id, String test_plan_name, String test_plan_id,
			String test_suite_name, String test_suite_id, String test_run_name,
			String test_run_id, String browser, String test_case_id, String test_title,  String step, List<String> Expectedstrings) {
		
		this.project_name = project_name;
		this.project_id = project_id;
		this.test_plan_name = test_plan_name;
		this.test_plan_id = test_plan_id;
		this.test_suite_name = test_suite_name;
		this.test_suite_id = test_suite_id;
		this.test_run_name = test_run_name;
		this.test_run_id = test_run_id;
		this.browser = browser;
		this.test_case_id = test_case_id;
		this.test_title = test_title;
		this.step = step;
		this.Expectedstrings = Expectedstrings;
	}
	/*public CreateTestCase(String project_name, String project_id, String test_plan_name, String test_plan_id,
			String test_suite_name, String test_suite_id, String test_run_name,
			String test_run_id, String browser, String test_case_id ) {
		
		this.project_name = project_name;
		this.project_id = project_id;
		this.test_plan_name = test_plan_name;
		this.test_plan_id = test_plan_id;
		this.test_suite_name = test_suite_name;
		this.test_suite_id = test_suite_id;
		this.test_run_name = test_run_name;
		this.test_run_id = test_run_id;
		this.browser = browser;
		this.test_case_id = test_case_id;
	}
*/


	public String getTest_title() {
		return test_title;
	}

	public void setTest_title(String test_title) {
		this.test_title = test_title;
	}

	public String getTest_suite_id() {
		return test_suite_id;
	}

	public void setTest_suite_id(String test_suite_id) {
		this.test_suite_id = test_suite_id;
	}

	public String getTest_plan_id() {
		return test_plan_id;
	}

	public void setTest_plan_id(String test_plan_id) {
		this.test_plan_id = test_plan_id;
	}

	public String getTest_run_id() {
		return test_run_id;
	}

	public void setTest_run_id(String test_run_id) {
		this.test_run_id = test_run_id;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public String getTest_run_name() {
		return test_run_name;
	}

	public void setTest_run_name(String test_run_name) {
		this.test_run_name = test_run_name;
	}

	public String getProject_id() {
		return project_id;
	}

	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getTest_plan_name() {
		return test_plan_name;
	}

	public void setTest_plan_name(String test_plan_name) {
		this.test_plan_name = test_plan_name;
	}

	public String getTest_case_id() {
		return test_case_id;
	}

	public void setTest_case_id(String test_case_id) {
		this.test_case_id = test_case_id;
	}

	public String getTest_suite_name() {
		return test_suite_name;
	}

	public void setTest_suite_name(String test_suite_name) {
		this.test_suite_name = test_suite_name;
	}
	
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public List<String> getExpectedstrings() {
		return Expectedstrings;
	}
	public void setExpectedstrings(List<String> expectedstrings) {
		Expectedstrings = expectedstrings;
	}


	@Override
	public String toString() {
		return "ClassPojo [test_title = " + test_title + ", test_suite_id = "
				+ test_suite_id + ", test_plan_id = " + test_plan_id
				+ ", test_run_id = " + test_run_id + ", project_name = "
				+ project_name + ", test_run_name = " + test_run_name
				+ ", project_id = " + project_id + ", browser = " + browser
				+ ", ID = " + ID + ", test_plan_name = " + test_plan_name
				+ ", test_case_id = " + test_case_id + ", test_suite_name = "
				+ test_suite_name + "]";
	}
}
