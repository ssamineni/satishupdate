package com.automation.CSVUility;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.automation.report.ReporterConstants;

public class CsvHandler {
	
	private static final Logger LOG = Logger.getLogger(CsvHandler.class);
	static File Dest;
	public static Xls_Reader xls;
	public static String comments = ReporterConstants.db_ExecutionComments;
	static List<String> test_case_ids = new ArrayList<String>();
	public static void CreateDir(String dirName) throws IOException {
		try {

			new File(".\\TestResults\\" + dirName).mkdir();
			File Source = new File(System.getProperty("user.dir")
					+ "/uploadData/Temp_TestRun.xlsx");
			Dest = new File(System.getProperty("user.dir") + "\\TestResults"
					+ "\\" + dirName);
			FileUtils.copyFileToDirectory(Source, Dest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Code Change End

	static int testCaseResult_rowNum = 2;
	static int testStepResult_rowNum;
	public static final String TestCaseResultSheet = "TestCaseResult";
	protected static final String ExecutionDetails = "ExecutionDetails";

	// static final String TestStepResultSheet = "TestStepResult";

	public static void writeTestCase(String project_name, String project_id,
			String test_plan_name, String test_plan_id, String test_suite_name,
			String test_suite_id, String test_run_name, String test_run_id,
			String testCase_title, String testCaseId, String browser) {

		xls.setCellData(TestCaseResultSheet, "project_name",
				testCaseResult_rowNum, project_name);
		xls.setCellData(TestCaseResultSheet, "project_id",
				testCaseResult_rowNum, String.valueOf(project_id));
		xls.setCellData(TestCaseResultSheet, "test_plan_name",
				testCaseResult_rowNum, test_plan_name);
		xls.setCellData(TestCaseResultSheet, "test_plan_id",
				testCaseResult_rowNum, String.valueOf(test_plan_id));
		xls.setCellData(TestCaseResultSheet, "test_suite_name",
				testCaseResult_rowNum, test_suite_name);
		xls.setCellData(TestCaseResultSheet, "test_suite_id",
				testCaseResult_rowNum, String.valueOf(test_suite_id));

		xls.setCellData(TestCaseResultSheet, "test_run_name",
				testCaseResult_rowNum, test_run_name);
		xls.setCellData(TestCaseResultSheet, "test_run_id",
				testCaseResult_rowNum, String.valueOf(test_run_id));
		xls.setCellData(TestCaseResultSheet, "test_title",
				testCaseResult_rowNum, testCase_title);
		xls.setCellData(TestCaseResultSheet, "test_case_id",
				testCaseResult_rowNum, testCaseId);
		LOG.info("==== Browser - " + browser + " ==== runId -> " + test_run_id);
		xls.setCellData(TestCaseResultSheet, "browser", testCaseResult_rowNum,
				browser);
		// project_name project_id test_plan_name test_plan_id
		// test_suite_name test_suite_id test_run_name test_run_id title
		// test_Status
		// testCaseResult_rowNum++;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void writeTestCaseSteps(List testSteps,
			List expectedStatements) {
		// Step_Id Step Expected_Result Step_Status Actual_Result

		LOG.info("testCaseResult_rowNum - " + testCaseResult_rowNum);
		LOG.info("testSteps size - " + testSteps.size());
		for (int i = 0; i < testSteps.size(); i++) {

			xls.setCellData(TestCaseResultSheet, "Step_Id",
					testCaseResult_rowNum, String.valueOf(i + 1));
			xls.setCellData(TestCaseResultSheet, "Step", testCaseResult_rowNum,
					testSteps.get(i).toString());
			xls.setCellData(TestCaseResultSheet, "Expected_Result",
					testCaseResult_rowNum, expectedStatements.get(i).toString());
			testCaseResult_rowNum++;

		}

		testCaseResult_rowNum++;
		LOG.info("after the program testCaseResult_rowNum - "
				+ testCaseResult_rowNum);

	}
	
	
	
	

	public static void setTestCaseStatus(String testCaseId, int test_Status,
			String testCaseStartTime, String testCaseEndTime) {
		LOG.info("in set testcase status _ " + testCaseId + " ---"
				+ test_Status);

		for (int i = 2; i <= xls.getRowCount(TestCaseResultSheet); i++) {

			String testId = xls.getCellData(TestCaseResultSheet,"test_case_id", i);
			if (testId.equalsIgnoreCase(testCaseId)) {
				xls.setCellData(TestCaseResultSheet, "test_Status", i,String.valueOf(test_Status));
				xls.setCellData(TestCaseResultSheet, "testcase_StartTime", i,testCaseStartTime);
				xls.setCellData(TestCaseResultSheet, "testcase_EndTime", i,testCaseEndTime);
			}
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void setTestStepStatus(String testCaseId,
			HashMap test_Step_Status, List actualStatements,
			List component_startTimer, List component_endTimer) {
		try {

			for (int i = 2; i <= xls.getRowCount(TestCaseResultSheet); i++) {

				String testId = xls.getCellData(TestCaseResultSheet,"test_case_id", i);
				if (testId.equalsIgnoreCase(testCaseId)) {

					for (int j = 0; j < test_Step_Status.size(); j++) {
						int stepNum = j + 1;
						int k = 0;

						if (test_Step_Status.get(stepNum) == Boolean.TRUE)
							k = 1;
						if (test_Step_Status.get(stepNum) == Boolean.FALSE)
							k = 5;
						
						xls.setCellData(TestCaseResultSheet, "Step_Status", i,String.valueOf(k));
						xls.setCellData(TestCaseResultSheet, "Actual_Result",i, actualStatements.get(j).toString());
						xls.setCellData(TestCaseResultSheet,"component_StartTime", i, component_startTimer.get(j).toString());
						xls.setCellData(TestCaseResultSheet,"component_EndTime", i,component_endTimer.get(j).toString());
						i++;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public static void setTestStepStatusForUnTestedComponents(String TestCaseID){		
		try{		
			for (int i = 2; i <= xls.getRowCount(TestCaseResultSheet); i++) {

				String testId = xls.getCellData(TestCaseResultSheet,"test_case_id", i);
				
				if (testId.equalsIgnoreCase(TestCaseID)) {
					
					for (int j = i; j >= i; j++){	
						String StepStatusForUnTestedComponenents = xls.getCellData(TestCaseResultSheet, "Step_Status", j);
						String StepID = xls.getCellData(TestCaseResultSheet, "Step_Id", j);											
						
						int k = 3;						
											
						if((StepStatusForUnTestedComponenents == "" || StepStatusForUnTestedComponenents == null) && StepID != ""){							
							xls.setCellData(TestCaseResultSheet, "Step_Status", j,String.valueOf(k));						
						}
						else if(StepID == ""){
							break;
						}					
						i++;
					}
				}				
			
			}	
						
		}catch(Exception ex){		
			ex.printStackTrace();		
		}		
	}	

	public static int setTestcaseStatusForUnTestedComponents(String TestCaseID) {
		int flag = 5;
		for (int i = 2; i <= xls.getRowCount(TestCaseResultSheet); i++) {
			
			String testID = xls.getCellData(TestCaseResultSheet,"test_case_id", i);	
			
			if (testID.equalsIgnoreCase(TestCaseID)) {
				
			for (int j = i; j >= i; j++) {
			String StepStatusForUnTestedComponenents = xls.getCellData(TestCaseResultSheet, "Step_Status", j);
			String StepID = xls.getCellData(TestCaseResultSheet, "Step_Id", j);	
			
				if (StepStatusForUnTestedComponenents.equalsIgnoreCase("1") && StepID != "") {
					flag = 1;
				} else if ((StepStatusForUnTestedComponenents.equalsIgnoreCase("5")|| StepStatusForUnTestedComponenents.equalsIgnoreCase("3")) && StepID != "") {
					flag = 5;
					break;
				} else if(StepID == ""){
					break;
				}
				i++;
			}
			
		  }
		}
		return flag;
	}

	public static void setTrue_InsertResult_TestRail(String testCase,
			String testcaseId) {

		LOG.info("in set setTrue_InsertResult_TestRail _ " + testCase + " ---");

		for (int i = 2; i <= xls.getRowCount(TestCaseResultSheet); i++) {

			String title = xls
					.getCellData(TestCaseResultSheet, "test_title", i);
			String testCaseID = xls.getCellData(TestCaseResultSheet,
					"test_case_id", i);
			if (title.equalsIgnoreCase(testCase)
					&& testCaseID.equalsIgnoreCase(testcaseId)) {
				xls.setCellData(TestCaseResultSheet, "updated_TestRail", i,
						"Yes");
				break;
			}
		}

	}

	public static String renameFileNameAfterTestRailUpdation(
			boolean flag_toInsertTestRail, String testResultPath) {
		String updatedName = "";
		if (flag_toInsertTestRail) {
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			updatedName = testResultPath.replace("Temp_TestRun",
					"Temp_TestRun_completed_TestRail");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		} else {
			// change the shet name to not completed
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			updatedName = testResultPath.replace("Temp_TestRun",
					"Temp_TestRun_NotCompleted_TestRail");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		}

		return updatedName;

	}

	public static String renameFileNameAfterDbInsertion(
			boolean flag_toInsertTestRail, String testResultPath) {
		String updatedName = "";
		if (flag_toInsertTestRail) {
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			updatedName = testResultPath.replaceAll(".xlsx",
					"_DbInsertionCompleted.xlsx");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		} else {
			// change the shet name to not completed
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			updatedName = testResultPath.replaceAll(".xlsx",
					"_DbInsertionFailed.xlsx");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		}

		return updatedName;

	}

	public static void renameFileNameAfterTestRailUpdation_Rerun(
			boolean flag_toInsertTestRail, String testResultPath) {

		if (flag_toInsertTestRail) {
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			String updatedName = testResultPath.replace(
					"Temp_TestRun_NotCompleted_TestRail",
					"Temp_TestRun_completed_TestRail");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		}
	}

	public static String renameFileNameAfterDbUpdation_Rerun(
			boolean flag_toInsertTestRail, String testResultPath) {

		String updatedName = testResultPath;
		if (flag_toInsertTestRail) {
			// change the testsheet name to completed
			File existingFile = new File(testResultPath);
			updatedName = testResultPath.replace("DbInsertionFailed",
					"DbInsertionCompleted");
			File updatedFile = new File(updatedName);
			existingFile.renameTo(updatedFile);
		}
		return updatedName;
	}

	public static void setExecutionDetails(String ExecutionDate,
			String ExecutionStartTime, String ExecutionEndTime) {

		LOG.info("date - " + ExecutionDate);
		LOG.info("start - " + ExecutionStartTime);
		LOG.info("end - " + ExecutionEndTime);
		LOG.info("comments - " + comments);

		xls.setCellData(ExecutionDetails, "ExecutionDate", 2, ExecutionDate);
		xls.setCellData(ExecutionDetails, "ExecutionStartTime", 2,
				ExecutionStartTime);
		xls.setCellData(ExecutionDetails, "ExecutionEndTime", 2,
				ExecutionEndTime);
		xls.setCellData(ExecutionDetails, "Comments", 2, comments);
	}

	public static void setExecutionID(String ExecutionId) {

		LOG.info("insert in excel - ExecutionId - " + ExecutionId);
		xls.setCellData(ExecutionDetails, "Execution_ID", 2, ExecutionId);
	}

	public static HashMap<String, String> getProjectsMap() {
		HashMap<String, String> projects = new HashMap<String, String>();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {
				String proj = xls.getCellData(sheetName, "project_name", i);
				if (!proj.isEmpty()) {
					String projId = xls.getCellData(sheetName, "project_id", i);
					projects.put(proj, projId);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return projects;
	}

	public static List<Integer> getProjectsIdList() {
		List<Integer> projectId = new ArrayList<Integer>();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {
				String proj = xls.getCellData(sheetName, "project_name", i);
				if (!proj.isEmpty()) {
					String projId = xls.getCellData(sheetName, "project_id", i);
					projectId.add(Integer.parseInt(projId));
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return projectId;
	}

	public static List<Integer> getPlanIdList() {
		List<Integer> planId = new ArrayList<Integer>();
		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {
				String planName = xls.getCellData(sheetName, "test_plan_name",
						i);
				if (!planName.isEmpty()) {
					String planIds = xls.getCellData(sheetName, "test_plan_id",
							i);
					planId.add(Integer.parseInt(planIds));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return planId;
	}

	public static String getTestCaseId(String testCaseName, String browser) {
		String tcID = null;
		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {

				String tcName = xls.getCellData(sheetName, " test_title", i);
				String browserName = xls.getCellData(sheetName, "browser", i);
				if (tcName.equalsIgnoreCase(testCaseName)
						&& (browserName.equalsIgnoreCase(browser))) {
					tcID = xls.getCellData(sheetName, "test_case_id", i);
					break;
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return tcID;
	}
	
	public static String GetTestCaseID() throws IOException
	{
		String tcID = null;
		ICsvBeanReader beanReader = null;
        try {
                beanReader = new CsvBeanReader(new FileReader(System.getProperty("user.dir")+"\\Csvdata\\testcasedata.csv"), CsvPreference.STANDARD_PREFERENCE);
                // the header elements are used to map the values to the bean (names must match)
             
                
                final String[] header = new String[] { null, null, null, null, null, null, null, null,
                		null,"test_case_id", null, null , null };
               CreateTestCase createTestCase = null;
               
           		CellProcessor[] processors = new CellProcessor[] {
			        new NotNull(), 
			        new NotNull(), 
			        new NotNull(), 
			        new NotNull(), 
			        new NotNull(),
			        new Optional(),
			        new NotNull(),
			        new NotNull(),
			        new NotNull(),
			        new NotNull(),
			        new NotNull(),
			        new NotNull(),
			        new NotNull(),
			};
           		
                while((createTestCase = beanReader.read(CreateTestCase.class, header, processors)) != null)
                {
                	test_case_ids.add(createTestCase.getTest_case_id());
                }
                
                for(int i=0 ; i<test_case_ids.size();i++)
                {
                	
                	tcID = test_case_ids.get(i);
                }
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	   finally {
    	            if( beanReader != null ) {
    	                beanReader.close();
    	                    
    	            }
    	 	   }
		return tcID;
                
	}
	
	
	
	
	
	

	public static List<Integer> getRunIdList() {
		List<Integer> runId = new ArrayList<Integer>();
		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {
				String runName = xls.getCellData(sheetName, "test_run_name", i);
				if (!runName.isEmpty()) {
					String runIds = xls
							.getCellData(sheetName, "test_run_id", i);
					runId.add(Integer.parseInt(runIds));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return runId;
	}

	public static HashMap<String, String> getPlansMap(String projectId) {
		HashMap<String, String> plans = new HashMap<String, String>();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {

				String projId = xls.getCellData(sheetName, "project_id", i);

				if (projId.equalsIgnoreCase(projectId)) {
					String planName = xls.getCellData(sheetName,
							" test_plan_name", i);
					if (!planName.isEmpty()) {
						String planId = xls.getCellData(sheetName,
								" test_plan_id", i);
						plans.put(planId, planName);
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return plans;
	}

	public static HashMap<String, String> getRunsMap(String planId) {
		HashMap<String, String> runs = new HashMap<String, String>();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {

				String plan_Id = xls.getCellData(sheetName, " test_plan_id", i);

				if (plan_Id.equalsIgnoreCase(planId)) {
					String runName = xls.getCellData(sheetName,
							"test_run_name", i);
					if (!runName.isEmpty()) {
						String runId = xls.getCellData(sheetName,
								"test_run_id", i);
						runs.put(runId, runName);
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return runs;
	}

	public static HashMap<String, String> getSuitesMap(String runId) {
		HashMap<String, String> suites = new HashMap<String, String>();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {

				String run_Id = xls.getCellData(sheetName, " test_run_id", i);

				if (runId.equalsIgnoreCase(run_Id)) {
					String suiteName = xls.getCellData(sheetName,
							"test_suite_name", i);
					if (!suiteName.isEmpty()) {
						String suiteId = xls.getCellData(sheetName,
								"test_suite_id", i);
						suites.put(suiteId, suiteName);
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return suites;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Hashtable getTestCasesInfo(String runId, String testCase) {
		Hashtable testInfo = new Hashtable();
		String sheetName = TestCaseResultSheet;
		for (int i = 2; i < xls.getRowCount(sheetName); i++) {

			String run_Id = xls.getCellData(sheetName, " test_run_id", i);

			if (runId.equalsIgnoreCase(run_Id)) {
				String testCaseName = xls.getCellData(sheetName, "test_title",
						i);
				if (!testCaseName.isEmpty()
						&& testCaseName.equalsIgnoreCase(testCase)) {
					String testStatus = xls.getCellData(sheetName,
							"test_Status", i);
					String testCaseId = xls.getCellData(sheetName,
							"test_case_id", i);
					String testStartTime = xls.getCellData(sheetName,
							"testcase_StartTime", i);
					String testEndTime = xls.getCellData(sheetName,
							"testcase_EndTime", i);
					testInfo.put("test_title", testCaseName);
					testInfo.put("test_Status", testStatus);
					testInfo.put("testCaseId", testCaseId);
					testInfo.put("testStartTime", testStartTime);
					testInfo.put("testEndTime", testEndTime);
				}
			}
		}
		return testInfo;
	}

	public static List getTestCasesForRun(String runId) {
		List tests = new ArrayList();

		try {
			String sheetName = TestCaseResultSheet;
			for (int i = 2; i < xls.getRowCount(sheetName); i++) {

				String run_Id = xls.getCellData(sheetName, " test_run_id", i);

				if (runId.equalsIgnoreCase(run_Id)) {
					String testCase = xls.getCellData(sheetName, "test_title",
							i);
					if (!testCase.isEmpty()) {
						tests.add(testCase);
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return tests;
	}

	/*
	 * public static String getLatestTimeStamp() { DateFormat dateformat = new
	 * SimpleDateFormat("MM-dd-yyyy_hh.mm.ss"); Date date = new Date(); return
	 * dateformat.format(date); }
	 */

}
