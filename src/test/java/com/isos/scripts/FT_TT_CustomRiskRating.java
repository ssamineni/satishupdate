package com.isos.scripts;

import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.automation.CSVUility.CsvHandler;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestRail;
import com.automation.testrail.TestScriptDriver;
import com.isos.ecms.libs.CommonECMSLib;
import com.isos.ecms.libs.ContentEditorLib;
import com.isos.ecms.libs.DeliveryPandemicLib;
import com.isos.ecms.libs.DesktopLib;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.libs.ManualTripEntryLib;
import com.isos.tt.libs.MyTripsLib;
import com.isos.tt.libs.NewsLib;
import com.isos.tt.libs.RiskRatingsLib;
import com.isos.tt.libs.SiteAdminLib;
import com.isos.tt.libs.TTLib;


/**
 * @author E002149
 *
 */


public class FT_TT_CustomRiskRating {

	NewsLib nlib = new NewsLib();
	ManualTripEntryLib mtelib= new ManualTripEntryLib();
	MyTripsLib myTripslib = new MyTripsLib();
	RiskRatingsLib risklib = new RiskRatingsLib();
	SiteAdminLib sitelib = new SiteAdminLib();
	TTLib ttlib = new TTLib();
	CommonLib clib = new CommonLib();
	ContentEditorLib celib = new ContentEditorLib();
	DesktopLib dlib = new DesktopLib();
	CommonECMSLib ecmscommonlib=new CommonECMSLib();
	DeliveryPandemicLib delvpanlib = new DeliveryPandemicLib();
	com.automation.testrail.TestRail trl = new TestRail(); 
	//public static String currtestcaseTitle;
	public static HashMap<Integer, Boolean> testStepStatus = new HashMap<Integer, Boolean>();
	public static String  testCaseId="";
	
	
	@AfterClass
	public void afterClass() {
		String fileName = Thread.currentThread().getStackTrace()[1].getFileName();
		TestScriptDriver.currtestcaseTitle = fileName.substring(0, fileName.lastIndexOf("."));
		testCaseId = CsvHandler.getTestCaseId(TestScriptDriver.currtestcaseTitle,
				TestEngineWeb.browser);
		String startTestCaseTime = (CommonLib.getCurrentTime());
		String endTestCaseTime = (CommonLib.getCurrentTime());
		CsvHandler.setTestCaseStatus(testCaseId,
				trl.getTestcaseStatus(testStepStatus), startTestCaseTime,
				endTestCaseTime);
		CsvHandler.setTestStepStatus(testCaseId, testStepStatus,
				CommonLib.componentActualresult, CommonLib.componentStartTimer,
				CommonLib.componentEndTimer);
		
		CsvHandler.setTestStepStatusForUnTestedComponents(testCaseId);
		CsvHandler.setTestCaseStatus(testCaseId,
				CsvHandler.setTestcaseStatusForUnTestedComponents(testCaseId), startTestCaseTime,
				endTestCaseTime);

		CommonLib.componentActualresult.clear();
		CommonLib.componentStartTimer.clear();
		CommonLib.componentEndTimer.clear();
		testStepStatus.clear();

	
	

    
   }
   
@Test
    public void FT_TT_CustomRiskRating() throws Throwable {
		
		//HashMap<Integer, Boolean> hm = new HashMap<Integer, Boolean>();
		
			try {
testStepStatus.put(1,clib.openBrowser("https://mobileqa.internationalsos.com/mobile/MapUI/v7/"));
testStepStatus.put(2,clib.login("ITGQA@internationalsos.com","VlRGb1QyUnRUWGRSV0doT1lXc3dkMVJzVWxwUVVUMDk="));
testStepStatus.put(3,risklib.verifyRiskRatingsPage());
testStepStatus.put(4,risklib.verifyTravelMedicalRisks("United States"));
testStepStatus.put(5,risklib.updateTravelMedicalRisks());
testStepStatus.put(6,risklib.verifyLastUpdatedMsg());
testStepStatus.put(7,risklib.verifyTravelAndMedicalRatings("United States"));
testStepStatus.put(8,clib.quitBrowser());
				
				 }
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			//return hm;
		}
		
		
}